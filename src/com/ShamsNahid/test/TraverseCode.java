package com.ShamsNahid.test;

import java.util.ArrayList;
import java.util.HashMap;

import com.ShamsNahid.Condition.Else.ElseConditionInfo;
import com.ShamsNahid.Condition.ElseIf.ElseIfConditionInfo;
import com.ShamsNahid.Condition.IF.IfConditionInfo;
import com.ShamsNahid.Generic.GenericInfo;
import com.ShamsNahid.Loop.For.ForLoopInfo;
import com.ShamsNahid.Loop.While.WhileLoopInfo;
import com.ShamsNahid.Objects.MyObject;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;
import com.ShamsNahid.Objects.ObjectInfo;
import com.ShamsNahid.method.MethodInfo;
import com.ShamsNahid.ParentContainer.ParentContainerInfo;


/*
 * HERE WE TRAVERSE THE WHOLE SYMBOL TABLE
 * All the method is saved in the symbol table
 * Hence by a loop we first access all the method identifier
 * Then we traverse the whole method body and then
 * For each loop or condition's head section and body portion will be printed
 * For each case the may be a nested loop or condition, they will be also printed by method printGenericInfo
*/
public class TraverseCode {
	
	private ParentContainerInfo parentContainerInfo;	//first we get the symbol table
	private Token token;	//also need all the token list identifier

	private HashMap<Integer, TokenInfo> hmTokenInfo;	//all the tokens info is placed according to their identifier
	private ArrayList<Integer> alToken;	//tokens identifier list
	
	public TraverseCode(ParentContainerInfo parentContainerInfo, Token token) {	//receive symbol table and the token
		System.out.println("\n\n+++++++++++++++++++++++TRAVERSE CODE++++++++++++++++++++++++++");
		this.parentContainerInfo = parentContainerInfo;	//store in the local variable
		this.token = token;	//store in the local variable
		hmTokenInfo = token.getHm();	//retrieve the tokens identity and info
		alToken = token.getToken();	//got the tokens identity
	}
	
	/*
	 * Actually symbol table contains the method name now
	 * Hence here we get each of the method identity and call the print their generic info
	 * using the printGenericInfo method 
	*/
	public void printSymbolTable() {
		ArrayList<Integer> alSymbolTable = parentContainerInfo.getAlMethods();
		HashMap<Integer, MethodInfo> hmMethodInfo = parentContainerInfo.getHmMethodInfo();
		
		for(Integer index : alSymbolTable) {
			MethodInfo methodInfo = hmMethodInfo.get(index);
			GenericInfo genericInfo = methodInfo.getGenericInfo();
			printGenericInfo(genericInfo, "    ");
		}
	}
	
	/*
	 * Here we get a generic Info and print it
	 * Each generic info has two parts: Head Portion and the body portion
	 * for a head portion just print the token identity using the printElementKeyword method
	 * then in body portion, we check if it is a generic element(condition or loop) or not
	 * if this is a generic element, then get the element generic info and print it
	 * else we just print the keyword 
	*/
	public void printGenericInfo(GenericInfo genericInfo, String startSpace) {
		ArrayList<Integer> headPortion = genericInfo.getHeadPortion();
		ArrayList<Integer> filterBody = genericInfo.getFilterBody();
		
		System.out.print(startSpace + "Head Portion: ");
		for(Integer index : headPortion) {
			printElementKeyword(index);
		}System.out.println();		

		//if there are object exist, then we display the information here
        System.out.println(startSpace + "Objects: " + genericInfo.getMyObject().getAlObjectList());
        /**
         * Avoiding the nested objects property, just first row of object is filtered
         * method() {
         *      obj1, obj2 = {
         *          x, y, z = {
         *              a, b, c
         *          }
         *      }
         *      ob1, ob2;
         * }
         * here we take onlyobj1, obj2, ob1, ob2 cause xyz are in lavel 2 and a, b, c are in lavel 3
        */
        ArrayList<Integer> levelOneObjectList = new ArrayList<>();
        for(Integer objectId : genericInfo.getMyObject().getAlObjectList()) {

			if(genericInfo.getMyObject().getHmObjectInfo().get(objectId).getParentTree().size() <= 3) {
                levelOneObjectList.add(objectId);
            }
        }
		printObjectInfo(startSpace, genericInfo.getMyObject().getAlObjectList(), genericInfo.getMyObject().getHmObjectInfo(), genericInfo);

		System.out.println(startSpace + "STATEMENTS:");
		for(ArrayList<Integer> statement : genericInfo.getStatementList()) {
            System.out.print(startSpace);
            for(Integer identity : statement) {
                printElementKeyword(identity);
                System.out.print(" ");
            }
            System.out.println();
        }

		System.out.println(startSpace + "Body: ");
		for(Integer index : filterBody) {
			TokenInfo ti = new TokenInfo();
			ti = hmTokenInfo.get(index);
			int genericCategory = ti.getGenericCatagory();
			
			if(genericCategory == 101 || genericCategory == 102) {	//if loop or condition
				int category = ti.getCatagory();
				if(genericCategory == 101) {	//this is loop 
					if(category == 31) {	//if for loop
						System.out.println("\n" + startSpace + "FOR LOOP" + " (identity: " + index + " )");
						HashMap<Integer, ForLoopInfo> hmForloopInfo = genericInfo.getHmForLoopInfo();
						ForLoopInfo forLoopInfo = hmForloopInfo.get(index);
						printGenericInfo(forLoopInfo.getGenericInfo(), startSpace += "    ");
					} else if(category == 33) {	//if while loop
						System.out.println("\n" + startSpace + "WHILE LOOP");
						HashMap<Integer, WhileLoopInfo> hmWhileloopInfo = genericInfo.getHmWhileLoopInfo();
						WhileLoopInfo whileLoopInfo = (WhileLoopInfo) hmWhileloopInfo.get(index);
						printGenericInfo(whileLoopInfo.getGenericInfo(), startSpace += "    ");
					}
				} else {	//this is a condition
					if(category == 32) {	//if condition
						System.out.println("\n" + startSpace + "IF CONDITION");
						HashMap<Integer, IfConditionInfo> hmIfConditionInfo = genericInfo.getHmIfConditionInfo();
						IfConditionInfo ifConditionInfo = hmIfConditionInfo.get(index);
						printGenericInfo(ifConditionInfo.getGenericInfo(), startSpace += "    ");
					} else if(category == 36) {	//else if condition
						System.out.println("\n" + startSpace + "ELSE IF CONDITION");
						HashMap<Integer, ElseIfConditionInfo> hmElseIfConditionInfo = genericInfo.getHmElseIfConditionInfo();
						ElseIfConditionInfo elseIfConditionInfo = hmElseIfConditionInfo.get(index);
						printGenericInfo(elseIfConditionInfo.getGenericInfo(), startSpace += "    ");
					} else if(category == 34) {	//else condition
						System.out.println("\n" + startSpace + "ELSE CONDITION");
						HashMap<Integer, ElseConditionInfo> hmElseConditionInfo = genericInfo.getHmElseConditionInfo();
						ElseConditionInfo elseConditionInfo = hmElseConditionInfo.get(index);
						printGenericInfo(elseConditionInfo.getGenericInfo(), startSpace += "    ");
					}
				}
			} else {
				System.out.print(startSpace +  ti.getKeyword());	//simple token
			}
		}
		System.out.println();
	}
	
	private void printObjectInfo(String startSpace, ArrayList<Integer> alObjectList, HashMap<Integer, ObjectInfo> hmObjectInfo, GenericInfo genericInfo) {
        //If there are object exist, then we print their name and various information

        for(Integer objectId : alObjectList) {
            ObjectInfo objectInfo = hmObjectInfo.get(objectId);

            ArrayList<Integer> alObjectValueExpression = objectInfo.getObjectValueExpression();

            System.out.println(startSpace + "**********************************************************");
            System.out.print(startSpace +  "|-----Full Path: ");
            printObjectParentTree(objectInfo.getParentTree(), genericInfo);
            System.out.println();
            System.out.println(startSpace + "|-----Name: " + objectInfo.getObjectName() + "(" + objectInfo.getObjectId() + ")");
            System.out.print(startSpace + "|--------Expression => ");
            for(Integer value : alObjectValueExpression) {
                printElementKeyword(value);
                System.out.print(" ");
            }
            System.out.println();
            System.out.println(startSpace + "|--------Value => " + objectInfo.getObjectValue());
            System.out.println(startSpace + "|--------Tree => " + objectInfo.getParentTree());
            System.out.println(startSpace + "|--------Category => " + objectInfo.getObjectCategory());
            System.out.println(startSpace + "|--------Properties => " + objectInfo.getAlPropertyObjectsId());
            System.out.println(startSpace + "|--------Elements => " + objectInfo.getAlArrayElementsObjectsId());

            if(objectInfo.getObjectCategory() == 3101) {
                //System.out.println("PLAIN PLAIN PLAIN PLAIN PLAIN PLAIN PLAIN PLAIN PLAIN PLAIN PLAIN PLAIN");
            } else if(objectInfo.getObjectCategory() == 3102) {
                printObjectInfo(startSpace + "    ", objectInfo.getAlPropertyObjectsId(), objectInfo.getHmPropertyObjectInfo(), genericInfo);
            } else if(objectInfo.getObjectCategory() == 3103) {
				printObjectInfo(startSpace + "    ", objectInfo.getAlArrayElementsObjectsId(), objectInfo.getHmPropertyObjectInfo(), genericInfo);
			}
        } System.out.println();
    }

	/*
	 * Here we get the index of a token and 
	 * using the token info just print the keyword
	*/
	private void printElementKeyword(Integer element) {
		TokenInfo ti = new TokenInfo();
		ti = hmTokenInfo.get(element);
		System.out.print(ti.getKeyword());
	}

	/**
     * Here we provide the parent tree of  the object and then the genericInfo
     * then we traverse from top to bottom of the tree
     * for each object we print the name and then we dig it's child object
     * we use the following recursive method to do this printObjectName(Integer objectId, ObjectInfo parentObjectInfo)
     * since 0 means the parent then we indicate it via root
     * first index of the tree is the parent, it is already available in the genericInfo myObject portion
     * the next object are only available to the current object info
	*/
	private void printObjectParentTree(ArrayList<Integer> alParentTree, GenericInfo genericInfo) {
        MyObject myObject = genericInfo.getMyObject();
        ObjectInfo objectInfo = new ObjectInfo();

        for(Integer index=0; index<alParentTree.size(); index++) {
            if(index == 0) {
                System.out.print("root");
                continue;
            }
            if(index == 1) {
                objectInfo = myObject.getHmObjectInfo().get(alParentTree.get(index));
                System.out.print("." + objectInfo.getObjectName());
            } else {
                if(objectInfo.getObjectCategory() == 3102) {
                    objectInfo = printObjectName(alParentTree.get(index), objectInfo.getAlPropertyObjectsId(), objectInfo.getHmPropertyObjectInfo());
                } else if(objectInfo.getObjectCategory() == 3103) {
                    objectInfo = printObjectName(alParentTree.get(index), objectInfo.getAlArrayElementsObjectsId(), objectInfo.getHmPropertyObjectInfo());
                }
            }
        }
    }

    private ObjectInfo printObjectName(Integer objectId, ArrayList<Integer> alObjectList, HashMap<Integer, ObjectInfo> hmObjectInfo) {
        ObjectInfo objectInfo  = new ObjectInfo();
        for(Integer childId: alObjectList) {
            if(childId == objectId) {
                objectInfo = hmObjectInfo.get(childId);
                System.out.print("." + objectInfo.getObjectName());
                break;
            }
        }
        return objectInfo;
    }

}
