package com.ShamsNahid.Helper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import com.ShamsNahid.Objects.MyObject;
import com.ShamsNahid.Objects.ObjectInfo;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;

public class Utility {
	//parrenthesis
	private ArrayList<String> alOpeningFirstBraket = new ArrayList<String>();
	private ArrayList<String> alClosingFirstBraket = new ArrayList<String>();
	private ArrayList<String> alFirstBrakets = new ArrayList<String>();
	
	private ArrayList<String> alOpeningSecondBraket = new ArrayList<String>();
	private ArrayList<String> alClosingSecondBraket = new ArrayList<String>();
	private ArrayList<String> alSecondBrakets = new ArrayList<String>();
	
	private ArrayList<String> alOpeningThirdBraket = new ArrayList<String>();
	private ArrayList<String> alClosingThirdBraket = new ArrayList<String>();
	private ArrayList<String> alThirdBrakets = new ArrayList<String>();
	
	private ArrayList<String> alOpeningBraket = new ArrayList<String>();
	private ArrayList<String> alClosingBraket = new ArrayList<String>();
	
	ArrayList<String> alAllBraketList = new ArrayList<String>();
	
	//if, else, if else container
	private ArrayList<String> alConditionalKeywordList = new ArrayList<String>();
	//variously used operatoor
	private ArrayList<String> alOperatorList = new ArrayList<String>();
	//conditional operator, like || $$ == != etc
	private ArrayList<String> alConditionalOpearatorList = new ArrayList<>();
	//commonly used data type list
	private ArrayList<String> alDAtaTypeList = new ArrayList<String>();
	//loop name list
	private ArrayList<String> alLoopList = new ArrayList<String>();
	//return type is all dataType with void
	private ArrayList<String> alReturnTypeList = new ArrayList<String>();
	//puntuationtionList
	private ArrayList<String> alPuntuationList = new ArrayList<String>();
	//specialKeywordList
	private ArrayList<String> alSpecialKeywordList = new ArrayList<String>();
	//all number 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0
	private ArrayList<String> alNumberList = new ArrayList<String>();
	//all Alphabet a, b, c, ..., A, B, C, ...
	private ArrayList<String> alAlphabetList = new ArrayList<String>();
	//library function
	private ArrayList<String> alLibraryFunctionList = new ArrayList<String>();
	//library keyword like _dec
	private ArrayList<String> alLibraryKeywordList = new ArrayList<String>();
	//Keyword value, like operator = 1, puntuation = 2 etc etc
	private HashMap<String, String> hmKeywordValueList = new HashMap<String, String>();
	
	
	public Utility() {
		readyBrakets();
		readyConditionalKeyWordList();
		readyOperatorList();
		setDataTypeList();
		setLoopList();
		setReturnTypeList();
		setPuntuationList();
		setSpecialKeywordList();
		setNumberList();
		setAlphabetList();
		setLibraryFunctionList();
		setLibraryKeywordList();
		setKeywordValueList();
		setConditionalOperatorList();
	}
	
	public ArrayList<String> getAlOpeningFirstBraket() {
		return alOpeningFirstBraket;
	}
	public ArrayList<String> getAlClosingFirstBraket() {
		return alClosingFirstBraket;
	}
	public ArrayList<String> getAlFirstBrakets() {
		return alFirstBrakets;
	}
	public ArrayList<String> getAlOpeningSecondBraket() {
		return alOpeningSecondBraket;
	}
	public ArrayList<String> getAlClosingSecondBraket() {
		return alClosingSecondBraket;
	}
	public ArrayList<String> getAlSecondBrakets() {
		return alSecondBrakets;
	}
	public ArrayList<String> getAlOpeningThirdBraket() {
		return alOpeningThirdBraket;
	}
	public ArrayList<String> getAlClosingThirdBraket() {
		return alClosingThirdBraket;
	}
	public ArrayList<String> getAlThirdBrakets() {
		return alThirdBrakets;
	}
	public ArrayList<String> getAllBraketList() {
		return alAllBraketList;
	}
	public ArrayList<String> getAlConditionalKeywordList() {
		return alConditionalKeywordList;
	}
	public ArrayList<String> getAlOperatorList() {
		return alOperatorList;
	}
	public ArrayList<String> getAlConditionalOperatorList() {
		return alConditionalOpearatorList;
	}
	public ArrayList<String> getAlDataTypeList() {
		return alDAtaTypeList;
	}
	public ArrayList<String> getAlLoopList() {
		return alLoopList;
	}
	public ArrayList<String> getAlReturnTypeList() {
		return alReturnTypeList;
	}	
	public ArrayList<String> getAlPuntuationList() {
		return alPuntuationList;
	}
	public ArrayList<String> getAlSpecialKeywordList() {
		return alSpecialKeywordList;
	}
	public ArrayList<String> getAlNumberList() {
		return alNumberList;
	}
	public ArrayList<String> getAlAlphabetList() {
		return alAlphabetList;
	}
	public ArrayList<String> getAlLibraryFunctionList() {
		return alLibraryFunctionList;
	}
	public ArrayList<String> getAlLibraryKeywordList() {
		return alLibraryKeywordList;
	}
	public HashMap<String, String> getHmKeywordValueList() {
		return hmKeywordValueList;
	}
	
	public String removeSpace(String str) {
		
		String newStr = "";
		String[] strArr = str.split("");
		
		for(String strTemp : strArr) {
			if(strTemp.equals("")) continue;
			else newStr += strTemp;
		}
		
		return newStr;
	}
	
	public void printArrayList(ArrayList<String> al) {
		for(String str : al) {
			System.out.print(str + "::");
		}
		System.out.println("");
	}
	
	public void printSimpleArrayList(ArrayList<String> al) {
		for(String str : al) {
			System.out.print(str + " ");
		}
		System.out.println();
	}
	
	private void readyBrakets() {
		alFirstBrakets.add("(");
		alFirstBrakets.add(")");
		
		alOpeningFirstBraket.add("(");
		alClosingFirstBraket.add(")");
		
		alSecondBrakets.add("{");
		alSecondBrakets.add("}");
		
		alOpeningSecondBraket.add("{");
		alClosingSecondBraket.add("}");
		
		alThirdBrakets.add("[");
		alThirdBrakets.add("]");
		
		alOpeningThirdBraket.add("[");
		alClosingThirdBraket.add("]");
		
		alOpeningBraket.add("(");
		alOpeningBraket.add("{");
		alOpeningBraket.add("[");
		
		alClosingBraket.add(")");
		alClosingBraket.add("}");
		alClosingBraket.add("]");
		
		alAllBraketList.add("(");
		alAllBraketList.add(")");
		alAllBraketList.add("{");
		alAllBraketList.add("}");
		alAllBraketList.add("[");
		alAllBraketList.add("]");
	}
	
	private void readyConditionalKeyWordList() {
		alConditionalKeywordList.add("if");
		alConditionalKeywordList.add("else");
		alConditionalKeywordList.add("else if");
	}
	
	private void readyOperatorList() {
		alOperatorList.add("!");
		alOperatorList.add("&");
		alOperatorList.add("=");
		alOperatorList.add("+");
		alOperatorList.add("-");
		alOperatorList.add("*");
		alOperatorList.add("/");
		alOperatorList.add("~");
		alOperatorList.add("#");
		alOperatorList.add("%");
		alOperatorList.add("|");
		alOperatorList.add("$");
		alOperatorList.add("<");
		alOperatorList.add(">");
	}
	
	private void setConditionalOperatorList() {
		alConditionalOpearatorList.add("||");
		alConditionalOpearatorList.add("&&");
		alConditionalOpearatorList.add("==");
		alConditionalOpearatorList.add("!=");
		alConditionalOpearatorList.add("<");
		alConditionalOpearatorList.add(">");
		alConditionalOpearatorList.add("<=");
		alConditionalOpearatorList.add(">=");
	}
	
	public boolean checkBrakets(ArrayList<String> al) {
		
		Stack<String> stack = new Stack<String>();
		
		for(String item : al) {
			if(alOpeningBraket.contains(item)) {
				stack.push(item);
			}
			else if(item.equals(")")) {
				if(stack.isEmpty()) return false;
				if(stack.pop().equals("(") == false) return false;
			}
			else if(item.equals("}")) {
				if(stack.isEmpty()) return false;
				if(stack.pop().equals("{") == false) return false;
			}
			else if(item.equals("]")) {
				if(stack.isEmpty()) return false;
				if(stack.pop().equals("[") == false) return false;
			}
		}
		if(stack.isEmpty()) return true;
		return false;
	}
	
	private void setDataTypeList() {
		alDAtaTypeList.add("int");
		alDAtaTypeList.add("double");
		alDAtaTypeList.add("float");
		alDAtaTypeList.add("long");
	}
	
	private void setLoopList() {
		alLoopList.add("for");
		alLoopList.add("while");
		alLoopList.add("do");
	}
	
	private void setReturnTypeList() {
		alReturnTypeList.add("int");
		alReturnTypeList.add("double");
		alReturnTypeList.add("float");
		alReturnTypeList.add("long");
		alReturnTypeList.add("void");
	}
	
	private void setPuntuationList() {
		alPuntuationList.add(";");
		alPuntuationList.add(",");
		alPuntuationList.add(".");
		alPuntuationList.add("\"");
		alPuntuationList.add("\'");
	}
	
	private void setSpecialKeywordList() {
		alSpecialKeywordList.add("return");
		alSpecialKeywordList.add("variable");
		alSpecialKeywordList.add("\n");
	}
	
	private void setNumberList() {
		alNumberList.add("0");		alNumberList.add("1");
		alNumberList.add("2");		alNumberList.add("3");
		alNumberList.add("4");		alNumberList.add("5");
		alNumberList.add("6");		alNumberList.add("7");
		alNumberList.add("8");		alNumberList.add("9");
		//alNumberList.add(".");
	}
	
	private void setAlphabetList() {
		alAlphabetList.add("a");		alAlphabetList.add("b");		alAlphabetList.add("c");		alAlphabetList.add("d");
		alAlphabetList.add("e");		alAlphabetList.add("f");		alAlphabetList.add("g");		alAlphabetList.add("h");
		alAlphabetList.add("i");		alAlphabetList.add("j");		alAlphabetList.add("k");		alAlphabetList.add("l");
		alAlphabetList.add("m");		alAlphabetList.add("n");		alAlphabetList.add("o");		alAlphabetList.add("p");
		alAlphabetList.add("q");		alAlphabetList.add("r");		alAlphabetList.add("s");		alAlphabetList.add("t");
		alAlphabetList.add("u");		alAlphabetList.add("v");		alAlphabetList.add("w");		alAlphabetList.add("x");
		alAlphabetList.add("y");		alAlphabetList.add("z");		alAlphabetList.add("A");		alAlphabetList.add("B");
		alAlphabetList.add("C");		alAlphabetList.add("D");		alAlphabetList.add("E");		alAlphabetList.add("F");
		alAlphabetList.add("G");		alAlphabetList.add("H");		alAlphabetList.add("I");		alAlphabetList.add("J");
		alAlphabetList.add("K");		alAlphabetList.add("L");		alAlphabetList.add("M");		alAlphabetList.add("N");
		alAlphabetList.add("O");		alAlphabetList.add("P");		alAlphabetList.add("Q");		alAlphabetList.add("R");
		alAlphabetList.add("S");		alAlphabetList.add("T");		alAlphabetList.add("U");		alAlphabetList.add("V");
		alAlphabetList.add("W");		alAlphabetList.add("X");		alAlphabetList.add("Y");		alAlphabetList.add("Z");
		alAlphabetList.add("_");
	}
	
	private void setLibraryFunctionList() {
		alLibraryFunctionList.add("Display");
		alLibraryFunctionList.add("Input");
	}
	
	private void setLibraryKeywordList() {
		alLibraryKeywordList.add("_dec");
	}
	
	public void printError(String str) {
		System.out.println("\n<<<ERROR>>> " + str);
	}
	
	public boolean isNumber(String str) {
		String[] strArray = str.split("");
		boolean isNum = true;
		
		if(!(alOperatorList.contains(strArray[0]) || alNumberList.contains(strArray[0]))) {
			return false;
		}
		
		for(int index=1; index<strArray.length; index++) {
			String strTemp = strArray[index];
			
			if(alNumberList.contains(strTemp) == false) {
				isNum = false;
				break;
			}
		}
		
		return isNum;
	}
	
	public boolean isOperator(String str) {
		String[] strArray = str.split("");
		boolean isOperator = true;
		
		for(String strTemp : strArray) {
			if(alOperatorList.contains(strTemp) == false) {
				isOperator = false;
				break;
			}
		}
		
		return isOperator;
	}
	
	//http://stackoverflow.com/questions/3422673/evaluating-a-math-expression-given-in-string-form
	public double eval(final String str) {
	    return new Object() {
	        int pos = -1, ch;

	        void nextChar() {
	            ch = (++pos < str.length()) ? str.charAt(pos) : -1;
	        }

	        boolean eat(int charToEat) {
	            while (ch == ' ') nextChar();
	            if (ch == charToEat) {
	                nextChar();
	                return true;
	            }
	            return false;
	        }

	        double parse() {
	            nextChar();
	            double x = parseExpression();
	            if (pos < str.length()) throw new RuntimeException("Unexpected: " + (char)ch);
	            return x;
	        }
	        
	        double parseExpression() {
	            double x = parseTerm();
	            for (;;) {
	                if      (eat('+')) x += parseTerm(); // addition
	                else if (eat('-')) x -= parseTerm(); // subtraction
	                else return x;
	            }
	        }

	        double parseTerm() {
	            double x = parseFactor();
	            for (;;) {
	                if      (eat('*')) x *= parseFactor(); // multiplication
	                else if (eat('/')) x /= parseFactor(); // division
	                else return x;
	            }
	        }

	        double parseFactor() {
	            if (eat('+')) return parseFactor(); // unary plus
	            if (eat('-')) return -parseFactor(); // unary minus

	            double x;
	            int startPos = this.pos;
	            if (eat('(')) { // parentheses
	                x = parseExpression();
	                eat(')');
	            } else if ((ch >= '0' && ch <= '9') || ch == '.') { // numbers
	                while ((ch >= '0' && ch <= '9') || ch == '.') nextChar();
	                x = Double.parseDouble(str.substring(startPos, this.pos));
	            } else if (ch >= 'a' && ch <= 'z') { // functions
	                while (ch >= 'a' && ch <= 'z') nextChar();
	                String func = str.substring(startPos, this.pos);
	                x = parseFactor();
	                if (func.equals("sqrt")) x = Math.sqrt(x);
	                else if (func.equals("sin")) x = Math.sin(Math.toRadians(x));
	                else if (func.equals("cos")) x = Math.cos(Math.toRadians(x));
	                else if (func.equals("tan")) x = Math.tan(Math.toRadians(x));
	                else throw new RuntimeException("Unknown function: " + func);
	            } else {
	                throw new RuntimeException("Unexpected: " + (char)ch);
	            }

	            if (eat('^')) x = Math.pow(x, parseFactor()); // exponentiation

	            return x;
	        }
	    }.parse();
	}
	
	private void setKeywordValueList() {
		hmKeywordValueList.put("Operator", "0");
		hmKeywordValueList.put("Unary", "1");
		hmKeywordValueList.put("Binary", "2");
		hmKeywordValueList.put("Puntuation", "3");
		hmKeywordValueList.put("Condition", "4");
		hmKeywordValueList.put("Loop", "5");
		hmKeywordValueList.put("DataType", "6");
		hmKeywordValueList.put("ReturnType", "7");
		hmKeywordValueList.put("SpecialKeyword", "8");
		hmKeywordValueList.put("LibraryFunction", "9");
		hmKeywordValueList.put("Braket", "10");
		hmKeywordValueList.put("Identifier", "11");
	}
	
	
	/*
	 * It's the only one generic printer
	 * If there is a identity list(integer), token hashmap(integer, tokeninfo) and token(integer) lsit 
	 * it will print the name  
	*/
	public void printGenericElements(ArrayList<Integer> alElements, HashMap<Integer, TokenInfo> hmTokenInfo, ArrayList<Integer> alToken) {
		for(int index : alElements) {	//got all the identity
			TokenInfo ti = new TokenInfo();	//declare the token info to store the desired identity's token info
			ti = hmTokenInfo.get(alToken.get(index));	//we got the token info from using the identity of the token
			System.out.print(ti.getKeyword() + " ");	// printing the tojkens keyword from the the tokeninfo class
		}
		System.out.println();	//need a new line after printing all the elements,, cause there i use print not printLine
	}

}
