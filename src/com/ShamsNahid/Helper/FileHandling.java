package com.ShamsNahid.Helper;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

public class FileHandling {
	
	public boolean openFile(String fileName, String text) {
		System.out.println("got: " + text + "/n end");
		
		BufferedWriter writer = null;
		try
		{
		    writer = new BufferedWriter( new FileWriter(fileName));
		    writer.write(text);

		}
		catch ( IOException e)
		{
		}
		finally
		{
		    try
		    {
		        if ( writer != null)
		        writer.close( );
		    }
		    catch ( IOException e)
		    {
		    }
		}
		
		return true;
	}
	
	public String readFile(String fileName) throws IOException {
		FileInputStream fstream = null;
		String str = "";
		
		try {
			fstream = new FileInputStream(fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

		String strLine;

		//Read File Line By Line
		while ((strLine = br.readLine()) != null)   {
		  // Print the content on the console
		  //System.out.println (strLine);
		  str += (strLine + "\n");
		}

		//Close the input stream
		br.close();
		return str;
	}

}
