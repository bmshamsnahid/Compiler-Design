package com.ShamsNahid.Loop.DoWhile;

import java.util.ArrayList;
import java.util.HashMap;

public class DoWhileLoopInfo {

	//---------------------------------------------------------------------------------------------
	private int doWhileLoopIdentity;	//doWwhile loop identity
	private String doWhileLoopName;	//lol it's doWhile loop, but also put it here
	private ArrayList<Integer> doWhileLoopCondition;	//loop condition part will be stored here
	private ArrayList<Integer> doWhileLoopBody;		//loop Body part will be stored here
	private HashMap<Integer, DoWhileLoopInfo> doWhileLoopHmDoWhileLoopInfo;	//nested for loop info will be stored here
		
	//GETTER AND SETTER
	public ArrayList<Integer> getDoWhileLoopCondition() {
		return doWhileLoopCondition;
	}
	public void setDoWhileLoopCondition(ArrayList<Integer> doWhileLoopCondition) {
		this.doWhileLoopCondition = doWhileLoopCondition;
	}
	public ArrayList<Integer> getDoWhileLoopBody() {
		return doWhileLoopBody;
	}
	public void setDoWhileLoopBody(ArrayList<Integer> doWhileLoopBody) {
		this.doWhileLoopBody = doWhileLoopBody;
	}
	public int getDoWhileLoopIdentity() {
		return doWhileLoopIdentity;
	}
	public void setDoWhileLoopIdentity(int doWhileLoopIdentity) {
		this.doWhileLoopIdentity = doWhileLoopIdentity;
	}
	public String getDoWhileLoopName() {
		return doWhileLoopName;
	}
	public void setDoWhileLoopName(String doWhileLoopName) {
		this.doWhileLoopName = doWhileLoopName;
	}
	public HashMap<Integer, DoWhileLoopInfo> getDoWhileLoopHmDoWhileLoopInfo() {
		return doWhileLoopHmDoWhileLoopInfo;
	}
	public void setDoWhileLoopHmDoWhileLoopInfo(HashMap<Integer, DoWhileLoopInfo> doWhileLoopHmDoWhileLoopInfo) {
		this.doWhileLoopHmDoWhileLoopInfo = doWhileLoopHmDoWhileLoopInfo;
	}
	
}
