package com.ShamsNahid.Loop.DoWhile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;

public class DoWhileLoopDigging {
	
	private Token token;	//we will receive this through constructor
	private ArrayList<Integer> alToken; 	//Then all token will be kept here
	private HashMap<Integer, TokenInfo> hmTokenInfo;	//Each token info is stored here for regarding its id

	private DoWhileLoopInfo doWhileLoopInfo;	//bitch here 
	
	private ArrayList<Integer> doWhileLoopCondition;	//doWhile loop condition will be placed here
	private ArrayList<Integer> doWhileLoopBody;		//doWhile loop body will be placed here
	private HashMap<Integer, DoWhileLoopInfo> doWhileLoopHmDoWhileLoopInfo;	//here we store all doWhile loop info found in the loop body, according to the integer
	
	private Integer doWhileLoopIdentifier;	//receive from methodDigging class
	private int doWhileLoopIndex;	//from doWhileLoopIdentifier, we receive the doWhileLoopIndex
	
	
	//GETTER
	public DoWhileLoopInfo getDoWhileLoopInfo()  {
		return doWhileLoopInfo;
	}
	
	public DoWhileLoopDigging(Token token, Integer doWhileLoopIdentifier) {
		this.token = token;		//got token
		this.alToken = token.getToken();	//got all token identifier
		this.hmTokenInfo = token.getHm();	//got token info according to the token identifier
		
		this.doWhileLoopIdentifier = doWhileLoopIdentifier;	//assign doWhileLoopIdentifier
		
		doWhileLoopInfo = new DoWhileLoopInfo();	//declaring MC object, loopCondition and loopBody will be stored here
		
		doWhileLoopCondition = new ArrayList<>();	//declaring the doWhileLoopContion part, loop condition will be placed here
		doWhileLoopBody = new ArrayList<>();	//declarng the doWhileLoopBody part, loop body will be placed here
		doWhileLoopHmDoWhileLoopInfo = new HashMap<>();	//declaring doWhileLoopHmDoWhileLoopInfo
		
		doWhileLoopIndex = alToken.indexOf(doWhileLoopIdentifier);		//here's the index of current proceeding loop identifier
	}
	
	//while loop digging placed here
	public int digDoWhileLoop() {
		//int index = getBody(getConditionPart(doWhileLoopIndex));
		int index = getConditionPart(getBody(doWhileLoopIndex));
		
		doWhileLoopInfo.setDoWhileLoopName("Do WHILE");
		doWhileLoopInfo.setDoWhileLoopIdentity(doWhileLoopIdentifier);
		doWhileLoopInfo.setDoWhileLoopCondition(doWhileLoopCondition);	//inserting the loop condition to doWhileLoopInfo object
		doWhileLoopInfo.setDoWhileLoopBody(doWhileLoopBody);	//inserting the loop body to doWhileLoopInfo object
		doWhileLoopInfo.setDoWhileLoopHmDoWhileLoopInfo(doWhileLoopHmDoWhileLoopInfo);	//inserting the doWhileWhileInfo
		
		return index;	//the next index of the while loop territory is placed here
	}
	
	

	private int getConditionPart(Integer doWhileLoopIndex) {
		int index = doWhileLoopIndex;	//assigning the doWhileLoopIndex
		index++;	 //index is doWhileLoop identifier and after increased by 1 we got the first parenthesis
		
		Stack<Integer> stkCondition = new Stack<Integer>();
		
		do {
			TokenInfo tokenInfo = new TokenInfo();
			tokenInfo = hmTokenInfo.get(alToken.get(index));
			Integer tokenIdentity = tokenInfo.getIdentity();	//get token identity
			Integer tokenCategory = tokenInfo.getCatagory();	//get token category
			Integer tokenGenericCategory = tokenInfo.getGenericCatagory();	//get token generic category to detect identifier(parameter)
			
			/*
			 * If token is opening first parenthesis
			 * 		Then the identity will be pushed in stack
			 * Else if the token is closing closing first parenthesis 
			 * 		Then the the value is pop out from stack
			 * When the stack is empty then the conditional portion is finished
			*/
			if(tokenCategory == 22) {
				stkCondition.push(tokenIdentity);
			} else if(tokenCategory == 21) {
				stkCondition.pop();
			}
			
			doWhileLoopCondition.add(tokenIdentity);  //element in the parameter territarry is added here
			
			index++;
		} while(!stkCondition.isEmpty());
		
		return index;
	}
	
	/*
	 * A method can have multiple statement
	 * 	-> Hence it will have second parenthesis { and }
	 * Otherwise It contains only one statement
	 * 	-> So have to traverse to ;
	*/
	private int getBody(int index) {
		index++;	//since do while loop receive a do keyword in this body section, we increase it by 1, so we can get opening close parenthesis
		Stack<Integer> stkBody = new Stack<Integer>();	//we gonna track loop body territory
		
		/*
		 * In this method single statement is found if no opening second parenthesis is found after methodd parameter territory
		 * Generic category is not equal 24 means, only one statement exist
		 * That might be a condition or loop
		*/
		if(hmTokenInfo.get(alToken.get(index)).getCatagory() != 24) {
			
			if(hmTokenInfo.get(alToken.get(index)).getGenericCatagory() == 101) {	//if this is a loop(for/while/do while)
				index = loopHandle(index);
			}
			else { //no loop or condition, track through ;
				while(true) {
					doWhileLoopBody.add(hmTokenInfo.get(alToken.get(index)).getIdentity());		//get the token identity and add to the methodBody ArrayList
					if(hmTokenInfo.get(alToken.get(index)).getCatagory() == 71) {	//for category value check the CATEGORY INFORMATION FOLDER	
						break;		//since there is single statement, loop is breaked by a ;
					}
					index++;		//to handle the next token
				}
				index++; 	//cause method digging is completed and now we can focus on next portion
			}
		} else {
			do {
				
				if(hmTokenInfo.get(alToken.get(index)).getGenericCatagory() == 101) {	//if this is a loop(for/while/do while)
					
					index = loopHandle(index);
					continue;
				}
				
				/*
				 * If token is opening first parenthesis
				 * 		Then the identity will be pushed in stack
				 * Else if the token is closing closing first parenthesis 
				 * 		Then the the value is pop out from stack
				 * When the stack is empty then the parameter portion is finished
				*/
				if(hmTokenInfo.get(alToken.get(index)).getCatagory() == 24) {	
					stkBody.push(hmTokenInfo.get(alToken.get(index)).getIdentity());	//if a opening second parenthesis is found then it is pushed to stack
				} else if(hmTokenInfo.get(alToken.get(index)).getCatagory() == 23) {
					stkBody.pop();		//closing second parenthesis is found and popped operation from the stack
				}
				
				doWhileLoopBody.add(hmTokenInfo.get(alToken.get(index)).getIdentity());		//get the current token and then add to the methodBody Arraylist
				
				index++;	//to handle the next token
				
			} while(!stkBody.isEmpty());	//if the stack is empty then we finished the current method method body territorry i.e. finished
		
		}
		///////?///////////////////////////
		System.out.print("%$%$%$%$Body: ");
		printElements(doWhileLoopBody);
		
		return index;
	}
	
	private int loopHandle(int index) {
		System.out.println("Handling do while loop keyword: " + hmTokenInfo.get(alToken.get(index)).getKeyword());
		if(hmTokenInfo.get(alToken.get(index)).getCatagory() == 35) {	//this is a doWhile loop
			int doWhileLoopIdentifier = hmTokenInfo.get(alToken.get(index)).getIdentity();	//get the current loop identifier
			
			doWhileLoopBody.add(doWhileLoopIdentifier);	//adding do while loop identifier to the method body
			
			DoWhileLoopDigging doWhileLoopDigging = new DoWhileLoopDigging(token, doWhileLoopIdentifier);
			index = doWhileLoopDigging.digDoWhileLoop();	//retrive the first index after the current doWhile loop teritorry
			
			DoWhileLoopInfo doWhileLoopInfo = new DoWhileLoopInfo();	//received the current Do while loop info
			doWhileLoopInfo = doWhileLoopDigging.getDoWhileLoopInfo();	//get the do while loop info from the forLoopDigging class
			
			System.out.print("%$%$%$%%%%%%%%%%Condition: ");
			printElements(doWhileLoopInfo.getDoWhileLoopCondition());
			System.out.print("%$%$%$%%%%%%%%%%Body: ");
			printElements(doWhileLoopInfo.getDoWhileLoopBody());
			
			doWhileLoopHmDoWhileLoopInfo.put(doWhileLoopIdentifier, doWhileLoopInfo);	//now put the info according to the identifier 
		}
		
		return index;
	}
	
	private void printElements(ArrayList<Integer> alElements) {
		for(int index : alElements) {
			TokenInfo ti = new TokenInfo();
			ti = hmTokenInfo.get(alToken.get(index));
			System.out.print(ti.getKeyword() + " ");
		}
		System.out.println();
	}

}
