package com.ShamsNahid.Loop.For;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import com.ShamsNahid.Generic.GenericDigging;
import com.ShamsNahid.Generic.GenericInfo;
import com.ShamsNahid.Helper.Utility;
import com.ShamsNahid.Loop.DoWhile.DoWhileLoopDigging;
import com.ShamsNahid.Loop.DoWhile.DoWhileLoopInfo;
import com.ShamsNahid.Loop.While.WhileLoopDigging;
import com.ShamsNahid.Loop.While.WhileLoopInfo;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;

public class ForLoopDigging {
	
	private Token token;	//we will receive this through constructor
	private ArrayList<Integer> alToken; 	//Then all token will be kept here
	private HashMap<Integer, TokenInfo> hmTokenInfo;	//Each token info is stored here for regarding its id

	private ForLoopInfo forLoopInfo;	//bitch here 
		
	private Integer forLoopIdentifier;	//receive from methodDigging class
	
	GenericDigging genericDigging;
	
	//GETTER
	public ForLoopInfo getForLoopInfo()  {
		return forLoopInfo;
	}
	
	public ForLoopDigging(Token token, Integer forLoopIdentifier) {
		this.token = token;
		this.alToken = token.getToken();
		this.hmTokenInfo = token.getHm();
		this.forLoopIdentifier = forLoopIdentifier;
		
		forLoopInfo = new ForLoopInfo();
	}
	
	//for loop digging placed here
	public int digForLoop() {
		
		genericDigging = new GenericDigging(token, forLoopIdentifier);
		int index = genericDigging.digGenericInfo();	//the last identifier (index + 1) of the loop is store in the index
		GenericInfo genericInfo = genericDigging.getgenericInfo();
		forLoopInfo.setGenericInfo(genericInfo);
		
		System.out.println("//Head Portion of  a loop: ");
		new Utility().printGenericElements(genericInfo.getHeadPortion(), hmTokenInfo, alToken);
		System.out.println("//Filter body of  a loop: ");
		new Utility().printGenericElements(genericInfo.getFilterBody(), hmTokenInfo, alToken);
		
		return index;	//the next index of the for loop territory is placed here
	}
	
	

}
