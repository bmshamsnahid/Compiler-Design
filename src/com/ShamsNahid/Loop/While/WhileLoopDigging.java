package com.ShamsNahid.Loop.While;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import com.ShamsNahid.Generic.GenericDigging;
import com.ShamsNahid.Generic.GenericInfo;
import com.ShamsNahid.Helper.Utility;
import com.ShamsNahid.Loop.DoWhile.DoWhileLoopDigging;
import com.ShamsNahid.Loop.DoWhile.DoWhileLoopInfo;
import com.ShamsNahid.Loop.For.ForLoopDigging;
import com.ShamsNahid.Loop.For.ForLoopInfo;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;

public class WhileLoopDigging {
	
	private Token token;	//we will receive this through constructor
	private ArrayList<Integer> alToken; 	//Then all token will be kept here
	private HashMap<Integer, TokenInfo> hmTokenInfo;	//Each token info is stored here for regarding its id

	private WhileLoopInfo whileLoopInfo;	//bitch here 
		
	private Integer whileLoopIdentifier;	//receive from methodDigging class
	
	GenericDigging genericDigging;
	
	//GETTER
	public WhileLoopInfo getWhileLoopInfo()  {
		return whileLoopInfo;
	}
	
	public WhileLoopDigging(Token token, Integer whileLoopIdentifier) {
		this.token = token;
		this.alToken = token.getToken();
		this.hmTokenInfo = token.getHm();
		this.whileLoopIdentifier = whileLoopIdentifier;
		
		whileLoopInfo = new WhileLoopInfo();
	}
	
	//while loop digging placed here
	public int digWhileLoop() {
		
		genericDigging = new GenericDigging(token, whileLoopIdentifier);
		int index = genericDigging.digGenericInfo();	//the last identifier (index + 1) of the loop is store in the index
		GenericInfo genericInfo = genericDigging.getgenericInfo();
		whileLoopInfo.setGenericInfo(genericInfo);
		
		System.out.println("//Head Portion of  a while loop: ");
		new Utility().printGenericElements(genericInfo.getHeadPortion(), hmTokenInfo, alToken);
		System.out.println("//Filter body of  a while loop: ");
		new Utility().printGenericElements(genericInfo.getFilterBody(), hmTokenInfo, alToken);
		
		return index;	//the next index of the for loop territory is placed here
	}

}
