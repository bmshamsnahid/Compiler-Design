package com.ShamsNahid.Loop.While;

import com.ShamsNahid.Generic.GenericInfo;

public class WhileLoopInfo {
	
	private GenericInfo genericInfo;

	public GenericInfo getGenericInfo() {
		return genericInfo;
	}

	public void setGenericInfo(GenericInfo genericInfo) {
		this.genericInfo = genericInfo;
	} 
}
