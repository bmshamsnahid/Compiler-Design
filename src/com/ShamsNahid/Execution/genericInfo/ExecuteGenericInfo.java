package com.ShamsNahid.Execution.genericInfo;

import java.util.ArrayList;
import java.util.HashMap;

import com.ShamsNahid.Condition.Else.ElseConditionInfo;
import com.ShamsNahid.Condition.ElseIf.ElseIfConditionInfo;
import com.ShamsNahid.Condition.IF.IfConditionInfo;
import com.ShamsNahid.Execution.CheckCondition;
import com.ShamsNahid.Execution.LoopExecution.ForLoopExecution;
import com.ShamsNahid.Execution.genericInfo.childProperties.statement.ExecuteAssignmentStatement;
import com.ShamsNahid.Execution.genericInfo.childProperties.statement.ExecuteDeclerationStatement;
import com.ShamsNahid.Generic.GenericInfo;
import com.ShamsNahid.Helper.Utility;
import com.ShamsNahid.Execution.LibraryMethod.Display;
import com.ShamsNahid.Execution.LibraryMethod.Input;
import com.ShamsNahid.Loop.For.ForLoopInfo;
import com.ShamsNahid.Objects.*;
import com.ShamsNahid.Objects.Assignment.ObjectAssignment;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;

public class ExecuteGenericInfo {
	
	private GenericInfo genericInfo;
	private Token token;	//also need all the token list identifier
	
	private Integer endIndex;

	private HashMap<Integer, TokenInfo> hmTokenInfo;	//all the tokens info is placed according to their identifier
	private ArrayList<Integer> alToken;	//tokens identifier list

	private Utility utility;
	
	public Integer getEndIndex() { return this.endIndex; }
	
	public ExecuteGenericInfo(GenericInfo genericInfo, Token token) {
		this.genericInfo = genericInfo;	//store the genericINfo that will execute here
		this.token = token;	//store in the local variable
		hmTokenInfo = token.getHm();	//retrieve the tokens identity and info
		alToken = token.getToken();	//got the tokens identity

		utility = new Utility();
		
		execute();
	}
	
	public void execute() {

        ArrayList<ArrayList<Integer>> statementList = genericInfo.getStatementList();

		for(int index=0; index<statementList.size(); index++) {
			ArrayList<Integer> statement = statementList.get(index);
			TokenInfo tokenInfo = hmTokenInfo.get(statement.get(0));	//get the token info of the first identity of the statement

            /*
			 * A decalration statement always start with _dec
			*/
			if(tokenInfo.getCatagory() == 11) {	//if this is a decleration statement
				ExecuteDeclerationStatement executeDeclerationStatement = new ExecuteDeclerationStatement(token, statement, genericInfo.getMyObject());
				executeDeclerationStatement.execute();
				MyObject myObjectContainer = executeDeclerationStatement.getNewObjectContainer();
				//merging the newly generated objects
				genericInfo.getMyObject().getAlObjectList().addAll(myObjectContainer.getAlObjectList());
				genericInfo.getMyObject().getHmObjectInfo().putAll(myObjectContainer.getHmObjectInfo());
                continue;
            }

			/*
			 * Start with a variable name, so it's a assignment statement
			*/
			ObjectUtility objectUtility = new ObjectUtility(token, genericInfo.getMyObject().getAlObjectList(), genericInfo.getMyObject().getHmObjectInfo());
			if(objectUtility.isParentObject(tokenInfo.getKeyword())) {
				ExecuteAssignmentStatement executeAssignmentStatement = new ExecuteAssignmentStatement(token, statement, genericInfo.getMyObject());
				executeAssignmentStatement.execute();
				genericInfo.setMyObject(executeAssignmentStatement.getNewObjectContainer());
				continue;
			}
			
			/**
			 * If condition execution
			*/
			if(tokenInfo.getCatagory() == 32) {

                IfConditionInfo ifConditionInfo = genericInfo.getHmIfConditionInfo().get(statement.get(0));
				CheckCondition checkCondition = new CheckCondition(ifConditionInfo.getGenericInfo().getHeadPortion(), token, genericInfo.getMyObject().getAlObjectList(), genericInfo.getMyObject().getHmObjectInfo());
				if(checkCondition.getFlag()) {
				    ifConditionInfo.getGenericInfo().setMyObject(genericInfo.getMyObject());    //provide the global objects to the child objects
					new ExecuteGenericInfo(ifConditionInfo.getGenericInfo(), token);    //execute the genericInfo of the if condition
				}
			}
			/**
			 * Else condition execution
			*/
			if(tokenInfo.getCatagory() == 34) {
				ElseConditionInfo elseConditionInfo = genericInfo.getHmElseConditionInfo().get(statement.get(0));
				elseConditionInfo.getGenericInfo().setMyObject(genericInfo.getMyObject());
				new ExecuteGenericInfo(elseConditionInfo.getGenericInfo(), token);
			}
			/**
			 * Else If condition execution
			*/
			if(tokenInfo.getCatagory() == 36) {
				ElseIfConditionInfo elseIfConditionInfo = genericInfo.getHmElseIfConditionInfo().get(statement.get(0));
				CheckCondition checkCondition = new CheckCondition(elseIfConditionInfo.getGenericInfo().getHeadPortion(), token, genericInfo.getMyObject().getAlObjectList(), genericInfo.getMyObject().getHmObjectInfo());
				if(checkCondition.getFlag()) {
					elseIfConditionInfo.getGenericInfo().setMyObject(genericInfo.getMyObject());
				    new ExecuteGenericInfo(elseIfConditionInfo.getGenericInfo(), token);
				}
			}
			
			if(tokenInfo.getCatagory() == 31) {
				//System.out.println("Encounter a for loop. " + hmTokenInfo.get(statement.get(0)).getKeyword() + " " + statement.get(0));
				ForLoopInfo forLoopInfo = genericInfo.getHmForLoopInfo().get(statement.get(0));
				//System.out.println("Body: " + forLoopInfo.getBody());
                forLoopInfo.getGenericInfo().setMyObject(genericInfo.getMyObject());
				ForLoopExecution forLoopExecution = new ForLoopExecution(forLoopInfo, genericInfo.getMyObject().getAlObjectList(), genericInfo.getMyObject().getHmObjectInfo(), token);
				forLoopExecution.execute();
			}
			
			/*
			 * Start with a method name, i.e invoking a method
			 * if contains in the library method, then it's the library method
			*/
			if(utility.getAlLibraryFunctionList().contains(tokenInfo.getKeyword())) {
                if(tokenInfo.getCatagory() == 90) {		//display method
                    new Display(genericInfo.getMyObject().getAlObjectList(), genericInfo.getMyObject().getHmObjectInfo(), token, statement);
				} if(tokenInfo.getCatagory() == 91) {	//input method
					new Input(genericInfo.getMyObject().getAlObjectList(), genericInfo.getMyObject().getHmObjectInfo(), token, statement);
				}
			}
		}
	}

}
