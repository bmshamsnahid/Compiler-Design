package com.ShamsNahid.Execution.genericInfo.childProperties.statement;

import com.ShamsNahid.Helper.Utility;
import com.ShamsNahid.Objects.Assignment.ObjectAssignment;
import com.ShamsNahid.Objects.MyObject;
import com.ShamsNahid.Objects.ObjectInfo;
import com.ShamsNahid.Objects.ObjectTracking;
import com.ShamsNahid.Objects.ObjectUtility;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;

import java.util.ArrayList;
import java.util.HashMap;

public class ExecuteAssignmentStatement {

    private Token token;	//also need all the token list identifier

    private HashMap<Integer, TokenInfo> hmTokenInfo;	//all the tokens info is placed according to their identifier
    private ArrayList<Integer> alToken;	//tokens identifier list

    private Utility utility;

    private ArrayList<Integer> statement;
    private MyObject myObject;

    private MyObject newObjectContainer;

    public MyObject getNewObjectContainer() {
        return newObjectContainer;
    }

    public ExecuteAssignmentStatement(Token token, ArrayList<Integer> statement, MyObject myObject) {
        this.token = token;	//store in the local variable
        this.statement = statement;
        this.myObject = myObject;

        hmTokenInfo = token.getHm();	//retrieve the tokens identity and info
        alToken = token.getToken();	//got the tokens identity

        utility = new Utility();
    }

    public void execute() {
        TokenInfo tokenInfo = hmTokenInfo.get(statement.get(0));	//get the token info of the first identity of the statement
        ObjectUtility objectUtility = new ObjectUtility(token, myObject.getAlObjectList(), myObject.getHmObjectInfo());
        if(objectUtility.isParentObject(tokenInfo.getKeyword())) {
            ArrayList<Integer> alObjectValueExpression = new ArrayList<>();
            for(Integer statementIndex = 0; statementIndex<statement.size(); statementIndex++) {
                TokenInfo ti;
                ti = hmTokenInfo.get(alToken.get(statement.get(statementIndex)));
                if(ti.getCatagory() == 40) break;
                alObjectValueExpression.add(statement.get(statementIndex));
            }
            ObjectTracking objectTracking = new ObjectTracking(alObjectValueExpression, token, myObject.getAlObjectList(), myObject.getHmObjectInfo());
            objectTracking.trackObject(0);
            ObjectInfo objectInfo = objectTracking.getObjectInfo();
            if(objectInfo != null) {
                ObjectAssignment objectAssignment = new ObjectAssignment(token, myObject, objectTracking.getObjectInfo());
                objectAssignment.assignObject(statement);
                newObjectContainer = objectAssignment.getNewObjectContainer();
            }
        }
    }
}
