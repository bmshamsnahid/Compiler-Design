package com.ShamsNahid.Execution.genericInfo.childProperties.statement;

import com.ShamsNahid.Helper.Utility;
import com.ShamsNahid.Objects.MyObject;
import com.ShamsNahid.Objects.ObjectDigging;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Here we receive a decleretion statement(aka start with _dec)
 * we will return the obeject container, the newly created objects
*/

public class ExecuteDeclerationStatement {

    private Token token;	//also need all the token list identifier

    private HashMap<Integer, TokenInfo> hmTokenInfo;	//all the tokens info is placed according to their identifier
    private ArrayList<Integer> alToken;	//tokens identifier list

    private Utility utility;

    private ArrayList<Integer> statement;
    private MyObject myObject;

    private MyObject newObjectContainer;

    public MyObject getNewObjectContainer() {
        return newObjectContainer;
    }

    public ExecuteDeclerationStatement(Token token, ArrayList<Integer> statement, MyObject myObject) {
        this.token = token;	//store in the local variable
        this.statement = statement;
        this.myObject = myObject;

        hmTokenInfo = token.getHm();	//retrieve the tokens identity and info
        alToken = token.getToken();	//got the tokens identity

        utility = new Utility();
    }

    public void execute() {
        TokenInfo tokenInfo = hmTokenInfo.get(statement.get(0));	//get the token info of the first identity of the statement

        ObjectDigging objectDigging = new ObjectDigging(token);   //instantiate the objectDigging object
        //since this is parentail sectiom, object are parent itself, add the 0
        ArrayList<Integer> alParentTree = new ArrayList<>();
        alParentTree.add(0);
        objectDigging.cookObject(tokenInfo.getIdentity(), myObject, alParentTree);

        //merging the newly generated objects
        newObjectContainer = objectDigging.getNewObjectContainer();
    }
}
