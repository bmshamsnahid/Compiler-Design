package com.ShamsNahid.Execution.LibraryMethod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import com.ShamsNahid.Helper.Utility;
import com.ShamsNahid.Objects.ObjectInfo;
import com.ShamsNahid.Objects.ObjectTracking;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;

public class Input {
	
	private ArrayList<Integer> alObjectList;
	private HashMap<Integer, ObjectInfo> hmObjectInfo;
	
	private Token token;
	private ArrayList<Integer> alToken;
	private HashMap<Integer, TokenInfo> hmTokenInfo;
	
	private ArrayList<Integer> statement;
	
	private ArrayList<String> parameterToDisplay;
	private String parameterAsString;
	
	private Utility utility;
	
	public Input(ArrayList<Integer> alObjectList, HashMap<Integer, ObjectInfo> hmObjectInfo, Token token, ArrayList<Integer> statement) {
		
		this.alObjectList = alObjectList;
		this.hmObjectInfo = hmObjectInfo;
		this.token = token;
		this.statement = statement;
		
		alToken = token.getToken();
		hmTokenInfo = token.getHm(); 
		
		utility = new Utility();
		
		digParameter();
	}

	private void digParameter() {
		statement.remove(0);
		statement.remove(0);
		statement.remove(statement.size()-1);
		statement.remove(statement.size()-1);
		
		ArrayList<Integer> unitObjectList = new ArrayList<>();
		
		for(Integer index=0; index<statement.size(); index++) {
			TokenInfo tokenInfo = hmTokenInfo.get(alToken.get(statement.get(index)));
			//System.out.print(tokenInfo.getKeyword());
			if(tokenInfo.getCatagory() == 70 || index == statement.size()-1) {
				if(index == statement.size()-1) {
					unitObjectList.add(tokenInfo.getIdentity());
				}
				
				ObjectTracking objectTracking = new ObjectTracking(unitObjectList, token, alObjectList, hmObjectInfo);
				objectTracking.trackObject(0);
				ObjectInfo objectInfo = objectTracking.getObjectInfo();
				
				Scanner scanner = new Scanner(System.in);
				String str = scanner.next();
				objectInfo.setObjectValue(str);
				
				unitObjectList = new ArrayList<>();
				
				continue;
			} else {
				unitObjectList.add(tokenInfo.getIdentity());
			}
		}
	}
}
