package com.ShamsNahid.Execution.LibraryMethod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.ShamsNahid.Helper.Utility;
import com.ShamsNahid.Objects.ObjectTracking;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;
import com.ShamsNahid.Objects.ObjectInfo;

/*
 * This is a display method, library method
 * It is invoked like, Display("Some String" + variableName + some Double value + other stuff)
 * Constructor will get the necessary information, like display statement, token, variable and their info
 * getParameter() method will retrieve the information from the opening first parenthesis including the plus sign and cottation
 * makeString() method will remove the plus sign and cottation from the string value 
*/
public class Display {
	private ArrayList<Integer> alObjectList;
	private HashMap<Integer, ObjectInfo> hmObjectInfo;
	
	private Token token;
	private ArrayList<Integer> alToken;
	private HashMap<Integer, TokenInfo> hmTokenInfo;
	
	private ArrayList<Integer> statement;
	
	private ArrayList<String> parameterToDisplay;
	private String parameterAsString;
	
	private Utility utility;
	
	/*
	 * CONSTRUCTOR
	 * Got the necessary inforamtion, 
	 * invoke method to get the parameter and made them string
	 * Finally mc task print it
	*/
	public Display(ArrayList<Integer> alObjectList, HashMap<Integer, ObjectInfo> hmObjectInfo, Token token, ArrayList<Integer> statement) {
		
		this.alObjectList = alObjectList;
		this.hmObjectInfo = hmObjectInfo;
		this.token = token;

		this.statement = new ArrayList<>();
		this.statement.addAll(statement);

		alToken = token.getToken();
		hmTokenInfo = token.getHm();
		
		utility = new Utility();
		
		digParameter();
	}
	
	/*
	 * Here from the method statement, we retrieve the method parameter portion
	 * 
	*/
	private void digParameter() {
		/*System.out.println("Before removing the first and last pair of token: ");
		utility.printGenericElements(statement, hmTokenInfo, alToken);*/

		statement.remove(0); //remove the Display keyword
		statement.remove(0);	//remove the opening first parenthesis
		statement.remove(statement.size() - 1);	 //remove the last closing first parenthesis
		statement.remove(statement.size() - 1);	 //remove the semicolon, end of the statement

		ArrayList<Integer> unitObjectList = new ArrayList<>();
		
		for(Integer index=0; index<statement.size(); index++) {
			TokenInfo tokenInfo = hmTokenInfo.get(alToken.get(statement.get(index)));
			if(tokenInfo.getCatagory() == 41 || index == statement.size()-1) { //if this is the plus sign or end of the statement
				if(index == statement.size()-1) {	//if end of the statement
					unitObjectList.add(tokenInfo.getIdentity());
				}
				ObjectTracking objectTracking = new ObjectTracking(unitObjectList, token, alObjectList, hmObjectInfo);
				objectTracking.trackObject(0);
				ObjectInfo objectInfo = objectTracking.getObjectInfo();

				if(objectInfo != null) {
					if(objectInfo.getObjectCategory() == 3101) {
						System.out.print(objectInfo.getObjectValue());
					} else {
						System.out.print("nested object");
					}
				} else {
					for(Integer index2=0; index2<unitObjectList.size(); index2++) {
						TokenInfo ti = hmTokenInfo.get(unitObjectList.get(index2));
						printKeyword(ti.getKeyword());
					}
				}
				//System.out.println("Display: ------------------------------- : clearing the unit object list");
				unitObjectList = new ArrayList<>();
			} else {
				unitObjectList.add(tokenInfo.getIdentity());
			}
		}
	}


	private void printKeyword(String tokenKeyword) {
		String formattedString;
		if(tokenKeyword.charAt(0) == '"' && tokenKeyword.charAt(tokenKeyword.length() - 1) == '"') {
			StringBuilder stringBuilder = new StringBuilder(tokenKeyword);
			stringBuilder.deleteCharAt(0);
			stringBuilder.deleteCharAt(tokenKeyword.length() - 2);
			formattedString = stringBuilder.toString();
		} else {
			formattedString = tokenKeyword;
		}

		List<String> alFormattedString = Arrays.asList(formattedString.split(""));
		for(Integer index=0; index<alFormattedString.size(); index++) {

			if(index != alFormattedString.size()-1 && alFormattedString.get(index).equals("\\") && alFormattedString.get(index+1).equals("n")) {
				index++;
				System.out.println();
			} else {
				System.out.print(alFormattedString.get(index));
			}
		}
	}

}
