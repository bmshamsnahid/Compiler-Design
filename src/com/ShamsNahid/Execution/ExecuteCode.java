package com.ShamsNahid.Execution;

import java.util.ArrayList;
import java.util.HashMap;

import com.ShamsNahid.Execution.genericInfo.ExecuteGenericInfo;
import com.ShamsNahid.Generic.GenericInfo;
import com.ShamsNahid.Helper.Utility;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.method.MethodInfo;
import com.ShamsNahid.ParentContainer.ParentContainerInfo;

public class ExecuteCode {
	
	private ParentContainerInfo parentContainerInfo;	//first we get the symbol table
	private Token token;	//also need all the token list identifier

	private Utility utility;
	private ExecuteGenericInfo executeGenericInfo;
	
	public ExecuteCode(ParentContainerInfo parentContainerInfo, Token token) {	//receive symbol table and the token
		System.out.println("|-------------------------------------------------------------------------------------------------------------------------|");
		System.out.println("|START EXECUTION------------------START EXECUTION----------------------START EXECUTION|");
		System.out.println("|------------------------------------------------------------------------------------------------------------------------ |");
		
		this.parentContainerInfo = parentContainerInfo;	//store in the local variable
		this.token = token;	//store in the local variable
		
		utility = new Utility();
	}
	
	/*
	 * Actually symbol table contains the method name now
	 * Hence here we get each of the method identity and call the print their generic info
	 * using the printGenericInfo method 
	*/
	public void execute() {
		ArrayList<Integer> alSymbolTable = parentContainerInfo.getAlMethods();
		HashMap<Integer, MethodInfo> hmMethodInfo = parentContainerInfo.getHmMethodInfo();
		
		for(Integer index : alSymbolTable) {
			MethodInfo methodInfo = hmMethodInfo.get(index);
			GenericInfo genericInfo = methodInfo.getGenericInfo();
			
			executeGenericInfo = new ExecuteGenericInfo(genericInfo, token);
		}
		
		System.out.println("|-------------------------------------------------------------------------------------------------------------------------|");
		System.out.println("|END EXECUTION----------------------END EXECUTION---------------------------END EXECUTION|");
		System.out.println("|------------------------------------------------------------------------------------------------------------------------ |");
	}

}
