package com.ShamsNahid.Execution.LoopExecution;

import com.ShamsNahid.Execution.CheckCondition;
import com.ShamsNahid.Execution.genericInfo.ExecuteGenericInfo;
import com.ShamsNahid.Helper.Utility;
import com.ShamsNahid.Loop.For.ForLoopInfo;
import com.ShamsNahid.Objects.*;
import com.ShamsNahid.Objects.Assignment.ObjectAssignment;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by bmshamsnahid on 6/24/17.
 */
public class ForLoopExecution {
	
	private ForLoopInfo forLoopInfo;
	private ArrayList<Integer> alObjectList;
	private HashMap<Integer, ObjectInfo> hmObjectInfo;
	private Token token;
	private ArrayList<Integer> alToken;
	private HashMap<Integer, TokenInfo> hmTokenInfo;
	private Utility utility;
	
	public ForLoopExecution(ForLoopInfo forLoopInfo, ArrayList<Integer> alObjectList, HashMap<Integer, ObjectInfo> hmObjectInfo, Token token) {

		this.forLoopInfo = new ForLoopInfo();
		
		this.alObjectList = new ArrayList<>();
		this.hmObjectInfo = new HashMap<>();
		
		this.token = new Token();
		this.alToken = new ArrayList<>();
		this.hmTokenInfo = new HashMap<>();
		
		this.utility = new Utility();
		
		this.forLoopInfo = forLoopInfo;
		this.alObjectList.addAll(alObjectList);
		this.hmObjectInfo.putAll(hmObjectInfo);
		
		this.token = token;
		this.alToken.addAll(token.getToken());
		this.hmTokenInfo.putAll(token.getHm());
	}
	
	public void execute() {
        /**
         * 0 index contains the initial value
         * 1 index contains the condition
         * 2 index contains the next value
        */
        ArrayList<ArrayList<Integer>> alHeadStatements = getSeperatedStatements(forLoopInfo.getGenericInfo().getHeadPortion());

        //GET THE INITIAL OBJECT AND UPDATE THE GENERIC INFO WITH IT
        ObjectInfo initialValueObjectInfo = getObjectInfo(alHeadStatements.get(0));
        //GENERIC OBJECT INFO IS UPDATING WITH THE NEW INITIALIZED OBJECT
        MyObject myObject = updateGenericObject(initialValueObjectInfo);

        while(checkLoopCondition(alHeadStatements.get(1), myObject) == true) {
            ExecuteGenericInfo executeGenericInfo1 = new ExecuteGenericInfo(forLoopInfo.getGenericInfo(), token);
            ObjectInfo newObjectInfo = operateNewValue(alHeadStatements.get(2), myObject);
        }
    }

    private ObjectInfo getObjectInfo(ArrayList<Integer> alStatement) {
	    TokenInfo tokenInfo = new TokenInfo();
	    tokenInfo = hmTokenInfo.get(alToken.get(alStatement.get(0)));

	    if(tokenInfo.getCatagory() == 11) { //declaration statement
            ObjectDigging objectDigging = new ObjectDigging(token);   //instantiate the objectDigging object
            ArrayList<Integer> alParentTree = new ArrayList<>();
            alParentTree.add(0);
            objectDigging.cookObject(tokenInfo.getIdentity(), forLoopInfo.getGenericInfo().getMyObject(), alParentTree);

            MyObject myObjectContainer = objectDigging.getNewObjectContainer();
            ObjectInfo objectInfo = myObjectContainer.getHmObjectInfo().get(myObjectContainer.getAlObjectList().get(0));    //cause there's only one object
            return objectInfo;
        } else {    //assignment statement
            ObjectTracking objectTracking = new ObjectTracking(alStatement, token, forLoopInfo.getGenericInfo().getMyObject().getAlObjectList(), forLoopInfo.getGenericInfo().getMyObject().getHmObjectInfo());
            objectTracking.trackObject(0);
            ObjectInfo objectInfo = objectTracking.getObjectInfo();
            return objectInfo;
        }
    }

    private boolean isObjectExist(ObjectInfo objectInfo, Integer objectId) {
        if(objectInfo.getObjectCategory() == 3101) {
            if(objectInfo.getObjectId() == objectId) {
                return true;
            }
        } else if(objectInfo.getObjectCategory() == 3102) {
            for(Integer propertyObjectsId : objectInfo.getAlPropertyObjectsId()) {
                ObjectInfo propertyObjectsInfo = objectInfo.getHmPropertyObjectInfo().get(propertyObjectsId);
                if(isObjectExist(propertyObjectsInfo, objectId) == true) {
                    return true;
                }
            }
        }
	    return false;
    }

    //=======================================================================================================
    //HERE CHECK THE MIDDLE WARE CONDITION OF THE LOOP
    //=======================================================================================================
    private boolean checkLoopCondition(ArrayList<Integer> alHeadCondition, MyObject myObject) {
        //alHeadCondition.remove(alHeadCondition.size() - 1);

        CheckCondition checkCondition = new CheckCondition(alHeadCondition, token, myObject.getAlObjectList(), myObject.getHmObjectInfo());

        if(checkCondition.getFlag()) {
            return true;
        } else {
            return false;
        }
    }

    //=======================================================================================================
    //HERE WE SEPERATE THE HEAD SECTION IN 3 BASCIS PORTION, INITIALIZE PART, CONDITIONAL PART, INCREASING SECTION
    //=======================================================================================================
    private ArrayList<ArrayList<Integer>> getSeperatedStatements(ArrayList<Integer> alHeadSection) {
        alHeadSection.remove(0);    //remove the first parenthesis
        alHeadSection.remove(alHeadSection.size()-1);   //remove the last parenthesis

        ArrayList<ArrayList<Integer>> alHeadStatements = new ArrayList<>();

        ArrayList<Integer> alTemp = new ArrayList<>();

        for(Integer index=0; index<alHeadSection.size(); index++) {
            TokenInfo tokenInfo = hmTokenInfo.get(alToken.get(alHeadSection.get(index)));
            if(tokenInfo.getCatagory() == 71) {   //if this is a semicolon
                alTemp.add(alHeadSection.get(index));
                alHeadStatements.add(alTemp);
                alTemp = new ArrayList<>();
            } else if(index == alHeadSection.size()-1) {    //if this is the last statement
                alTemp.add(alHeadSection.get(index));
                alHeadStatements.add(alTemp);
            } else {
                alTemp.add(alHeadSection.get(index));
            }
        }

        //remove semicolon from the end of consitional statements
        alHeadStatements.get(1).remove(alHeadStatements.get(1).size() - 1);

        /*System.out.println("For Loop Seperated head section:");
        for(ArrayList<Integer> alUnitHeadSection: alHeadStatements) {
            System.out.println("===============================================================================");
            for(Integer index=0; index<alUnitHeadSection.size(); index++) {
                System.out.print(hmTokenInfo.get(alToken.get(alUnitHeadSection.get(index))).getKeyword() + " ");
            }
            System.out.println();
        }
        System.out.println("===============================================================================");*/

        return alHeadStatements;
    }

    //=======================================================================================================
    //HERE WE UPDATE THE CURRENT GENERIC INFO WITH THE NEW INITIALIZED OBJECT
    //=======================================================================================================
    private MyObject updateGenericObject(ObjectInfo initialValueObjectInfo) {
	    MyObject myObject = new MyObject();
        myObject.setAlObjectList(forLoopInfo.getGenericInfo().getMyObject().getAlObjectList());
        myObject.setHmObjectInfo(forLoopInfo.getGenericInfo().getMyObject().getHmObjectInfo());

        boolean isObjectAlreadyDeclared = false;
        for(Integer objectId: myObject.getAlObjectList()) {
            ObjectInfo objectInfo = hmObjectInfo.get(objectId);
            if(isObjectExist(objectInfo, initialValueObjectInfo.getObjectId())) {
                isObjectAlreadyDeclared = true;
            }
        }

        /**
         * If the loop head section, provide a newly created object then we have to add it in the for loop generic info
         * scope.
         */
        if(isObjectAlreadyDeclared == true) {   //object already exists
            //System.out.println("Object was already declared.");
        } else {    //this is a new created object
            System.out.println("Object was not declared on previous step.");
            myObject.getAlObjectList().add(initialValueObjectInfo.getObjectId());
            myObject.getHmObjectInfo().put(initialValueObjectInfo.getObjectId(), initialValueObjectInfo);
        }
        return myObject;
    }
    //=======================================================================================================
    //HERE WE GET THE NEW VALUE AND UPDATE THE GENERIC INFO OBJECT
    //=======================================================================================================
    private ObjectInfo operateNewValue(ArrayList<Integer> alValueChangeSection, MyObject myObject) {
        //System.out.println("Value Change section: ");
        ObjectInfo objectInfo = new ObjectInfo();

        TokenInfo tokenInfo = hmTokenInfo.get(alToken.get(alValueChangeSection.get(0)));
        //System.out.println("First object: " + tokenInfo.getKeyword());
        ObjectUtility objectUtility = new ObjectUtility(token, myObject.getAlObjectList(), myObject.getHmObjectInfo());
        if (objectUtility.isParentObject(tokenInfo.getKeyword())) {
            ArrayList<Integer> alObjectValueExpression = new ArrayList<>();
            for (Integer statementIndex = 0; statementIndex < alValueChangeSection.size(); statementIndex++) {
                TokenInfo ti;
                ti = hmTokenInfo.get(alToken.get(alValueChangeSection.get(statementIndex)));
                //System.out.println("ti name: " + ti.getKeyword());
                if (ti.getCatagory() == 40) break;
                alObjectValueExpression.add(alValueChangeSection.get(statementIndex));
            }
            ObjectTracking objectTracking = new ObjectTracking(alObjectValueExpression, token, myObject.getAlObjectList(), myObject.getHmObjectInfo());
            objectTracking.trackObject(0);
            objectInfo = objectTracking.getObjectInfo();
            //System.out.println(">>>>>>>>>>>>object: " + objectInfo.getObjectName() + " value: " + objectInfo.getObjectValue());
            if(objectInfo != null) {
                ObjectAssignment objectAssignment = new ObjectAssignment(token, myObject, objectTracking.getObjectInfo());
                objectAssignment.assignObject(alValueChangeSection);
                objectInfo = objectAssignment.getRetrievedObjectInfo();
                //System.out.println(">>>>>>>>>>>>object: " + objectInfo.getObjectName() + " value: " + objectInfo.getObjectValue());
            }
        } else {

        }
	    return objectInfo;
    }
    //=======================================================================================================
    //HERE CHECK THE MIDDLE WARE CONDITION OF THE LOOP
    //=======================================================================================================
    //=======================================================================================================
    //HERE CHECK THE MIDDLE WARE CONDITION OF THE LOOP
    //=======================================================================================================
	
}