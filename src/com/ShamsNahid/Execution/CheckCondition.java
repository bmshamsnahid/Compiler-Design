package com.ShamsNahid.Execution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Stack;

import com.ShamsNahid.Helper.ExpressionEvaluation;
import com.ShamsNahid.Helper.Utility;
import com.ShamsNahid.Objects.ObjectTracking;
import com.ShamsNahid.Objects.ObjectUtility;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;
import com.ShamsNahid.Objects.ObjectInfo;

/*
 *Here we get a statement with parenthesis on the both side and return true or false
 *The true and false is determined by 1 and 0
 *The true false is stored in the flag variable and a getter will set to reurn it
 *
 * In constructor we get the tokens and variable info with the conditiojnal statem,ent
 * Initialize them and utility class
 * 
 * Constructor invoke the check method, that will determine if the condition is true or false
 * the resulting value will be returned(0 for false, 1 for true) and store it in the flag
 * 
 * The check method separate each of the unit of the condition, like from (a==b) && c, ub=nit condition is (a==b)
 * then the unit condition is send to the miniUnit result to determine if this is true or false
 * 
 * the miniUnitResult() receive the operator and its pre and post value
 * according to the operator, we determine if the unit condition is true or false and return the result value 
*/
public class CheckCondition {
	
	private ArrayList<Integer> conditionStatement;	//we dig this statement
	private ArrayList<String> alConditionalOperatorList;	//contains the condition al operators, like <. <=. == etc
	private ArrayList<String> alOperatorList;	//contains the operator, like +, - etc	
	private Stack<String> stack; // to track the unit parenthesis	
	private boolean flag;	//MC object, return true or false according to the condition 
	
	private Token token;	//also need all the token list identifier

	private HashMap<Integer, TokenInfo> hmTokenInfo;	//all the tokens info is placed according to their identifier
	private ArrayList<Integer> alToken;	//tokens identifier list
	
	private HashMap<Integer, ObjectInfo> hmObjectInfo;	//for each variable name, the variabble info stored here
	private ArrayList<Integer> alObjectList;	//all the variable of the parent vaseal is stored here
	
	private Utility utility;	//for get various keyword list and evaluation
	ObjectUtility objectUtility;
	
	//GETTER for the flag
	public boolean getFlag() {
		return flag;
	}

	public CheckCondition() {
	}

	/*
         * Get all the information about the statement and also token, variabble list
         * retrieve the token list and token info,
         * initialize the utility class
         * finally invoke the check method, to get the result of the condition
        */
	public CheckCondition(ArrayList<Integer> conditionStatement, Token token, ArrayList<Integer> alObjectList, HashMap<Integer, ObjectInfo> hmObjectInfo) {
		this.conditionStatement = conditionStatement;
		
		this.token = token;	//store in the local variable
		hmTokenInfo = token.getHm();	//retrieve the tokens identity and info
		alToken = token.getToken();	//got the tokens identity
		
		this.alObjectList = alObjectList;
		this.hmObjectInfo = hmObjectInfo;
		
		utility = new Utility();
		alConditionalOperatorList = utility.getAlConditionalOperatorList();
		alOperatorList = utility.getAlOperatorList();
		
		objectUtility = new ObjectUtility(this.token, this.alObjectList, this.hmObjectInfo);


		/*//#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		System.out.println("Received condition statement: ");
		for(Integer identity: conditionStatement) {
			System.out.print(hmTokenInfo.get(alToken.get(identity)).getKeyword() + " ");
		}
		System.out.println();
        //#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

        flag = check();	//we get, if the condition is true or false
	}

	/*
	 * We traverse the whole statement
	 * we push each element to the stack, except the closing first parenthesis
	 * If there is a closing first parenthesis, then
	 * get all the element till we get the opening first parenthesis, and add them to the arraylist
	 * Now we get the unit condition of the but in reverse order, so we reverse the arrayList
	 * Here we traverse the unit condition
	 * if in the unit condition is signed, i.e start with a sign like +3 != -3, then
	 * for total value, we check the previosu and next keyword
	 * when ever we get  a operator, we get the previous and post value
	 * if the operator is a conditional operator, then we get compare it in the miniUnitResult
	 * otherwise if it is the general operator, we then evaluate the expression
	 * after these for conditional operator, the unit condition is replaced by the 1 or 0
	 * and for the general operator the unit operation is replaced with the evaluated value
	 * finally the stack contains only one 1 or 0
	 * for 0, flag is false and for 1 flag is true 
	*/
	private boolean check() {
		
		stack = new Stack<>();	//initialize the stack
				
		/*
		 * for closing first parenthesis, we pop the value from stack till got a opening first parenthesis
		 * otherwise push to the stack
		*/
		for(int index=0; index<conditionStatement.size(); index++) {	//traverse the whole sattement
			TokenInfo tokenInfo = new TokenInfo();	//will store the current token
			tokenInfo = hmTokenInfo.get(conditionStatement.get(index));		//got the current token info
			
			int tokenCategory = tokenInfo.getCatagory();	//current token category
			String tokenKeyword = tokenInfo.getKeyword();	//current token keyword
			
			/*
			 * If there is a token that is a variable
			 * then we replace the variable with it's value
			*/	
			//if(variableList.contains(tokenKeyword)) {	//this is a variable identifier
			if(objectUtility.isParentObject(tokenKeyword)) {	//this is a variable identifier

				ObjectTracking objectTracking = new ObjectTracking(conditionStatement, token, alObjectList, hmObjectInfo);
				objectTracking.trackObject(index);
				
				ObjectInfo retrievedObjectInfo = objectTracking.getObjectInfo();
				index = objectTracking.getIndex();
				
				//tokenKeyword = hmVariableInfo.get(tokenKeyword).getObjectValue();	//replace the variable identifier with it's value
				tokenKeyword = retrievedObjectInfo.getObjectValue();	//replace the variable identifier with it's value
				index --;
			}
			
			/*
			 * got a closing first parenthesis, i.e end of the unit condition
			 * the unit conditin is started with a opening first parenthesis
			 * so we store all the identifier from the closing first parenthesis to the opening first parenthessis
			 * from the stack, by popping the stack
			 * whenever we popping, if there is a opening first parenthesis, we reached the end of unit condditon in reverse order
			 * so we reverse the arraylist
			 * Initially we assume the condition is true
			*/
			if(tokenCategory == 21) {	//closing first parenthesis
				Stack<String> stkUnitCondition = new Stack<>();	//since end of the unit condition, we store here
				stkUnitCondition.add(tokenKeyword);	//insert the closing first parenthesis 
				
				while(true) {	//till the stack is empty, or get a opening  parenthesis
					
					String keyWord = stack.pop();	//get the top keyword
					stkUnitCondition.add(keyWord);	//add the top keyword to the unit condition arrraylist
					
					if(keyWord.equals("(") || stack.isEmpty()) {	//opening first parenthesis ot stack is empty
						
						Collections.reverse(stkUnitCondition);	//since got from the stack, it's in the reverse  order, so alter
						
						int middleIndex = stkUnitCondition.size()/2;	//middle position, i.e operator of the unit condition
						String middleOperator = stkUnitCondition.get(middleIndex);	//got the middle operator
						
						String val;// = miniUnitResult(middleOperator, stkUnitCondition.get(1), stkUnitCondition.get(3));

						/*
						 * 
						*/
						
						//>>>>>>>>>log
						//System.out.print("Unit Condition: ");
						
						ArrayList<String> alUnitCondition = new ArrayList<>();
						
						
						/*if(alOperatorList.contains(stkUnitCondition.get(1)) || alConditionalOperatorList.contains(stkUnitCondition.get(1))) {
							System.out.println("Got a start operator: " + stkUnitCondition.get(0));
							stkUnitCondition.push("0");
						} else {
							System.out.println("No Start operator " + stkUnitCondition.get(0));
						}*/
						
						for(int count=0; count<stkUnitCondition.size(); count++) {
							alUnitCondition.add(stkUnitCondition.get(count));
						} 
						
						if(alOperatorList.contains(alUnitCondition.get(1)) || alConditionalOperatorList.contains(alUnitCondition.get(1))) {
							alUnitCondition.add(1, "0");
						}
						
						ExpressionEvaluation expressEvaluation = new ExpressionEvaluation(alUnitCondition);
						val  = expressEvaluation.getResult();
						stack.push(val);
						break;
					}
				}
				
			} else {
				stack.push(tokenKeyword);
			}
		}
		
		if(!stack.isEmpty()) {
			String conditionResult;
			if(stack.size()>1) {
				conditionResult = miniUnitResult(stack.get(1), stack.get(0), stack.get(2));
			} else {
				conditionResult = stack.pop();
			}
			
			if(conditionResult.equals("1")) {
				return true;
			} else if(conditionResult.equals("0")) {
				return false;
			}
		} else {
			//>>>>>>>log
			//System.out.println("-----?????-- Unexpected Stack");
		}
		return false;
	}
	
	/*
	 * Here we get a conditional operator and its two values, on the left and right side
	 * we manually compare the value accoding the operaor and finally send the value
	 * 1 for true and 0 for false 
	*/
	public String miniUnitResult(String operator, String preVal, String postVal) {
       /* //#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        System.out.println("PreVal: " + preVal + " postVal: " + postVal);
        //#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

        switch(operator) {
		case "==" : 
			if(Double.parseDouble(preVal) == Double.parseDouble(postVal)) 
				return "1";
			else return "0";
		case "!=":
			if(Double.parseDouble(preVal) != Double.parseDouble(postVal)) 
				return "1";
			else return "0";
		case "<": 
			if(Double.parseDouble(preVal) < Double.parseDouble(postVal)) 
				return "1";
			else return "0";
		case ">": 
			if(Double.parseDouble(preVal) > Double.parseDouble(postVal)) 
				return "1";
			else return "0";
		case "<=": 
			if(Double.parseDouble(preVal) <= Double.parseDouble(postVal)) 
				return "1";
			else return "0";
		case ">=": 
			if(Double.parseDouble(preVal) >= Double.parseDouble(postVal)) 
				return "1";
			else return "0";
		case "||" : 
			if((Double.parseDouble(preVal) != 0) ||  (Double.parseDouble(postVal) != 0)) 
				return "1";
			else return "0";
		case "&&" : 
			if((Double.parseDouble(preVal) != 0) &&  (Double.parseDouble(postVal) != 0)) 
				return "1";
			else return "0";
		default:
			return "1";
		}
	}

}
