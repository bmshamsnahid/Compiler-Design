package com.ShamsNahid.Tokenizing;

/*
 * For each token all info is stored here
 * For token category and generic category value check the CATEGORY INFORMATION folder
 * 
*/
public class TokenInfo {
	
	private int identity;	//this is the token unique identity
	private int position;	//position of the token
	private int catagory;	//specific category, like each data type, operator, puntuation has unique category value 
	private int genericCatagory;	//generic category, like, datatype, operator, puntuation
	private int line;	//line no of the token
	private String keyword;	//name of the token
	private String info;	//if any additional information needed

	//GETTER AND SETTER
	public int getIdentity() {
		return identity;
	}
	public void setIdentity(int identity) {
		this.identity = identity;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public int getCatagory() {
		return catagory;
	}
	public void setCatagory(int catagory) {
		this.catagory = catagory;
	}
	public int getGenericCatagory() {
		return genericCatagory;
	}
	public void setGenericCatagory(int genericCatagory) {
		this.genericCatagory = genericCatagory;
	}
	public int getLine() {
		return line;
	}
	public void setLine(int line) {
		this.line = line;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
}
