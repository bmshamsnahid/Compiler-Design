package com.ShamsNahid.Tokenizing;

import java.util.ArrayList;
import java.util.HashMap;
import com.ShamsNahid.Helper.FileHandling;
import com.ShamsNahid.Helper.Utility;

public class TokenDigging {

	
	Utility utility;
	private Token token;
	
	
	//Getter for token
	public Token getToken() {
		return token;
	}
	
	/*
	 * From input text file we got all token
	 * value, position, line, keyWord, info all will be completed here and 
	 * resulting token is kept in token object
	 * 
	*/
	public TokenDigging() {
		utility = new Utility();
		
		ArrayList<String> al = new ArrayList<String>();
		
		/*
		 * From tokenHelper class we can get file string as arrayList and also can print token
		*/
		TokenHelper tokenHelper = new TokenHelper();	 //declare the tokenHelper object
		al = tokenHelper.getFileText(); 	//intput file text is retrived as arrayList
		
		TokenMaking tokenMaking  = new TokenMaking(al);		// declare tokenMaking object and fron here we got raw token
		ArrayList<String> alToken = tokenMaking.getAlToken();	 // raw token is received via getter
		
		TokenCatagorized tokenCategorized = new TokenCatagorized(alToken);	 // here we categorized the raw token
		token = tokenCategorized.getToken();	 //categorized token is received via getter and put in alToken Arraylist
		
		tokenHelper.printToken(token);	 //Here we are prinying the final token with info
	}
	
}
