package com.ShamsNahid.Tokenizing;

import java.util.ArrayList;
import java.util.HashMap;

import com.ShamsNahid.Helper.Utility;

/*
 * FOR CATEGORY VALUE, PLEASE CHECK "CATEGORY INFORMATION" FOLDER
*/
public class TokenCatagorized {
	
	private Token token; 	//Our main performer buddy
	private ArrayList<Integer> alToken; 	//Token is stored as integer value and kept here
	private HashMap<Integer, TokenInfo> hm; 	//According to each integer value token info is placed in this hashMap
	private TokenInfo tokenInfo; 	//For each token position, value, lineNumber, category etc. etc are stored here
	
	//retrive from utility class
	private ArrayList<String> alConditionalKeyWordList = new ArrayList<String>();
	private ArrayList<String> alOperatorList= new ArrayList<String>();
	private ArrayList<String> alConditionalOperatorList = new ArrayList<>();
	private ArrayList<String> alLoopList= new ArrayList<String>();
	private ArrayList<String> alDataTypeList= new ArrayList<String>();
	private ArrayList<String> alAllBraketList= new ArrayList<String>();
	private ArrayList<String> alReturnTypeList= new ArrayList<String>();
	private ArrayList<String> alNumberList = new ArrayList<String>();
	private ArrayList<String> alAlphabetList = new ArrayList<String>();
	private ArrayList<String> alSpecialKeywordList = new  ArrayList<String>();
	private ArrayList<String> alLibraryFunctionList = new ArrayList<String>();
	private ArrayList<String> alLibraryKeywordList = new ArrayList<>();
	private ArrayList<String> alPuntuationList = new ArrayList<String>();
	
	Utility utility;
	
	//      GETTER
	public Token getToken() {
		return token;
	}
	
	/*
	 * 		CONSTRUCTOR
	 * -> Here is the fact 
	 * -> Declare objects and retrieve necessary fundamental list from utility class
	 * -> Now we call tokenCategory() method to categorized all raw token
	 * 
	*/
	public TokenCatagorized(ArrayList<String> al) {
			
		utility = new Utility();	//creating object of Utility class
		token = new Token();
		alToken = new ArrayList<Integer>();
		hm = new HashMap<Integer, TokenInfo>();
		tokenInfo = new TokenInfo();
		
		//All keyword stored in utility class and here we are, getting them to use
		alConditionalKeyWordList = utility.getAlConditionalKeywordList();
		alOperatorList = utility.getAlOperatorList();
		alConditionalOperatorList = utility.getAlConditionalOperatorList();
		alLoopList = utility.getAlLoopList();
		alDataTypeList = utility.getAlDataTypeList();
		alAllBraketList = utility.getAllBraketList();
		alReturnTypeList = utility.getAlReturnTypeList();
		alNumberList = utility.getAlNumberList();
		alAlphabetList = utility.getAlAlphabetList();
		alSpecialKeywordList = utility.getAlSpecialKeywordList();
		alLibraryFunctionList = utility.getAlLibraryFunctionList();
		alLibraryKeywordList = utility.getAlLibraryKeywordList();
		alPuntuationList = utility.getAlPuntuationList();
		
		tokenCategory(al); 	//Game start for token categorized
		getFunctionIdentity();	//since basic categorigation is finished, so moved on to detech the method name
		
		//Token categorizing is finished and added token list and their info in token class
		token.setToken(alToken);	
		token.setHm(hm);
	}
	
	/*
	 * 		PROCEDURE
	 * -> We assume our line start with value 1
	 * -> With each newline character we increase our line number
	 * -> For each keyword except method name / variable name we assign category value
	 * -> For method check a retrun type keyword before the current index and
	 * 		the next index must be a breakets
	 * -> To avoid risk, when checking method name by checking the previous and next index ERROR may occoured and thus we 
	 * 		use try/catch section(though we check boundaries) 
	 * -> For variable.....................................................
	*/
	public void tokenCategory(ArrayList<String> al) {
		int line = 1;
		int value = 0;
		int size = al.size();
		int index;
		String str;

		for(String element : alLibraryKeywordList) {
			System.out.print(element + " ");
		} System.out.println();

		for(String element : alLibraryFunctionList) {
			System.out.print(element + " ");
		} System.out.println();

		for(String element : alLoopList) {
			System.out.print(element + " ");
		} System.out.println();

		for(index=0; index<size; index++) {
			str = al.get(index);
			if(str.contains("\n")) {
				line++; 	//if a newLine found we increase the line number and continue this portion
				continue;
			}
			
			int _value = value++; 	//for each token we need a unique value and  we assign them from 0
			int _position = index; 	//Postition is the token position in editor
			int _category = 0;	 // For each token there is a specific category, check the first comment
			int _genericCategory = 0; 	//For each token there is a specific generic category, check the first comment
			int _line = line;
			String _keyWord = str;	 //The token keyword is itself
			String _info = ""; 	//Short description of toekn
			
			if(alLoopList.contains(str)) {
				_genericCategory = 101;
				_category = setLoopCategory(str);
				_info = "loop";
			} else if(alConditionalKeyWordList.contains(str)) {
				_genericCategory = 102;
				 _category = setConditionCategory(str);
				_info = "condition";
			} else if(alDataTypeList.contains(str) || alReturnTypeList.contains(str)) {
				_genericCategory = 103;
				_category = setDataTypeCategory(str);
				_info = "dataType/returnType";
			} else if(alPuntuationList.contains(str)) {
				_genericCategory = 104;
				_category = setPuntuationCategory(str);
				_info = "puntuation";
			} else if(alSpecialKeywordList.contains(str)) {
				_genericCategory = 105;
				_info = "specialKyWord";
			} else if(alLibraryFunctionList.contains(str)) {
				_genericCategory = 106;
				_category = setLibraryMethodCategory(str);
				_info = "libraryFunction";
			} else if(alOperatorList.contains(str)) {
				_genericCategory = 107;
				_category = setOperatorCategory(str);
				_info = "operator";
			} else if(alConditionalOperatorList.contains(str)) {
				_genericCategory = 111;
				_category = setConditionalOperatorCategory(str);
				_info = "operator";
			} else if(alAllBraketList.contains(str)) {
				_genericCategory = 108;
				_category = setParenthesisCategory(str);
				_info = "parenthesis";
			} else if(alLibraryKeywordList.contains(str)) {
				_genericCategory = 120;
				_category = setLibraryKeywordCategory(str);
				_info = "libraryKeyword";
			} else {
				_genericCategory = 115;
				_category = (utility.isNumber(str) ? 61 : 65);
				_info = "Identifier";
			}
			
			tokenInfo = null; 	//erase previous values if exist
			tokenInfo = new TokenInfo(); 	//Declare again to get a fresh object
			
			//Assign values to the current token in tokenInfo
			tokenInfo.setIdentity(_value);
			tokenInfo.setPosition(_position);
			tokenInfo.setGenericCatagory(_genericCategory);
			tokenInfo.setCatagory(_category);
			tokenInfo.setLine(_line);
			tokenInfo.setKeyword(_keyWord);
			tokenInfo.setInfo(_info);
			
			alToken.add(_value); 	//Unique token value is added
			hm.put(_value, tokenInfo);     //For unique token value corresponding tokenInfo is added in hashmap
		}
			
	}
	

	/*
	 * Here we found the method name
	 * Here is the procedure
	 * -> If There is a identifire(115) 
	 * -> And on the previous position there is a data Type or a return type (103)
	 * -> And on the next position there is a parenthesis (108 && 22) opening first parenthesis
	 * -> Then we got a function name
	 * -> Previous category is generic cause it may be anyone like int, float , double , string etc. etc..
	 * -> but next category is category not generic, cause it must be a opening first parenthjesi
	*/
	private void getFunctionIdentity() {
		ArrayList<Integer> alTemp = new ArrayList<Integer>();	//when method name will be found, then that's identity will be kept here 
		int index;
		int currentCategory, nextCategory, previousCategory;
		int size = alToken.size();
		
		//loop is through size-1, cause after a function there is always something(for optimization can be placed size-2)
		for(index=1; index<size-1; index++) {
			TokenInfo ti = hm.get(alToken.get(index));
			currentCategory = hm.get(alToken.get(index)).getGenericCatagory();  //got current generic category
			if(currentCategory == 115) {
				nextCategory = hm.get(alToken.get(index+1)).getCatagory(); 	//got next category(not generic)
				previousCategory = hm.get(alToken.get(index-1)).getGenericCatagory();	//got previous generic category
				if(previousCategory == 103 && nextCategory == 22) {
					ti.setInfo("method");	//since we got a method, then set it's info as "method"
					ti.setGenericCatagory(110);	//for a method generic category will be 110
				}
			}
		}
		
	}
	
	
	/*
	 * According to parenthesis we categorized here
	 * FOR CATEGORY VALUE, PLEASE CHECK "CATEGORY INFORMATION/BBRAKETS CATEGORY" FOLDER
	 * 
	*/
	private int setParenthesisCategory(String str) {
		switch (str) {
		case "(":
			return 22;
		case "{":
			return 24;
		case "[":
			return 26;
		case ")":
			return 21;
		case "}":
			return 23;
		case "]":
			return 25;

		default:
			return 0;
		}
	}
	
	/*
	 * According to puntuation we categorized here
	 * FOR CATEGORY VALUE, PLEASE CHECK "CATEGORY INFORMATION/CATEGORY PUNTUATION" FOLDER
	 * 
	*/
	private int setPuntuationCategory(String str) {
		switch (str) {
		case ",":
			return 70;
		case ";":
			return 71;
		case "\"":
			return 72;
		case "\'":
			return 73;
		case ".":
			return 74;
		default:
			return 0;
		}
	}
	
	/*
	 * According to Data Type or Return Type we categorized here
	 * FOR CATEGORY VALUE, PLEASE CHECK "CATEGORY INFORMATION/Data Type CATEGORY" FOLDER
	 * 
	*/
	private int setDataTypeCategory(String str) {
		switch(str) {
		case "int":
			return 60;
		case "double":
			return 61;
		case "float":
			return 62;
		case "long":
			return 63;
		case "void":
			return 64;
		default:
			return 0;
		}
	}
	
	/*
	 * According to Loop: for, while, do-while Type we categorized here
	 * FOR CATEGORY VALUE, PLEASE CHECK "CATEGORY INFORMATION/CONDITIONAL AND LOOP CATEGORY" FOLDER
	 * 
	*/
	private int setLoopCategory(String str) {
		switch(str) {
		case "for":
			return 31;
		case "while":
			return 33;
		case "do":
			return 35;
		default:
			return 0;
		}
	}
	
	private int setConditionCategory(String str) {
		switch(str) {
		case "if":
			return 32;
		case "else":
			return 34;
		case "else if":
			return 36;
		default:
			return 0;
		}
	}
	
	/*
	 * According to library keyword: _dec Type we categorized here
	 * FOR CATEGORY VALUE, PLEASE CHECK "CATEGORY INFORMATION/LIBRARY KEYWORD" FOLDER
	 * 
	*/
	private int setLibraryKeywordCategory(String str) {
		switch(str) {
		case "_dec":
			return 11;
		default:
			return 0;
		}
	}
	
	/*
	 * According to operators: +, -, = ... we categorized here
	 * FOR CATEGORY VALUE, PLEASE CHECK "CATEGORY INFORMATION/LIBRARY KEYWORD" FOLDER
	 * 
	*/
	private int setOperatorCategory(String str) {
		switch(str) {
		case "!":
			return 48;
		case "&":
			return 49;
		case "=":
			return 40;
		case "+":
			return 41;
		case "-":
			return 42;
		case "*":
			return 43;
		case "/":
			return 44;
		case "%":
			return 45;
		case "~":
			return 52;
		case "#":
			return 53;
		case "|":
			return 54;
		case "$":
			return 55;
		default:
			return 0;
		}
	}
	
	/*
	 * According to conditional operators: ==, != || && ... we categorized here
	 * FOR CATEGORY VALUE, PLEASE CHECK "CATEGORY INFORMATION/conditional Operator" FOLDER
	 * 
	*/
	private int setConditionalOperatorCategory(String str) {
		switch(str) {
		case "==":
			return 202;
		case "!=":
			return 203;
		case "||":
			return 200;
		case "&&":
			return 201;
		case "<":
			return 204;
		case ">":
			return 205;
		case "<=":
			return 201;
		case ">=":
			return 201;		
		default:
			return 0;
		}
	}
	
	/*
	 * According to library keyword: Display, Input Type we categorized here
	 * FOR CATEGORY VALUE, PLEASE CHECK "CATEGORY INFORMATION/LIBRARY Method" FOLDER
	 * 
	*/
	private int setLibraryMethodCategory(String str) {
		switch(str) {
		case "Display":
			return 90;
		case "Input":
			return 91;
		default:
			return 0;
		}
	}
}
