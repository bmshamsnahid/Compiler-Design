package com.ShamsNahid.Tokenizing;

import java.util.ArrayList;
import java.util.HashMap;

import com.ShamsNahid.Helper.FileHandling;
import com.ShamsNahid.Helper.Utility;

public class TokenHelper {
	
	public ArrayList<String> getFileText() {
		Utility utility = new Utility();
		FileHandling fileHandling = new FileHandling();
		String str = "";
		try {
			str = fileHandling.readFile("input.txt");
		} catch (Exception e) {
			utility.printError("In main Fucntion: Error in reading file,  System Says: " + e.toString());
		}
		
		
		String strArr[] = str.split("");
		ArrayList<String> strAl = new ArrayList<String>();
		
		for(String strTemp : strArr) {
			strAl.add(strTemp);
		}
		
		return strAl;
	}
	
	public void printToken(Token token) {
		ArrayList<Integer> alTokenInt = new ArrayList<Integer>();
		HashMap<Integer, TokenInfo> hm = new HashMap<Integer, TokenInfo>();
		
		alTokenInt = token.getToken();
		hm = token.getHm();
		
		for(Integer index : alTokenInt) {
			TokenInfo ti = hm.get(index);
			System.out.println("	Identity: " + ti.getIdentity());
			System.out.println("	Position: " + ti.getPosition());
			System.out.println("	Category: " + ti.getCatagory());
			System.out.println("	GenericCategory: " + ti.getGenericCatagory());
			System.out.println("	Line: " + ti.getLine());
			System.out.println("	KeyWord: " + ti.getKeyword());
			System.out.println("	Info: " + ti.getInfo());
			
			System.out.println("--------------------------------------------------------------------");
		}
	}

}
