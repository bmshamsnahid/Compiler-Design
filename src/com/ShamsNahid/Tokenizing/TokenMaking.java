package com.ShamsNahid.Tokenizing;
import java.util.ArrayList;
import com.ShamsNahid.Helper.Utility;

public class TokenMaking {
	//store all keyword as +, - if, else, ++, >>>, for etc.
	private ArrayList<String> alPrimaryToken = new ArrayList<String>();
	//remove comment 
	private ArrayList<String> alFilterToken1 = new ArrayList<String>();
	//jointing else if , removing extra space
	private ArrayList<String> alFilterToken2 = new ArrayList<String>();
	//here we get raw token
	private ArrayList<String> alToken = new ArrayList<String>();
	
	//retrive from utility class
	private ArrayList<String> alConditionalKeyWordList = new ArrayList<String>();
	private ArrayList<String> alOperatorList= new ArrayList<String>();
	private ArrayList<String> alLoopList= new ArrayList<String>();
	private ArrayList<String> alDataTypeList= new ArrayList<String>();
	private ArrayList<String> alAllBraketList= new ArrayList<String>();
	private ArrayList<String> alReturnTypeList= new ArrayList<String>();
	private ArrayList<String> alNumberList = new ArrayList<String>();
	private ArrayList<String> alAlphabetList = new ArrayList<String>();
	private ArrayList<String> alSpecialKeywordList = new  ArrayList<String>();
	private ArrayList<String> alLibraryFunctionList = new ArrayList<String>();
	private ArrayList<String> alPuntuationList = new ArrayList<String>();
	
	Utility utility;

	//GETTER	
	public ArrayList<String> getAlToken() {
		return alToken;
	}
	
	
	/*
	 * 		CONSTRUCTOR
	 * 
	 * we receive file string and process here
	 * createToken() function will generated raw token
	 * all kinds of fundamental stuff will be retrieve from utility class
	 * for further hierarchy follow the create token method 
	*/
	public TokenMaking(ArrayList<String> al) {
		
		utility = new Utility();	//creating object of Utility class
		
		//All keyword stored in utility class and here we are, getting them to use
		alConditionalKeyWordList = utility.getAlConditionalKeywordList();
		alOperatorList = utility.getAlOperatorList();
		alLoopList = utility.getAlLoopList();
		alDataTypeList = utility.getAlDataTypeList();
		alAllBraketList = utility.getAllBraketList();
		alReturnTypeList = utility.getAlReturnTypeList();
		alNumberList = utility.getAlNumberList();
		alAlphabetList = utility.getAlAlphabetList();
		alSpecialKeywordList = utility.getAlSpecialKeywordList();
		alLibraryFunctionList = utility.getAlLibraryFunctionList();
		alPuntuationList = utility.getAlPuntuationList();
		
		//starting our tokenizing
		createToken(al);
						
	}

	
	
	/*
	 * 		TOKENIZING WILL BE PERFORMED HERE
	 * 
	 * Here is the procedure
	 * -> primary token will separate keyword, operator, puntuation, brakets, space and new line
	 * -> comments, single line and multiline are removed and they are stored in alFilterToken1 in removeComment() method()
	 * -> empty spaces will be removed and "else if" will be joined in tokenRefurbished() method
	 * -> micellinious job will be completed in finalizeToken() method and stored in alToken arrayList 
	*/
	private void createToken(ArrayList<String> al) {
		
		alPrimaryToken = primaryToken(al);	 //separating space, newLine, operator, puntuation, keywords, brakets
		alFilterToken1 = removeComment(alPrimaryToken); 	//removing singleline and multiline comment
		alFilterToken2 = tokenRefurbished(alFilterToken1);	 //removing un-necessary space and joning "else if" if they are neighbour
		alToken = finalizeToken(alFilterToken2);	 //other micellinious job will be done here
		//System.out.println("Token: " + alToken);	 //printing our raw token with comma separated
	}
	
	
	/*
	 * 		SEPARATING SPACE, OPEARATOR, NEWLINE, PUNTUATION, KEYWORD, BRAKETS
	 * 
	 * Here is the procedure
	 * -> if there is a character(number/alphabet) then we go 
	 * 		 while continiously character is found and put it as a identifier
	 * -> if there is a operator then we while Ex int, main, var etc.
	 * 		 operator is found and put it as a operator Ex: +. -. ++. +=. <<< etc
	 * -> if a puntuation is found put it as a token
	 * -> if a braket is found put it as a token
	 * 
	*/
	private ArrayList<String> primaryToken(ArrayList<String> al) {
		ArrayList<String> alTokenTemp = new ArrayList<String>();
		
		int index = 0;
		int size = al.size();
		String str;
		String identifier, operator;
		
		while(true) {
			if(index >= size) {
				break;
			}
			
			str = al.get(index);
			
			if(alAlphabetList.contains(str) || alNumberList.contains(str)) {
				identifier = "";
				while(true) {
					str = al.get(index);
					if(alAlphabetList.contains(str) || alNumberList.contains(str)) {
						identifier += str;
						index++;
					} else {
						alTokenTemp.add(identifier);
						index--;
						break;
					}
				}
			}
			
			else if(alOperatorList.contains(str)) {
				operator = "";
				while(true) {
					str = al.get(index);
					if(alOperatorList.contains(str)) {
						operator += str;
						index++;
					} else {
						alTokenTemp.add(operator);
						index--;
						break;
					}
				}
			}
			
			/*
			 * If there is a connattion we will traverse till the conttation is ended
			 * we keep the cottation and also keep the inside value of the cottation and make a token
			*/
			else if(alPuntuationList.contains(str)) {
				if(str.equals("\"")) {
					//String stringConstant = "\"";
					String stringConstant = "\"";
					index++;
					while(true) {
						str = al.get(index);
						index++;		
						if(str.equals("\"")) {
							stringConstant += "\"";
							alTokenTemp.add(stringConstant);
							index--;
							break;
						} else {
							stringConstant += str;
						}
					}
				} else {
					alTokenTemp.add(str);
				}
			}
			
			else if(alAllBraketList.contains(str)) {
				alTokenTemp.add(str);
			}
			
			else {
				alTokenTemp.add(str);
			}
			
			index++;
		}
		
		return alTokenTemp;
	}
	
	/*
	 * 		REMOVE SINGLE LINE COMMENT AND MULTILINE COMMENT
	 * 
	 * Here is the procedure
	 * -> if // found we ignore the rest of primary token untill we found a new line
	 * -> if /* found we ignore the rest of primamry token untill we found a end of multiline comment symbole(star_slash)
	 * 
	*/
	private ArrayList<String> removeComment(ArrayList<String> al) {
		ArrayList<String> alTokenTemp = new ArrayList<String>();
		
		int index = 0;
		int size = al.size();
		String str;
		String identifier;
		
		while(true) {
			if(index >= size) {
				break;
			}
			
			str = al.get(index);
			
			if(str.contains("//")) {
				//System.out.println("got // in " + str );
				while(!str.contains("\n")) {
					index++;
					str = al.get(index);
				}
			}
			
			if(str.contains("/*")) {
				while(!str.contains("*/")) {
					if(str.contains("\n")) alTokenTemp.add("\n");
					index++;
					str = al.get(index);
				}
				index++;
				str = al.get(index);
			}
			alTokenTemp.add(str);
			index++;
		}
		return alTokenTemp;
	}
	
	/*
	 * 		REMOVE UNNECESSARY SPACES AND JOINING ELSE IF
	 * 
	 * Here is the procedure
	 * -> if there is a empty spaecs like " " then we skip that token
	 * -> if there is a else with next position if then we make these two token as one token
	*/
	private ArrayList<String> tokenRefurbished(ArrayList<String> alM) {
		
		ArrayList<String> al = new ArrayList<String>();
		
		/*
		 * if we get a \n then we keep it to track line number in tokenCategorised
		 * else we keep a string after trimming if size is more then 0
		*/
		for(String str: alM) {			
			
			if(str.contains("\n")) {
				str = "\n";
				al.add(str);
			} else if(str.trim().length() > 0) {
				al.add(str.trim());
			}
		}
		
		ArrayList<String> alTokenTemp = new ArrayList<String>();
		
		int size = al.size();
		String str, strNext;
		
		for(int index=0; index<size-1; index++) {
			str = al.get(index);
			strNext = al.get(index+1);
			
			if(str.equals("else") && strNext.equals("if")) {
				alTokenTemp.add(str + " " + strNext);
				index++;
				continue;
			}
			
			alTokenTemp.add(str);
		}
		
		return alTokenTemp;
	}
	
	/*
	 * 		ANY FINAL PROCEDURE WILL BE PERFORMED HERE
	 * 
	 * Here is the procedure
	*/
	private ArrayList<String> finalizeToken(ArrayList<String> al) {
		return al;
		
	}
	
}