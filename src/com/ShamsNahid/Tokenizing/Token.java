package com.ShamsNahid.Tokenizing;

import java.util.ArrayList;
import java.util.HashMap;

/*
 * Here token is stored in a ArrayList as Integer 
 * The info of the each token integer value is kept in the TokenInfo, can be found by Hashmap hm
 * with corresponding integer value
*/
public class Token {
	
	//Integer value of token is stroed here
	private ArrayList<Integer> token;
	//according to integer value all token info is stored in hashmap
	private HashMap<Integer, TokenInfo> hm;
	
	//Getter and Setter
	public ArrayList<Integer> getToken() {
		return token;
	}
	public void setToken(ArrayList<Integer> token) {
		this.token = token;
	}
	public HashMap<Integer, TokenInfo> getHm() {
		return hm;
	}
	public void setHm(HashMap<Integer, TokenInfo> hm) {
		this.hm = hm;
	}

	
	
}
