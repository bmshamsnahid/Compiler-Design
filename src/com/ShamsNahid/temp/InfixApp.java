package com.ShamsNahid.temp;

//infix.java
//converts infix arithmetic expressions to postfix
//to run this program: C>java InfixApp
import java.io.*;
import java.util.ArrayList;
import java.util.Stack;

class StackX {

	private int maxSize;
	private String[] stackArray;
	private int top;

	public StackX(int s) {
		maxSize = s;
		stackArray = new String[maxSize];
		top = -1;
	}

	public void push(String j) { // put item on top of stack
		stackArray[++top] = j;
	}

	public String pop() { 	// take item from top of stack
		return stackArray[top--];
	}

	public String peek() {	// peek at top of stack
		return stackArray[top];
	}

	public boolean isEmpty() { // true if stack is empty
		return (top == -1);
	}

	public int size() {	// return size
		return top + 1;
	}

	public String peekN(int n) { // return item at index n
		return stackArray[n];
	}

	public void displayStack(String s) {
		System.out.print(s);
		System.out.print("Stack (bottom-->top): ");
		for (int j = 0; j < size(); j++) {
			System.out.print(peekN(j));
			System.out.print(" ");
		}
		System.out.println("");
	}
	
} 

class InToPost { // infix to postfix conversion

	private StackX theStack;
	private ArrayList<String> input;
	private ArrayList<String> output;

	public InToPost(ArrayList<String> input) { // constructor
		this.input = input;
		int stackSize = input.size();
		theStack = new StackX(stackSize);
		output = new ArrayList<>();
	}

	public ArrayList<String> doTrans() {	// do translation to postfix

		for (String ch : input) {
			theStack.displayStack("For" + ch + " "); // *diagnostic*
			switch (ch) {
			case "|":// it’s | or &
			case "&":
				gotOper(ch, 0); // go pop operators
				break;
			case "+": // it’s + or -
			case "-":
				gotOper(ch, 1); // go pop operators
				break; // (precedence 1)
			case "*": // it’s * or /
			case "/":
				gotOper(ch, 2); // go pop operators
				break; // (precedence 2)
			case "(": // it’s a left paren
				theStack.push(ch); // push it
				break;
			case ")": // it’s a right paren
				gotParen(ch); // go pop operators
				break;
			default: // must be an operand
				output.add(ch); // write it to output
				break;
			}  
		}  
		while (!theStack.isEmpty()) { // pop remaining opers
			theStack.displayStack("While "); // *diagnostic*
			output.add(theStack.pop()); // write to output
		}
		theStack.displayStack("End"); // *diagnostic*
		return output; 
	}  
		 
	public void gotOper(String opThis, int prec1) { // got operator from input
		while (!theStack.isEmpty()) {
			String opTop = theStack.pop();
			if (opTop.equals("(")) {
				theStack.push(opTop);
				break;
			} else {
				int prec2;
				// if it’s a ‘(‘
				// restore ‘(‘
				// it’s an operator
				// precedence of new op
				if (opTop.equals("+") || opTop.equals("-")) // find new op prec
					prec2 = 1;
				else
					prec2 = 2;
				if (prec2 < prec1) { 
					// if prec of new op less
					// than prec of old
					theStack.push(opTop);
					// save newly-popped op
					break;
				} else
					// prec of new not less
					output.add(opTop); // than prec of old
			}  
		} 
		theStack.push(opThis); // push new operator
	}  
	
	public void gotParen(String ch) { 	// got right paren from input
		while (!theStack.isEmpty()) {
			String chx = theStack.pop();
			if (chx.equals("(")) // if popped ‘(‘
				break; // we’re done
			else
				// if popped operator
				output.add(chx); // output it
		}  
	}  
}  

class InfixApp {
	public static void main(String[] args) throws IOException {
		ArrayList<String> output, input;
		while (true) {
			System.out.print("Enter infix: ");
			System.out.flush();
			//input = getString();
			//input = "(51+2)|0+3";
			
			input = new ArrayList<>();
			output = new ArrayList<>();
			input.add("(");
			input.add("51");
			input.add("+");
            input.add("2");
			input.add(")");
			input.add("&");
			input.add("0");
			input.add("+");
			input.add("3");
			
			System.out.println("Input is: " + input);
			
			// make a translator
			InToPost theTrans = new InToPost(input);
			output = theTrans.doTrans(); // do the translation
			System.out.println("Postfix is " + output + "\n");
			System.out.println("After Evaluation: " + evaluation(output));
			break;
		} // end while
	} // end main()
		// --------------------------------------------------------------

	public static String getString() throws IOException {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String s = br.readLine();
		return s;
	}
	// --------------------------------------------------------------
	
	public static double evaluation(ArrayList<String> output) {
		double val = 0;
		
		System.out.println("Got the output: " + output);
		
		Stack<String> stk = new Stack<String>();
		
		for(int index=output.size()-1; index>=0; index--) {
			stk.push(output.get(index));
		}
		
		System.out.println("Stack is: " + stk);
		
		ArrayList<String> al = new ArrayList<>();
		String str;
		
		do {
			str = stk.pop();
			System.out.println("Stack pop: " + str);
			
			al.add(str);
			
			if(isOperator(str)) {
				System.out.println("sending: " + al.get(0) + al.get(2) + al.get(1) + " and got " + operation(al));
				stk.push(operation(al));
				al = new ArrayList<>();
			}
			
		} while(!stk.empty());
		
		System.out.println("str: " + str);
		
		return Double.valueOf(str);
	}
	
	private static String operation(ArrayList<String> al) {
		
		double val1 = Double.valueOf(al.get(0));
		double val2 = Double.valueOf(al.get(1));
		String operator = al.get(2);
		
		if(operator.equals("+")) {
			return String.valueOf(val1 + val2);
		} else if(operator.equals("-")) {
			return String.valueOf(val1 - val2);
		} else if(operator.equals("*")) {
			return String.valueOf(val1 * val2);
		} else if(operator.equals("/")) {
			return String.valueOf(val1 / val2);
		} else if(operator.equals("|")) {
			if(val1 > 0 || val2 > 0) return "1";
			else return "0";
		} else if(operator.equals("&")) {
			if(val1 <= 0 || val2 <= 0) return "0";
			else return "1";
		}
		
		return "0";
	}

	public static boolean isOperator(String ch) {
		
		if(ch.equals("+") || ch.equals("-") || ch.equals("*") || ch.equals("/") || ch.equals("|") || ch.equals("&")) {
			return true;
		} else {
			return false;
		}
	}
	
} 