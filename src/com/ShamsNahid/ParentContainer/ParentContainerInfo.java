package com.ShamsNahid.ParentContainer;

import java.util.ArrayList;
import java.util.HashMap;
import com.ShamsNahid.method.MethodInfo;

public class ParentContainerInfo {

	//THIS IS THE IDENTIFIER OF THE METHODS
	private ArrayList<Integer> alMethods;
	//HERE WE KEEP TRACK OF THE METHOD INFO USING THE METHOD IDENIFIER
	private HashMap<Integer, MethodInfo> hmMethodInfo;


	//GETTER AND SETTER
	public ArrayList<Integer> getAlMethods() {
		return alMethods;
	}
	public void setAlMethods(ArrayList<Integer> alMethods) {
		this.alMethods = alMethods;
	}
	public HashMap<Integer, MethodInfo> getHmMethodInfo() {
		return hmMethodInfo;
	}
	public void setHmMethodInfo(HashMap<Integer, MethodInfo> hmMethodInfo) {
		this.hmMethodInfo = hmMethodInfo;
	}

}
