package com.ShamsNahid.Objects;

import com.ShamsNahid.method.MethodInfo;

import java.util.ArrayList;
import java.util.HashMap;

/*
 * Store the variable information
 * Keep the nested object and object properties
 * EXAMPLE:
 * _dec object = {
 *      _dec propertyName1 = "propertyValue";
 *      _dec propertyName2 = "propertyValue";
 *      _dec nestedObj1 = {},
 *          nestedObj2 = {
 *              _dec nestedObjValue = "value of second nested object"
 *          };
 *      void methodName() {
 *          Display("I am nested object method.");
 *      }
 * }, arrOb = [1, 2, { _dec nestedArrObj; }];
*/

/** MEMBER INFORMATION
 *
 * objectName: Name of the current object
 * objectValue: Only if this is a plain object(objectCategory: 3101) then it has a value, other wise it is null
 * parentTree: Hierarchy of the the current object
 * objectId: Unique Id for each object, Never change
 * objectCategory: Object can be plain(objectCategory: 3101), nested(objectCategory: 3102) and array(objectCategory: 3103)
 * objectValueExpression: If there is a expression for a plain object or body for the nested object
 * alPropertyObjectsId: For a nested object(objectCategory: 3101), child objects id are stored here
 * alArrayElementsObjectsId: For a array object(objectCategory: 3103), array element objects id are stored here
 * alPropertyMethodsId: For a nested object(objectCategory: 3102), methods id are stored here
 * hmPropertyObjectInfo: Mapping of the property object and array elements information according to their id in alPropertyObjectsId and alArrayElementObjectsId
 * hmPropertyMethodInfo: Mapping of the property method information according to their id in alPropertyMethodsId
*/

public class ObjectInfo {

	private String objectName;
	private String objectValue;
	
    private ArrayList<Integer> parentTree;
	
    private Integer objectId;
	
    private Integer objectCategory;
    
    private ArrayList<Integer> objectValueExpression;

    private ArrayList<Integer> alPropertyObjectsId;
    private ArrayList<Integer> alArrayElementsObjectsId;
    private ArrayList<Integer> alPropertyMethodsId;
	
	private HashMap<Integer, ObjectInfo> hmPropertyObjectInfo;
    private HashMap<Integer, MethodInfo> hmPropertyMethodInfo;

    //GETTER AND SETTER
    public String getObjectName() {
        return objectName;
    }
    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }
    public String getObjectValue() {
        return objectValue;
    }
    public void setObjectValue(String objectValue) {
        this.objectValue = objectValue;
    }
    public ArrayList<Integer> getParentTree() {
        return parentTree;
    }
    public void setParentTree(ArrayList<Integer> parentTree) {
        this.parentTree = parentTree;
    }
    public Integer getObjectId() {
        return objectId;
    }
    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }
    public Integer getObjectCategory() {
        return objectCategory;
    }
    public void setObjectCategory(Integer objectCategory) {
        this.objectCategory = objectCategory;
    }
    public ArrayList<Integer> getObjectValueExpression() {
        return objectValueExpression;
    }
    public void setObjectValueExpression(ArrayList<Integer> objectValueExpression) {
        this.objectValueExpression = objectValueExpression;
    }
    public ArrayList<Integer> getAlPropertyObjectsId() {
        return alPropertyObjectsId;
    }
    public ArrayList<Integer> getAlArrayElementsObjectsId() {
        return alArrayElementsObjectsId;
    }
    public void setAlArrayElementsObjectsId(ArrayList<Integer> alArrayElementsObjectsId) {
        this.alArrayElementsObjectsId = alArrayElementsObjectsId;
    }
    public void setAlPropertyObjectsId(ArrayList<Integer> alPropertyObjectsId) {
        this.alPropertyObjectsId = alPropertyObjectsId;
    }
    public ArrayList<Integer> getAlPropertyMethodsId() {
        return alPropertyMethodsId;
    }
    public void setAlPropertyMethodsId(ArrayList<Integer> alPropertyMethodsId) {
        this.alPropertyMethodsId = alPropertyMethodsId;
    }
    public HashMap<Integer, MethodInfo> getHmPropertyMethodInfo() {
        return hmPropertyMethodInfo;
    }
    public void setHmPropertyMethodInfo(HashMap<Integer, MethodInfo> hmPropertyMethodInfo) {
        this.hmPropertyMethodInfo = hmPropertyMethodInfo;
    }
    public HashMap<Integer, ObjectInfo> getHmPropertyObjectInfo() {
        return hmPropertyObjectInfo;
    }
    public void setHmPropertyObjectInfo(HashMap<Integer, ObjectInfo> hmPropertyObjectInfo) {
        this.hmPropertyObjectInfo = hmPropertyObjectInfo;
    }
}
