package com.ShamsNahid.Objects;

import com.ShamsNahid.Helper.Utility;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by bmshamsnahid on 4/30/17.
 * This is the most important class
 * We get the value expression and track the end object
 * We receive an object list like ob1.ob2.ob3.[5].ob5
 * And from the list we track the ob5 object
 *
 * Here is the getter for the end objectInfo and end last index
 */
public class ObjectTracking {

    private ObjectInfo objectInfo;
    private Integer myIndex;

    private ArrayList<Integer> alObjectValueExpression;

    private Token token;
    private ArrayList<Integer> alToken;
    private HashMap<Integer, TokenInfo> hmTokenInfo;

    private ArrayList<Integer> alObjectList;
    private HashMap<Integer, ObjectInfo> hmObjectInfo;

    private ArrayList<String> alTrackObjectList;
    private int expressionValueIdentity;
    private String expressionValue;

    private Utility utility;

    public ObjectInfo getObjectInfo() {
        return objectInfo;
    }

    public Integer getIndex() {
        return myIndex;
    }

    /**
     * Receive the expression, and parent and sibling objects
     *
     * Using the trackObject method, we find out the end objectInfo and the index
     * The desired objectInfo and index will be available via the getter
    */
    public ObjectTracking(ArrayList<Integer> alObjectValueExpression, Token token, ArrayList<Integer> alObjectList, HashMap<Integer, ObjectInfo> hmObjectInfo) {

        this.alObjectValueExpression = alObjectValueExpression;

        this.token = token;
        this.alToken = token.getToken();
        this.hmTokenInfo = token.getHm();

        this.alObjectList = alObjectList;
        this.hmObjectInfo = hmObjectInfo;

        objectInfo = new ObjectInfo();
        alTrackObjectList = new ArrayList<>();

        utility = new Utility();
    }

    /**
     * Here is the method, trackObject
     * It will receive the index of the starting point and then find out the end object
     * Procedure:
     *  Traverse the whole expressionValue from the start point
     *  First we filter the object list
     *      EX: ob1.ob2.[2].ob4 for general assignment
     *      Here After filtering the object we got, ob1, ob2, 2, ob4
     *      Ex: (ob1.ob2 == ob3) for checking condition
     *      Here filtering process will be terminated whenever got a conditional operator
     *      Here filtering process will be terminated whenever got a closing first parenthesis
     *      Procedure:
     *          If this is the first index, check if this is an object using isObjectMethod and for true result add the identity to the trackedObjectList
     *          If there is a dot, just ignore it
     *          If there is a opening third parenthesis, we take the next identity i.e. array index
     *          If the current index is an object, checked by cisObject method, for true result add the identity to the trackedObjectList
     *  Then using the filtered object we recursively find the end object and will be available to the getter of objectInfo and index
    */
    public void trackObject(Integer startIndex) {
	    ArrayList<Integer> alParent = new ArrayList<>();
        ArrayList<Integer> alFilteredObjectExpression = new ArrayList<>();
        alParent.add(0);

        for(Integer index=startIndex; index<alObjectValueExpression.size(); index++) {
            myIndex = index;
            expressionValueIdentity = alObjectValueExpression.get(index);   //CURRENT keyword identity of the value expression
            TokenInfo tokenInfo = hmTokenInfo.get(alToken.get(expressionValueIdentity));
            expressionValue = tokenInfo.getKeyword();
            if(utility.getAlConditionalOperatorList().contains(expressionValue) || tokenInfo.getCatagory() == 21 || utility.getAlOperatorList().contains(expressionValue)) {  //checking the conditional operator list
                break;
            }
            if(tokenInfo.getCatagory() == 74) { //got a dot .
                continue;
            } else if(tokenInfo.getCatagory() == 26) {  //opening third parenthesis
                index++;
                alFilteredObjectExpression.add(hmTokenInfo.get(alToken.get(alObjectValueExpression.get(index))).getIdentity());
                index++;
                myIndex = index;
            } else {
                if(index == startIndex) {
                    alParent = new ArrayList<>();
                    alParent.add(0);
                    objectInfo = isObject(expressionValue, alParent, alObjectList, hmObjectInfo);
                    if(objectInfo != null) {
                        alFilteredObjectExpression.add(expressionValueIdentity);
                    } else {
                        break;
                    }
                } else {
                    if(objectInfo.getObjectCategory() == 3202) {
                        objectInfo = isObject(expressionValue, objectInfo.getParentTree(), objectInfo.getAlPropertyObjectsId(), objectInfo.getHmPropertyObjectInfo());
                    } else if(objectInfo.getObjectCategory() == 3203) {
                        objectInfo = isObject(expressionValue, objectInfo.getParentTree(), objectInfo.getAlArrayElementsObjectsId(), objectInfo.getHmPropertyObjectInfo());
                    }
                    if(objectInfo != null) {
                        alFilteredObjectExpression.add(expressionValueIdentity);
                    } else {
                        break;
                    }
                }
            }
        }

        /*System.out.print("Received object:::::: ");
        for(Integer index=startIndex; index<alObjectValueExpression.size(); index++) {
            expressionValueIdentity = alObjectValueExpression.get(index);   //CURRENT keyword identity of the value expression
            expressionValue = hmTokenInfo.get(alToken.get(expressionValueIdentity)).getKeyword();
            System.out.print(expressionValue + " ");
        }
        System.out.println();

        System.out.print("Filtered object:::::: ");
        for(Integer index=0; index<alFilteredObjectExpression.size(); index++) {
            expressionValueIdentity = alFilteredObjectExpression.get(index);   //CURRENT keyword identity of the value expression
            expressionValue = hmTokenInfo.get(alToken.get(expressionValueIdentity)).getKeyword();
            System.out.print(expressionValue + " ");
        }
        System.out.println();*/

        for(Integer index=0; index<alFilteredObjectExpression.size(); index++) {
            expressionValueIdentity = alFilteredObjectExpression.get(index);   //CURRENT keyword identity of the value expression
            expressionValue = hmTokenInfo.get(alToken.get(expressionValueIdentity)).getKeyword();    //current keyword of the expression value
            if(expressionValue.equals(".")) continue;   //may be this is irrelevant, cause the filtering process might filter the dot
            else {
                if(index == 0) {
                    alParent = new ArrayList<>();
                    alParent.add(0);
                    objectInfo = isObject(expressionValue, alParent, alObjectList, hmObjectInfo);
                } else {
                    if(utility.isNumber(expressionValue)) {
                        objectInfo = isObject(expressionValue, objectInfo.getParentTree(), objectInfo.getAlArrayElementsObjectsId(), objectInfo.getHmPropertyObjectInfo(), Integer.parseInt(expressionValue));
                    } else {
                        objectInfo = isObject(expressionValue, objectInfo.getParentTree(), objectInfo.getAlPropertyObjectsId(), objectInfo.getHmPropertyObjectInfo());
                    }
                }
            }
        }
    }

    /**
     * One of the overloaded method section to find the child object
     *
     * here we just check the object name
     * if this is a nested or parent can't be determinde here
     * got a name, try to matched already declared object name
     * if matched then we return true
     * other wise return false
     */
    private ObjectInfo isObject(String objectName, ArrayList<Integer> alParent, ArrayList<Integer> alObjectList, HashMap<Integer, ObjectInfo> hmObjectInfo) {
        ObjectInfo objectInfo = new ObjectInfo();
        Boolean matched = false;
        if(alObjectList == null) return null;
        for(Integer index : alObjectList) {
            ObjectInfo tempObjectInfo = hmObjectInfo.get(index);
            if(tempObjectInfo.getObjectName().equals(objectName) && compareArrayListMinSize(alParent, tempObjectInfo.getParentTree())) {
                objectInfo = tempObjectInfo;
                matched = true;
                break;
            }
        }
        if(matched) {
            return objectInfo;
        } else {
            return null;
        }
    }

    /**
     * One of the overloaded method section to find the array elemetn obeject
     *
     * For procedure check the sibling overloaded method
    */
    private ObjectInfo isObject(String objectName, ArrayList<Integer> alParent, ArrayList<Integer> alObjectElementsList, HashMap<Integer, ObjectInfo> hmObjectInfo, Integer arrayIndex) {
        ObjectInfo objectInfo = new ObjectInfo();
        Boolean matched = false;
        if(alObjectElementsList == null) return null;
        try {
            Integer objectId = alObjectElementsList.get(arrayIndex);
            objectInfo = hmObjectInfo.get(objectId);
            matched = true;
        } catch (Exception e) {
            System.out.println("ERROR ERROR ERROR ERROR ERROR ERROR : " + e.toString());
            matched = false;
        }
        if(matched) {
            return objectInfo;
        } else {
            return null;
        }
    }

    /**
     * ArrayList1=> 1, 2, 3, 4
     * ArrayList2=> 1, 2, 3
     * it is true if both arrayList contains the previous pattern respectively
     *
     * *Info
     * Objects have same name in different level
     * but have unique parent tree
     *
     * *Example
     * suppose we have
     * ob1 = ob2.ob3.ob4;
     * ob5 = ob4;
     * here one ob4 is inside ob2(root.ob2.ob3.ob4) and another is under root (root.ob4)
     * to dig ob1, we got ob2.ob3 and have to find ob4,
     * by check the previous tree we detect the desired ob4 contain parent tree (root.ob2.ob3) not (root.ob4)
     */
    private Boolean compareArrayListMinSize(ArrayList<Integer> al1, ArrayList<Integer> al2) {
        int length = (al1.size() < al2.size()) ? al1.size() : al2.size();

        if(Math.abs(al1.size() - al2.size()) != 1) return false;

        for(int index=0; index<length; index++) {
            if(al1.get(index) != al2.get(index)) return false;
        }

        return true;
    }



}
