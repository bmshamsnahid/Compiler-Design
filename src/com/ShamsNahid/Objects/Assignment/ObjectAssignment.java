package com.ShamsNahid.Objects.Assignment;

import com.ShamsNahid.Helper.Utility;
import com.ShamsNahid.Objects.MyObject;
import com.ShamsNahid.Objects.ObjectInfo;
import com.ShamsNahid.Objects.ObjectUtility;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by bmshamsnahid on 4/21/17.
 */
public class ObjectAssignment {

    private ObjectInfo objectInfo;
    private MyObject newObjectContainer;

    public ObjectInfo getRetrievedObjectInfo() {
        return retrievedObjectInfo;
    }

    ObjectInfo retrievedObjectInfo;

    private ArrayList<Integer> alToken; 	//from token, list of the token identity will be retrieved and kept here
    private HashMap<Integer, TokenInfo> hmTokenInfo;	//also from token, each tokens info is stored according to their identity

    //getter
    public MyObject getNewObjectContainer() {
        return newObjectContainer;
    }

    private Token token;

    private Utility utility;
    private ObjectUtility objectUtility;

    private ArrayList<Integer> alObjectList;
    private HashMap<Integer, ObjectInfo> hmObjectInfo;

    public ObjectAssignment(Token token, MyObject myObject, ObjectInfo mcObjectInfo) {
        alToken = token.getToken();		//retrieve the tokens identifier in the arrayList alToken
        hmTokenInfo = token.getHm();	//retrieve the token information according their identifier and kept in the hashMap hmTokenInfo

        alObjectList = new ArrayList<>();
        hmObjectInfo = new HashMap<>();

        utility = new Utility();    //instantiate the object
        objectUtility = new ObjectUtility(token, myObject.getAlObjectList(), myObject.getHmObjectInfo());
        this.token = token;

        this.objectInfo = mcObjectInfo;

        newObjectContainer = myObject;
    }

    public void assignObject(ArrayList<Integer> statement) {
        Boolean flag = false;

        ArrayList<Integer> alObjectValueExpression = new ArrayList<>();

        for(Integer index=0; index<statement.size()-1; index++) { //avoiding the semicolon
            if(flag) {
                alObjectValueExpression.add(statement.get(index));
            }
            if(hmTokenInfo.get(alToken.get(statement.get(index))).getCatagory() == 40) {
                flag = true;
            }
        }


        retrievedObjectInfo = new ObjectInfo();
        retrievedObjectInfo = objectUtility.evaluateObjectValueExpression(alObjectValueExpression, objectInfo);
        if(retrievedObjectInfo == null) {
        } else {
        }

        if(retrievedObjectInfo.getObjectCategory() == 3101) {
            objectInfo.setObjectValue(retrievedObjectInfo.getObjectValue());
        } else {
            objectInfo.setParentTree(retrievedObjectInfo.getParentTree());
            objectInfo.setObjectValueExpression(retrievedObjectInfo.getObjectValueExpression());
            objectInfo.setObjectValue(retrievedObjectInfo.getObjectValue());
            objectInfo.setObjectCategory(retrievedObjectInfo.getObjectCategory());

            objectInfo.setAlPropertyObjectsId(retrievedObjectInfo.getAlPropertyObjectsId());
            objectInfo.setAlPropertyMethodsId(retrievedObjectInfo.getAlPropertyMethodsId());

            objectInfo.setHmPropertyObjectInfo(retrievedObjectInfo.getHmPropertyObjectInfo());
            objectInfo.setHmPropertyMethodInfo(retrievedObjectInfo.getHmPropertyMethodInfo());
        }
    }

}
