package com.ShamsNahid.Objects;
import com.ShamsNahid.Helper.Utility;
import com.ShamsNahid.Objects.ObjectGenerator.GenerateArrayObject;
import com.ShamsNahid.Objects.ObjectGenerator.GenerateNestedObject;
import com.ShamsNahid.Objects.ObjectGenerator.GeneratePlainObject;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;

import java.util.*;



/*
 * Created by bmshamsnahid on 3/24/17.
 * Here we received the token information(token identifier: alToken and their information: hmTokenInfo all in the Token class object) in the constructor
 * Then in digging the object we receive the identifier of the _dec, then from the identifier we dig all the object in that declaration statement
 */
public class ObjectDigging {
    private ArrayList<Integer> alToken; 	//from token, list of the token identity will be retrieved and kept here
    private HashMap<Integer, TokenInfo> hmTokenInfo;	//also from token, each tokens info is stored according to their identity

    private Token token;

    private Utility utility;
    private MyObject newObjectContainer;
    private ArrayList<Integer> alObjectList;
    private HashMap<Integer, ObjectInfo> hmObjectInfo;

    private ArrayList<Integer> newAlObjectList;
    private HashMap<Integer, ObjectInfo> newHmObjectInfo;

    //getter
    public MyObject getNewObjectContainer() {
        return newObjectContainer;
    }

    public ArrayList<Integer> getalObjectList() {
        return  alObjectList;
    }

    public HashMap<Integer, ObjectInfo> getHmObjectInfo() {
        return hmObjectInfo;
    }

    public static Integer objectId = 0;

    public ObjectDigging(Token token) {
        alToken = token.getToken();		//retrieve the tokens identifier in the arrayList alToken
        hmTokenInfo = token.getHm();	//retrieve the token information according their identifier and kept in the hashMap hmTokenInfo

        newObjectContainer = new MyObject();

        alObjectList = new ArrayList<>();
        hmObjectInfo = new HashMap<>();

        newAlObjectList = new ArrayList<>();
        newHmObjectInfo = new HashMap<>();

        utility = new Utility();    //instantiate the object
        this.token = token;
    }

    /**
     * Here we get the objectIndetifier _dec and the previous object information of the genericInfo elements
     * Dig the objects
     * Store them to a list with information and pack in newObjectContainer class, that had the previous object
     *      information and the new ones
     * Return to the first keyword identity after the object declation arena
    */
    public int cookObject(int objectIdentifier, MyObject receivedObject, ArrayList<Integer> alParentTree) {

        alObjectList = receivedObject.getAlObjectList();
        hmObjectInfo = receivedObject.getHmObjectInfo();

        int returnIndex = digObject(alParentTree, objectIdentifier);

        return returnIndex;
    }

    /**
     * Each time we generate a new object, required a object id
     * the object id should be unique
     * we get the object id only from here
    */
    public static Integer getNewObjectId() {
        objectId++;
        return  objectId;
    }

    /**
     * while there is a object we move
     * we only terminate if there is a semicolon
     * After termination, we add the newly object info to the object container
    */
    public int digObject(ArrayList<Integer> alParentTree, int objectIdentifier) {

        Integer index = objectIdentifier;
        if(hmTokenInfo.get(alToken.get(objectIdentifier)).getCatagory() == 11) {
            index++;
        } else {
        }

        while(true) {

            index = generateObject(alParentTree, index);
            if(hmTokenInfo.get(alToken.get(index)).getCatagory() == 70) {
                index++;
                continue;
            }

            index++; //first identifier after the _dec statement

            if(hmTokenInfo.get(alToken.get(index-1)).getCatagory() == 71) {
                newObjectContainer.setAlObjectList(newAlObjectList);
                newObjectContainer.setHmObjectInfo(newHmObjectInfo);
                return index;
            }
        }
    }

    /**
     * This is the starting point of each and every object digging
     * We initialize this class by invoking the generate object
     * while we encounter a nested object, if the nested object contains some more object then they also invoke this method
     * After generating the object it just return the information of <></>he object
     *
     * Object can be two type: plain and nested
     * nested object is started with a { after the equation sign (Example:   _dec nestedObject = {...})
     * plain object can have a value after a equation sign or just a name(Example: _dec plainObject = "value", anotherPlainObject;)
     *
     * If this is a plain object then the object is digged in the generatePlainObject() method
     * else If this is a nested object then the object is digged in the generateNestedObject() method
     *
     * Before invoking these method
     *      we initially make a objectInfo to contains information
     *      set the name (this method start with the index of object name, just next of _dec keyword)
     *      set the object Id (static variable, start from 1 and increased for each object by 1)
     *      set the parent tree(if this is from a nested object, then it get parent tree of it's parent, else new)
     *      set the object value expression(new)
     *      we assume this is a plain object i.e isPlain = true; (if nested then we change it to false)
     *
     * This new object and it's all information is inserted the alObjectList and hmObjectInfo
     *      If this is unintialized i.e no equation sign and just name, comma we insert instantly (_dec unintializedObjectName, anotherObject = "value";)
     *      other wise we add them just before returning the object information
    */

    public Integer generateObject(ArrayList<Integer> alParentTree, Integer index) {


        ObjectInfo objectInfo = new ObjectInfo();

        String objectName;
        objectName = hmTokenInfo.get(alToken.get(index)).getKeyword();
        objectId++;
        ArrayList<Integer> objectParentTree = new ArrayList<>();
        objectParentTree.addAll(alParentTree);
        ArrayList<Integer> objectValueExpression = new ArrayList<>();

        objectInfo.setObjectValueExpression(objectValueExpression);
        objectInfo.setObjectName(objectName);
        objectInfo.setObjectId(objectId);
        //objectInfo.setPlain(true);  //initially assume this is a plain object
        objectParentTree.add(objectId);
        objectInfo.setParentTree(objectParentTree);

        index++; //reached to the equation sign or comma or semicolon

        if(hmTokenInfo.get(alToken.get(index)).getCatagory() != 40) { //if there is not a equation sign, then this is uninitialized
            objectInfo.setObjectValue("");
            objectInfo.setObjectCategory(3101);
            newAlObjectList.add(objectInfo.getObjectId());
            newHmObjectInfo.put(objectInfo.getObjectId(), objectInfo);
            return index;
        }

        index++; //may be a opening second parenthesis or a object value

        if(hmTokenInfo.get(alToken.get(index)).getCatagory() ==  24) {  //opening second parenthesis
            GenerateNestedObject generateNestedObject = new GenerateNestedObject(token, alObjectList, hmObjectInfo, index);
            generateNestedObject.generate(objectInfo);
            index = generateNestedObject.getIndex();
            objectInfo = generateNestedObject.getObjectInfo();
        } else if(hmTokenInfo.get(alToken.get(index)).getCatagory() ==  26) {   //this is a array
            GenerateArrayObject generateArrayObject = new GenerateArrayObject(token, alObjectList, hmObjectInfo);
            objectInfo = generateArrayObject.generate(objectInfo, index);
            index = generateArrayObject.getIndex();
        } else {    //this is a plain  object
            GeneratePlainObject generatePlainObject = new GeneratePlainObject(token, alObjectList, hmObjectInfo, index);
            generatePlainObject.generate(objectInfo, false);
            index = generatePlainObject.getIndex();
            objectInfo = generatePlainObject.getObjectInfo();
        }

        newAlObjectList.add(objectInfo.getObjectId());
        newHmObjectInfo.put(objectInfo.getObjectId(), objectInfo);
        
        return index;
    }
}