package com.ShamsNahid.Objects;

import com.ShamsNahid.Helper.Utility;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by bmshamsnahid on 4/21/17.
 *
 * Mainly evaluate object value expression
 * Example:
 *  _dec myVal = 1 + 3;
 *  Provide next identity of the equation sign
 * Here the object utility class return the myVal object with value 4
 * _dec myVal = ob1.ob2.ob3;
 * provide the next identity of the equation sign
 * Here the object utility class assign the nested object ob3 property to the myVal object
 *
 * Object tracing like ob1.ob2.ob3 is done by the ObjectTracking class
 *
 * All the expression evaluation is done by the native method evaluateObjectValueExpression
 */
public class ObjectUtility {

    private Utility utility;

    private ArrayList<Integer> alObjectList;
    private HashMap<Integer, ObjectInfo> hmObjectInfo;

    private ArrayList<Integer> alToken;
    private HashMap<Integer, TokenInfo> hmTokenInfo;

    private Token token;

    /**
     * Need the sibling and parent objectList and their information
    */
    public ObjectUtility(Token token, ArrayList<Integer> alObjectList, HashMap<Integer, ObjectInfo> hmObjectInfo) {

        utility = new Utility();

        this.alObjectList = alObjectList;
        this.hmObjectInfo = hmObjectInfo;

        alToken = token.getToken();
        hmTokenInfo = token.getHm();

        this.token = token;
    }

    /**
     * Here we dig the actual object value(plain object), property(nested and array object), the assign it
     *
     * _dec myVal = a + b;
     * alObjectValueExpression : a + b
     * objectInfo: Info of myVal
     *
     * Procedure:
     *  MiddleWare expression is an empty string, for objectValue we add the value the expression and finally evaluate
     *      Ex: 1 + ob1.ob2 + 3
     *          let ob1.ob2 = 5
     *          so the expression will be 1 + 5 + 3
     *  Traverse the whole expression
     *  Check if this is a parent object or not
     *      if this is a parent object then trace the end of it, may have child object
     *      using the objectTracking, get the end object
     *      Ex: for ob1.ob2.ob3 we get the ob3 info
     *      If retrieved object is a nested or array object just assign it to the current objectInfo and return
     *      else add them to the expression
     *  If a parent plain object, then add the value of the object to the expression
     *  If not a parent object, just value, then add the value
     *  Finally evaluate the expression using the eval method in utility class
     *      Ex: eval method will return the arithmetic value
     *          Input: "1+3"
     *          Returned Value: "4"
    */
    public ObjectInfo evaluateObjectValueExpression(ArrayList<Integer> alObjectValueExpression, ObjectInfo objectInfo) {

        ArrayList<Integer> alObjectArena = new ArrayList<>();
        alObjectArena.add(0, 0);
        String expression = "";

        for(Integer index=0; index<alObjectValueExpression.size(); index++) {
            int expressionValueIdentity = alObjectValueExpression.get(index);   //CURRENT keyword identity of the value expression
            String expressionValue = hmTokenInfo.get(alToken.get(expressionValueIdentity)).getKeyword();    //current keyword of the expression value
            ObjectTracking objectTracking;

            if(isParentObject(expressionValue) == true) { //this is a obejct
                objectTracking = new ObjectTracking(alObjectValueExpression, token, alObjectList, hmObjectInfo);
                objectTracking.trackObject(index);

                ObjectInfo retrievedObjectInfo = objectTracking.getObjectInfo();
                index = objectTracking.getIndex();

                if(retrievedObjectInfo != null) {
                    if(index != alObjectValueExpression.size() -1) {
                        index--;
                    }
                } else {
                }

                if(retrievedObjectInfo.getObjectCategory() == 3101) {
                    expression += retrievedObjectInfo.getObjectValue();
                } else {
                    objectInfo.setParentTree(retrievedObjectInfo.getParentTree());
                    objectInfo.setObjectValueExpression(retrievedObjectInfo.getObjectValueExpression());
                    objectInfo.setObjectValue(retrievedObjectInfo.getObjectValue());
                    objectInfo.setObjectCategory(retrievedObjectInfo.getObjectCategory());

                    objectInfo.setAlPropertyObjectsId(retrievedObjectInfo.getAlPropertyObjectsId());
                    objectInfo.setAlArrayElementsObjectsId(retrievedObjectInfo.getAlArrayElementsObjectsId());
                    objectInfo.setAlPropertyMethodsId(retrievedObjectInfo.getAlPropertyMethodsId());

                    objectInfo.setHmPropertyObjectInfo(retrievedObjectInfo.getHmPropertyObjectInfo());
                    objectInfo.setHmPropertyMethodInfo(retrievedObjectInfo.getHmPropertyMethodInfo());
                    
                    return objectInfo;
                }
            } else {    //just a arithmatic keyword, not a object
                expression += expressionValue;
            }
        }
        
        String expressionResults = "";
        if(expression.length() > 0) {
            try { expressionResults = Double.toString(utility.eval(expression)); } catch (Exception e) {
                objectInfo.setObjectValue(expression);
                objectInfo.setObjectValueExpression(alObjectValueExpression);
                return objectInfo;
            }
            objectInfo.setObjectValue(expressionResults);
            objectInfo.setObjectValueExpression(alObjectValueExpression);
            return objectInfo;
        } else {
            objectInfo.setObjectValue(expression);
            objectInfo.setObjectValueExpression(alObjectValueExpression);
            return objectInfo;
        }
    }

    public Boolean isParentObject(String objectName) {
        //System.out.println("In object utility class, isParentObject method: ");
        for(Integer identifier : alObjectList) {
            ObjectInfo objectInfo  = new ObjectInfo();
            objectInfo = hmObjectInfo.get(identifier);

            //System.out.println("Current object name: " + objectInfo.getObjectName() + "  Desired object name: " + objectName);

            if(objectInfo.getObjectName().equals(objectName) == true) {
                return true;
            }
        }
        return false;
    }
}
