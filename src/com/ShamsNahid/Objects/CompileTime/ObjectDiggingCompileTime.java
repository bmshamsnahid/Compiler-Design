package com.ShamsNahid.Objects.CompileTime;

import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

/**
 * Created by bmshamsnahid on 4/17/17.
 */
public class ObjectDiggingCompileTime {

    private ArrayList<Integer> alToken;
    private HashMap<Integer, TokenInfo> hmTokenInfo;
    private ArrayList<Integer> statement;

    public ArrayList<Integer> getStatement() {
        return statement;
    }

    public ObjectDiggingCompileTime(Token token) {
        alToken = token.getToken();
        hmTokenInfo = token.getHm();

        statement = new ArrayList<>();
    }

    /**
     * if this is a _dec statement, we dig the object
     * we will return on ly if there is a semicolon
     * if there is a opening second parenthesis, we track to the end of closing second parenthesis
     */
    public int objectHandle(int index) {

        while(true) {
            TokenInfo currentToken = hmTokenInfo.get(alToken.get(index));
            if(currentToken.getCatagory() == 71) { //if this is a semicolon
                //System.out.println("Got semicolon, end of the statement");
                statement.add(currentToken.getIdentity());
                index++;    //first index of the next portion
                break;
            } else {
                if(currentToken.getCatagory() == 24) {    //if this is a opening second parenthesis
                    //System.out.println("Opening second parenthesis found.");
                    index = nestedObject(index);
                    continue;
                } else {
                    statement.add(currentToken.getIdentity());
                    //System.out.println("Got general token");
                    index++;    //traverse to the next statement
                }
            }
        }

        return index;
    }

    private int nestedObject(int index) {

        Stack<Integer> stkTrackObject = new Stack<>();
        do {
            TokenInfo currentTokenInfo = hmTokenInfo.get(alToken.get(index));
            if(currentTokenInfo.getCatagory() == 24) {
                //System.out.println("Track opening second parenthesis");
                stkTrackObject.push(currentTokenInfo.getIdentity());
            } else if(currentTokenInfo.getCatagory() == 23) {
                //System.out.println("Track closing second parenthesis");
                stkTrackObject.pop();
            }
            statement.add(currentTokenInfo.getIdentity());
            //System.out.println("Adding general token." + currentTokenInfo.getKeyword());
            index++;
        } while(!stkTrackObject.isEmpty());
        //System.out.println("END OF NESTED OBJECT");
        return index;
    }

}
