package com.ShamsNahid.Objects;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Each GenericInfo has own objects.
 * Objects are kept by the id in an ArrayList
 * An HashMap is provided to give the full information of the objects according to their id
 *
 * NB: ONLY PARENT OBJECT OF THE GENERIC INFO IS KEPT HERE
 * Example:
 * void myGenericInfo() {
 *     _dec myObject = {
 *       _dec childObject;
 *     };
 * }
 * Here the myObject is only concerned about the myObject, not childObject
*/

public class MyObject {

    private ArrayList<Integer> alObjectList;
    private HashMap<Integer, ObjectInfo> hmObjectInfo;

    public ArrayList<Integer> getAlObjectList() {
        return alObjectList;
    }

    public void setAlObjectList(ArrayList<Integer> alObjectList) {
        this.alObjectList = alObjectList;
    }

    public HashMap<Integer, ObjectInfo> getHmObjectInfo() {
        return hmObjectInfo;
    }

    public void setHmObjectInfo(HashMap<Integer, ObjectInfo> hmObjectInfo) {
        this.hmObjectInfo = hmObjectInfo;
    }
}
