package com.ShamsNahid.Objects.ObjectGenerator;

import com.ShamsNahid.Objects.ObjectDigging;
import com.ShamsNahid.Objects.ObjectInfo;
import com.ShamsNahid.Objects.ObjectUtility;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;


class ElementsAndIndexContainer {
    public ArrayList<Integer> alArrayElements;
    public Integer index;
}

/**
 * Created by bmshamsnahid on 5/3/17.
 */
public class GenerateArrayObject {

    private Token token;
    private ArrayList<Integer> alToken;
    private HashMap<Integer, TokenInfo> hmTokenInfo;

    private ArrayList<Integer> alObjectList;
    private HashMap<Integer, ObjectInfo> hmObjectInfo;

    private int index;

    public Integer getIndex() {
        return index;
    }

    public GenerateArrayObject(Token token, ArrayList<Integer> alObjectList, HashMap<Integer, ObjectInfo> hmObjectInfo) {

        this.token = token;
        this.alToken = token.getToken();
        this.hmTokenInfo = token.getHm();

        this.alObjectList = new ArrayList<>();
        this.hmObjectInfo = new HashMap<>();

        this.alObjectList.addAll(alObjectList);
        this.hmObjectInfo.putAll(hmObjectInfo);
    }

    /*
    */
    public ObjectInfo generateTempObject(ArrayList<Integer> alParentTree, String objectName, ArrayList<Integer> alObjectValueExpression, Integer objectCategory) {
        ObjectInfo objectInfo = new ObjectInfo();
        ArrayList<Integer> objectParentTree = new ArrayList<>();

        Integer objectId = new ObjectDigging(token).getNewObjectId();

        objectParentTree.addAll(alParentTree);
        objectInfo.setObjectValueExpression(alObjectValueExpression);

        objectInfo.setObjectName(objectName);
        objectInfo.setObjectId(objectId);
        objectInfo.setObjectCategory(objectCategory);

        //objectInfo.setPlain(true);  //initially assume this is a plain object
        objectParentTree.add(objectId);
        objectInfo.setParentTree(objectParentTree);

        //setting object value from the value expression
        //objectInfo = objectUtility.evaluateObjectValueExpression(alObjectValueExpression, objectInfo);

        if(objectInfo.getObjectCategory() == 3102) {
            GenerateNestedObject generateNestedObject = new GenerateNestedObject(token, alObjectList, hmObjectInfo, alObjectValueExpression.get(0));
            generateNestedObject.generate(objectInfo);
            objectInfo = generateNestedObject.getObjectInfo();
        } else if(objectInfo.getObjectCategory() == 3101) {
            GeneratePlainObject generatePlainObject = new GeneratePlainObject(token, alObjectList, hmObjectInfo, alObjectValueExpression.get(0));
            generatePlainObject.generate(objectInfo, true);
            objectInfo = generatePlainObject.getObjectInfo();
        } else if(objectInfo.getObjectCategory() == 3103) {
            GenerateArrayObject generateArrayObject = new GenerateArrayObject(token, alObjectList, hmObjectInfo);
            objectInfo = generateArrayObject.generate(objectInfo, alObjectValueExpression.get(0));
        }

        alObjectList.add(objectInfo.getObjectId());
        hmObjectInfo.put(objectInfo.getObjectId(), objectInfo);

        return  objectInfo;
    }

    public ObjectInfo generate(ObjectInfo objectInfo, Integer index) {

        /*System.out.println("ARENA OF THE NEW ARRAY OBJECT HAVE TO DIG THE ELEMENTS");
        System.out.println("------------------------------------------------------");

        System.out.println("Name: " + objectInfo.getObjectName());
        System.out.println("Id: " + objectInfo.getObjectId());
        System.out.println("Tree: " + objectInfo.getParentTree());

        System.out.println("------------------------------------------------------");*/

        //since this is a array type object then here we introduce the object list and the object category
        ArrayList<Integer> alObjectArrayElements = new ArrayList<>();
        Integer objectCategory = 3103;

        ArrayList<Integer> alPropertyObjectsId = new ArrayList<>();
        HashMap<Integer, ObjectInfo> hmPropertyObjectInfo = new HashMap<>();

        ArrayList<Integer> alArrayBody = new ArrayList<>();

        Stack<Integer> stkTraceArrayBoody  = new Stack<>();

        do {
            TokenInfo tokenInfo = new TokenInfo();
            tokenInfo = hmTokenInfo.get(alToken.get(index));
            if(tokenInfo.getCatagory() == 26) {
                stkTraceArrayBoody.push(tokenInfo.getIdentity());
            } else if(tokenInfo.getCatagory() == 25) {
                stkTraceArrayBoody.pop();
            }

            alArrayBody.add(tokenInfo.getIdentity());

            index++;
        } while(stkTraceArrayBoody.empty() == false);

        alArrayBody.remove(alArrayBody.size()-1);
        alArrayBody.remove(0);

        /*System.out.print("ArrayBody: ");
        for(Integer identity : alArrayBody) {
            System.out.print(hmTokenInfo.get(alToken.get(identity)).getKeyword() + " ");
        }
        System.out.println();
        System.out.println();*/

        for(Integer identity=0; identity<alArrayBody.size(); identity++) {
            TokenInfo tokenInfo = hmTokenInfo.get(alToken.get(alArrayBody.get(identity)));
            if(tokenInfo.getCatagory() == 70) {
                continue;
            } else if(tokenInfo.getCatagory() == 24) {
                ElementsAndIndexContainer elementsAndIndexContainer = getElementBody(24, 23, identity, alArrayBody);
                identity = elementsAndIndexContainer.index;

                ObjectInfo elementObjectInfo = generateTempObject(objectInfo.getParentTree(), "ArrayElement", elementsAndIndexContainer.alArrayElements, 3102);

                alObjectArrayElements.add(elementObjectInfo.getObjectId());
                //alPropertyObjectsId.add(elementObjectInfo.getObjectId());
                hmPropertyObjectInfo.put(elementObjectInfo.getObjectId(), elementObjectInfo);
            } else if(tokenInfo.getCatagory() == 26) {
                ElementsAndIndexContainer elementsAndIndexContainer = getElementBody(26, 25, identity, alArrayBody);
                identity = elementsAndIndexContainer.index;

                ObjectInfo elementObjectInfo = generateTempObject(objectInfo.getParentTree(), "ArrayElement", elementsAndIndexContainer.alArrayElements, 3103);

                alObjectArrayElements.add(elementObjectInfo.getObjectId());
                //alPropertyObjectsId.add(elementObjectInfo.getObjectId());
                hmPropertyObjectInfo.put(elementObjectInfo.getObjectId(), elementObjectInfo);
            } else {
                ElementsAndIndexContainer elementsAndIndexContainer = getElementBody(70, identity, alArrayBody);
                identity = elementsAndIndexContainer.index;

                ObjectInfo elementObjectInfo = generateTempObject(objectInfo.getParentTree(), "ArrayElement", elementsAndIndexContainer.alArrayElements, 3101);

                alObjectArrayElements.add(elementObjectInfo.getObjectId());
                //alPropertyObjectsId.add(elementObjectInfo.getObjectId());
                hmPropertyObjectInfo.put(elementObjectInfo.getObjectId(), elementObjectInfo);
            }
        }

        this.index = index;

        //objectInfo.setAlPropertyObjectsId(alPropertyObjectsId);
        objectInfo.setAlArrayElementsObjectsId(alObjectArrayElements);
        objectInfo.setHmPropertyObjectInfo(hmPropertyObjectInfo);

        objectInfo.setObjectCategory(3103);

        return objectInfo;
    }

    private ElementsAndIndexContainer getElementBody(Integer pushCategory, Integer popCategory, Integer index, ArrayList<Integer> alArrayBody) {
        Stack<Integer> stkElementBody = new Stack<>();
        ArrayList<Integer> alElementBody = new ArrayList<>();

        do {
            TokenInfo tokenInfo = hmTokenInfo.get(alToken.get(alArrayBody.get(index)));
            if(tokenInfo.getCatagory() == pushCategory) {
                stkElementBody.push(tokenInfo.getIdentity());
            } else if(tokenInfo.getCatagory() == popCategory) {
                stkElementBody.pop();
            }
            alElementBody.add(tokenInfo.getIdentity());
            index++;
        } while(!stkElementBody.isEmpty());

        /*System.out.print("    UNIT ELEMENT BODY: ");
        for(Integer identity : alElementBody) {
            System.out.print(hmTokenInfo.get(alToken.get(identity)).getKeyword() + " ");
        }
        System.out.println();*/


        ElementsAndIndexContainer elementsAndIndexContainer = new ElementsAndIndexContainer();
        elementsAndIndexContainer.alArrayElements = alElementBody;
        elementsAndIndexContainer.index = index;

        return elementsAndIndexContainer;
    }

    private ElementsAndIndexContainer getElementBody(Integer endCategory, Integer index, ArrayList<Integer> alArrayBody) {
        ArrayList<Integer> alElementBody = new ArrayList<>();

        do {
            if(alArrayBody.size() <= index) {
                break;
            }
            TokenInfo tokenInfo = hmTokenInfo.get(alToken.get(alArrayBody.get(index)));
            if(tokenInfo.getCatagory() == endCategory) {
                break;
            }
            alElementBody.add(tokenInfo.getIdentity());
            index++;
        } while(true);

        /*System.out.print("\n    UNIT ELEMENT BODY: ");
        for(Integer identity : alElementBody) {
            System.out.print(hmTokenInfo.get(alToken.get(identity)).getKeyword() + " ");
        }
        System.out.println();*/


        ElementsAndIndexContainer elementsAndIndexContainer = new ElementsAndIndexContainer();
        elementsAndIndexContainer.alArrayElements = alElementBody;
        elementsAndIndexContainer.index = index;

        return elementsAndIndexContainer;
    }

}
