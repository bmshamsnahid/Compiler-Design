package com.ShamsNahid.Objects.ObjectGenerator;

import com.ShamsNahid.Objects.ObjectInfo;
import com.ShamsNahid.Objects.ObjectUtility;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by bmshamsnahid on 5/3/17.
 */
public class GeneratePlainObject {

    private ObjectInfo objectInfo;

    private Token token;
    private ArrayList<Integer> alToken;
    private HashMap<Integer, TokenInfo> hmTokenInfo;

    private ArrayList<Integer> alObjectList;
    private HashMap<Integer, ObjectInfo> hmObjectInfo;

    private Integer index;

    public ObjectInfo getObjectInfo() {
        return this.objectInfo;
    }

    public Integer getIndex() {
        return index;
    }

    public GeneratePlainObject(Token token, ArrayList<Integer> alObjectList, HashMap<Integer, ObjectInfo> hmObjectInfo, Integer index) {
        this.token = token;
        this.alToken = token.getToken();
        this.hmTokenInfo = token.getHm();

        this.alObjectList = alObjectList;
        this.hmObjectInfo = hmObjectInfo;

        this.index = index;
    }

    /**
     * We dig the plain object here
     * Here we received a plain object information except the object's value expression
     * Object value expression is started from, after the equation sign till the comma or semicolon
     *
     * So traverse just after the equation sign to the comma or semicolon and find the expression
     * Add the expression to the objcetInfo
     * Return the completed object
     *
     **Example
     * _dec a = 5 + 6;
     * here we traverse 5+7 and add it to the object information
     */

    public void generate(ObjectInfo objectInfo, Boolean isArray) {  //the object identifier contains the object name i.e just after the _dec or a comma keyword
        objectInfo.setObjectCategory(3101);
        ArrayList<Integer> alObjectValueExpression = new ArrayList<>();

        while(true) {
            int tokenCategory = hmTokenInfo.get(alToken.get(index)).getCatagory();
            /**
             * _dec val = [1, 2];
             * here 1 is a plain object and 2 is a plain object. value of the oject 2 is ended by the ]
             * hence when it comes to array then it will end with third parenthesis
            */
            if(isArray && tokenCategory == 25) {
                break;
            }
            if(tokenCategory == 70 || tokenCategory == 71) { //if this is a comma or semicolon or third parenthesis for array
                break;
            } else {    //still in object value
                alObjectValueExpression.add(hmTokenInfo.get(alToken.get(index)).getIdentity());
            }
            index++;
        }

        ObjectUtility objectUtility = new ObjectUtility(token, alObjectList, hmObjectInfo);
        objectInfo = objectUtility.evaluateObjectValueExpression(alObjectValueExpression, objectInfo);

        //System.out.println("category of the plain object: " + objectInfo.getObjectCategory());

        this.objectInfo = objectInfo;
    }

}
