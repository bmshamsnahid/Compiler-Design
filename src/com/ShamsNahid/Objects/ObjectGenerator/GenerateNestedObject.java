package com.ShamsNahid.Objects.ObjectGenerator;

import com.ShamsNahid.Objects.MyObject;
import com.ShamsNahid.Objects.ObjectDigging;
import com.ShamsNahid.Objects.ObjectInfo;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;
import com.ShamsNahid.method.MethodInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

/**
 * Created by bmshamsnahid on 5/3/17.
 */
public class GenerateNestedObject {

    private ObjectInfo objectInfo;

    private Token token;
    private ArrayList<Integer> alToken;
    private HashMap<Integer, TokenInfo> hmTokenInfo;

    private MyObject newObjectContainer;
    private ArrayList<Integer> alObjectList;
    private HashMap<Integer, ObjectInfo> hmObjectInfo;

    private Integer index;

    public ObjectInfo getObjectInfo() {
        return this.objectInfo;
    }

    public Integer getIndex() {
        return index;
    }

    public GenerateNestedObject(Token token, ArrayList<Integer> alObjectList, HashMap<Integer, ObjectInfo> hmObjectInfo, Integer index) {
        this.token = token;
        this.alToken = token.getToken();
        this.hmTokenInfo = token.getHm();

        this.alObjectList = alObjectList;
        this.hmObjectInfo = hmObjectInfo;

        newObjectContainer = new MyObject();

        newObjectContainer.setAlObjectList(alObjectList);
        newObjectContainer.setHmObjectInfo(hmObjectInfo);

        this.index = index;
    }


    /**
     * We dig the nested object here
     * Nested object can have child object and property method
     * So here we dig the internal object and methods and add their id and information to the parent object
     *
     * We track the whole nested object body (inside the resonable second parenthesis)
     *      If we find a object then we dig it to the generateObjcet() method
     *      Add the founded method to this parent object's property
     *      Have to do same to the methods
     *
     * Since we assume all objects are plain, here we change this object property isPlain = false
     * After adding all the property method and object, just return this object
     */

    public void generate(ObjectInfo objectInfo) {
        //System.out.println("                    HERE IN THE NESTED OBJECT START WITH: " + hmTokenInfo.get(alToken.get(index)).getKeyword());
        objectInfo.setObjectCategory(3102);
        objectInfo.setObjectValueExpression(trackNestedObject(index));

        ArrayList<Integer> alPropertyObjectsId = new ArrayList<>();
        ArrayList<Integer> alPropertyMethods = new ArrayList<>();

        HashMap<Integer, MethodInfo> hmPropertyMethodInfo = new HashMap<>();
        HashMap<Integer, ObjectInfo> hmPropertyObjectInfo = new HashMap<>();

        Stack<Integer> stackObjectTrace = new Stack<>();

        do {
            if(hmTokenInfo.get(alToken.get(index)).getCatagory() == 11 || hmTokenInfo.get(alToken.get(index)).getCatagory() == 70) {   //_dec keyword or a comma
                index ++; //currently in the dec keyword, jump to the object name

                ObjectDigging objectDigging = new ObjectDigging(token);

                index = objectDigging.cookObject(index, newObjectContainer, objectInfo.getParentTree());

                alPropertyObjectsId.addAll(objectDigging.getNewObjectContainer().getAlObjectList());
                hmPropertyObjectInfo.putAll(objectDigging.getNewObjectContainer().getHmObjectInfo());
            } else {
                if(hmTokenInfo.get(alToken.get(index)).getCatagory() == 24) {
                    stackObjectTrace.push(hmTokenInfo.get(alToken.get(index+3)).getIdentity());
                } else if(hmTokenInfo.get(alToken.get(index)).getCatagory() == 23) {
                    stackObjectTrace.pop();
                }
                index ++;
            }
        } while(!stackObjectTrace.isEmpty());

        objectInfo.setAlPropertyObjectsId(alPropertyObjectsId);
        objectInfo.setAlPropertyMethodsId(alPropertyMethods);

        objectInfo.setHmPropertyObjectInfo(hmPropertyObjectInfo);
        objectInfo.setHmPropertyMethodInfo(hmPropertyMethodInfo);

        //objectInfo.setPlain(false);

        this.objectInfo = objectInfo;
    }

    /**
     * Here we get the first index of a nested object, that is {
     * And we traverse through the resionable }
     * then all the keyword identity will be send
     *
     * *EXAMPLE
     * obj1 = { _dec a = { _dec b = 5; }; };
     * for object obj1 we return { _dec a = { _dec b = 5; }; }
     * for object a we return { _dec b = 5; }
     */

    private ArrayList<Integer> trackNestedObject(int bodyIndex) {
        ArrayList<Integer> nestedObjectValueExpression = new ArrayList<>();

        Stack<Integer> stkNestedObjectBody = new Stack<>();

        do {
            TokenInfo currentToken = hmTokenInfo.get(alToken.get(bodyIndex));

            if(currentToken.getCatagory() == 24) {
                stkNestedObjectBody.push(currentToken.getIdentity());
            }

            if(currentToken.getCatagory() == 23) {
                stkNestedObjectBody.pop();
            }

            nestedObjectValueExpression.add(currentToken.getIdentity());

            bodyIndex ++;

        } while(!stkNestedObjectBody.isEmpty());

        return  nestedObjectValueExpression;
    }

}
