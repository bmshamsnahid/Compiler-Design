package com.ShamsNahid.method;

import java.util.ArrayList;
import java.util.HashMap;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;
import com.ShamsNahid.Generic.GenericDigging;
import com.ShamsNahid.Generic.GenericInfo;
import com.ShamsNahid.Helper.Utility;
/////////////////////////////////TASK///////////////////////////////
/*
 * Have to extend the class to generate the body and conditional part
 * That class will extend the method investigation to investigate the condition and execution the method body statements
*/

/*
 * HERE WE SEPARATE METHOD BODY AND PARAMETER
 * -> Receive a method identity
 * -> Then we separate method parameter part and body part
 * -> Then parameter part and body part will be investigate in MethodInvestigation class
*/
public class MethodDigging {
	
	private Token token;	//we will receive this through constructor
	private ArrayList<Integer> alToken; 	//Then all token will be kept here
	private HashMap<Integer, TokenInfo> hmTokenInfo;	//Each token info is stored here for regarding its id

	private MethodInfo methodInfo;	//bitch here 
		
	private Integer methodIdentifier;	//receive from methodDigging class
	
	GenericDigging genericDigging;
	
	
	//GETTER FOR METHOD INFO
	public MethodInfo getMethodInfo() {
		return methodInfo;
	}
	
	//CONSTRUCTOR
	/*
	 * RECEIVE A METHOD IDENTIFIER, GENERATE AND STORE THE MEHTOD INFORMATION
	 * 
	*/
	public MethodDigging(Token token, Integer methodIdentifier) {
		this.token = token;
		this.alToken = token.getToken();
		this.hmTokenInfo = token.getHm();
		this.methodIdentifier = methodIdentifier;
		
		methodInfo = new MethodInfo();
		digMethod();
	}
	
	//for loop digging placed here
	public int digMethod() {
		
		genericDigging = new GenericDigging(token, methodIdentifier);
		int index = genericDigging.digGenericInfo();	//the last identifier (index + 1) of the loop is store in the index
		GenericInfo genericInfo = genericDigging.getgenericInfo();
		methodInfo.setGenericInfo(genericInfo);
		
		//System.out.println("//****Parameter Portion of  a Method: ");
		//new Utility().printGenericElements(genericInfo.getHeadPortion(), hmTokenInfo, alToken);
		//System.out.println("//****Filter body of  a Method: ");
		//new Utility().printGenericElements(genericInfo.getFilterBody(), hmTokenInfo, alToken);
		
		return index;	//the next index of the for loop territory is placed here
	}
	
}
