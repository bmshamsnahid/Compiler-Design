package com.ShamsNahid.method;

import com.ShamsNahid.Generic.GenericInfo;

public class MethodInfo extends GenericInfo {
	private GenericInfo genericInfo;

	public GenericInfo getGenericInfo() {
		return genericInfo;
	}

	public void setGenericInfo(GenericInfo genericInfo) {
		this.genericInfo = genericInfo;
	}
}
