package com.ShamsNahid.CompilerDesign;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ShamsNahid.Helper.Utility;

public class ValidationChecker {
	
	private ArrayList<String> alExpression;
	
	
	
	public ValidationChecker() {
		//alExpression.add();
	}
	
	public boolean checkValidation(String str) {
		
		Utility utility = new Utility();
		str = utility.removeSpace(str);
		
		Scanner sc = new Scanner(System.in);
		str = sc.nextLine();
		String text = "";
		text = str;
		
		
		//unknown_statement if (unknown_condition) unknown_statement;
		String expressionIf = "if[(].*[)].*;";
		Pattern patternIf = Pattern.compile(expressionIf);
		Matcher matchIf = patternIf.matcher(text);
		
		//unknown_statement if (unknown_condition) {unknown_statement;}
		String expressionIfExtended = "if[(].*[)].*[{].*[;][}]";
		Pattern patternIfExtended = Pattern.compile(expressionIf);
		Matcher matchIfExtended = patternIfExtended.matcher(text);
		
		//unknown_statement if (unknown_condition) unknown_statement ; else unknown_statement ;
		String expressionIfElse = "if[(].*[)].*[;]else.*[;]";
		Pattern patternIfElse = Pattern.compile(expressionIfElse);
		Matcher matchIfElse = patternIfElse.matcher(text);
		
		//unknown_statement if (unknown_condition) { unknown_statement ;} else {unknown_statement ;}
		String expressionIfElseExtended = "if[(].*[)][{].*[;][}]else[{].*[;][}]";
		Pattern patternIfElseExtended = Pattern.compile(expressionIfElseExtended);
		Matcher matchIfElseExtended = patternIfElseExtended.matcher(text);
		
		
		if((matchIf.find() == true)||(matchIfExtended.find() == true)||(matchIfElse.find() == true)||(matchIfElseExtended.find() == true)) {
			System.out.println("Matched");
			return true;
		} else {
			System.out.println("not Matched");
			return false;
		}
		
		/*if(matchIfElseExtended.find() == true) {
			System.out.println("Matched");
			return true;
		} else {
			System.out.println("not Matched");
			return false;
		}*/
		
	}

}
