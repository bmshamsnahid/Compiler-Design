package com.ShamsNahid.CompilerDesign;

import com.ShamsNahid.Execution.ExecuteCode;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenDigging;
import com.ShamsNahid.ParentContainer.ParentContainerInfo;
import com.ShamsNahid.ParentContainer.ParentContainerDigging;
import com.ShamsNahid.test.TraverseCode;

class Main {
	
	public static void main(String[] args) {
		//test on new machine
		
		
		TokenDigging tokenDigging =  new TokenDigging();		//Tokenigation is started and token will be ready
		Token token = tokenDigging.getToken();		//received the tokens
		
		ParentContainerDigging parentContainerDigging = new ParentContainerDigging(token); //working on symbol table
		ParentContainerInfo parentContainerInfo = parentContainerDigging.getParentContainerInfo();	//retrive the symbol table

		TraverseCode traverseCode;
		traverseCode = new TraverseCode(parentContainerInfo, token);
		traverseCode.printSymbolTable();
		
		ExecuteCode executeCode = new ExecuteCode(parentContainerInfo, token);
		executeCode.execute();
		
		traverseCode = new TraverseCode(parentContainerInfo, token);
		traverseCode.printSymbolTable();
	}

}
	