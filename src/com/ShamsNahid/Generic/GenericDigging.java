package com.ShamsNahid.Generic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import com.ShamsNahid.Condition.Else.ElseConditionDigging;
import com.ShamsNahid.Condition.Else.ElseConditionInfo;
import com.ShamsNahid.Condition.ElseIf.ElseIfConditionDigging;
import com.ShamsNahid.Condition.ElseIf.ElseIfConditionInfo;
import com.ShamsNahid.Condition.IF.IfConditionDigging;
import com.ShamsNahid.Condition.IF.IfConditionInfo;
import com.ShamsNahid.Helper.Utility;
import com.ShamsNahid.Loop.For.ForLoopDigging;
import com.ShamsNahid.Loop.For.ForLoopInfo;
import com.ShamsNahid.Loop.While.WhileLoopDigging;
import com.ShamsNahid.Loop.While.WhileLoopInfo;
import com.ShamsNahid.Objects.CompileTime.ObjectDiggingCompileTime;
import com.ShamsNahid.Objects.MyObject;
import com.ShamsNahid.Objects.ObjectDigging;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;
import com.ShamsNahid.Objects.ObjectInfo;

/*
 * WHAT IS DONE HERE BETCHA.....
 * 
 * All loops(for, while), method and conditions(if, else if and else) will be digged here
 * Loop, method or conditions have two portions, header portions and body portions
 * Each loop statements goes to the body portion and the loop starting, ending, loop limits goes to the header portion
 * Except the else, if and else if has a condition and statements.
 * All condition goes to the head portions and statements goes to the body portion
 * For a loop or condition, there may be nested loop or condition
 * If there is nested loop or condtition, in filter body we only keep the main part of the loop and nested loop or condition first identifier
 * Nested loop or condition will be then handled later, in recursive mode
 * In body portion we keep the all elements, including all the nested elements
 * for a object declaration, we handle it in the ObjectDiggingCompileTime class, it send a statement, just add the statement tot hte statement list
 * 
 * In the body each statement will be separeted and stored in the genericInfo
*/

/*
 * HOW IS DONE HERE ....
 * 
 * First we get the token object and the tokens info, like, token identifier(alToken) and their corresponding information(hmTokenInfo)
 * Then declare and initialize the GenericInfo Class(Check this class)..MC MyObject
 * We have two portion, header portion and body portion
 * And nested loop and condition may contain
 * Declare and initialize the both portion and loop, condition container(HashMap)
 * We track the header portion(Except else condition) by tracking the first parenthesis
 * And we track the body portion by tracking by second parenthesis(multiple statements) or the till the semicolon(single statements)
 * Then save the info to the genericInfo class
 * 
 * In the body each semicolon stands for a end of new statement
 * generic element identity also stand alone for a new statement  
*/
public class GenericDigging extends GenericInfo {
	
	private Token token;	//all the tokens stored here, received via the constructor
	private ArrayList<Integer> alToken; 	//from token, list of the token identity will be retrieved and kept here
	private HashMap<Integer, TokenInfo> hmTokenInfo;	//also from token, each tokens info is stored according to their identity
	private Integer gElementIdentifier; 	//this is the current loop or condition identity
	private ArrayList<Integer> statement;

	//for various help purpose
	private Utility utility;
	private ObjectDigging objectDigging;    //to dig a object when discover a _dec in a body statement
	
	private GenericInfo genericInfo;	//after process the generic info of the element will be found here
	private ArrayList<Integer> headPortion;	//here is the loop condition, conditional statements condition and method parameters part stored
	private ArrayList<Integer> filterBody;	//No full statement of a loop, condition exist, only the first keyword
	private ArrayList<Integer> body; 	//body of the current elements
	private ArrayList<ArrayList<Integer>> statementList;

	//in the current elements, if there is a nested loop or condition, then their information will be placed here
	private HashMap<Integer, ForLoopInfo> hmForLoopInfo; 	//nested for loop
	private HashMap<Integer, WhileLoopInfo> hmWhileLoopInfo;	//nested while loop
	private HashMap<Integer, IfConditionInfo> hmIfConditionInfo;	//nested if condition
	private HashMap<Integer, ElseIfConditionInfo> hmElseIfConditionInfo;	// nested else if
	private HashMap<Integer, ElseConditionInfo> hmElseConditionInfo;

	//GETTER
	public GenericInfo getgenericInfo() {
		return genericInfo;		//send the elements generic information to the parent object
	}
	
	//CONSTRUCTOR
	public GenericDigging(Token token, Integer gElementIdentifier) {
		this.token = token;	//store the token in local variable
		alToken = token.getToken();		//retrieve the tokens identifier in the arrayList alToken
		hmTokenInfo = token.getHm();	//retrieve the token information according their identifier and kept in the hashMap hmTokenInfo

        utility = new Utility();    //instantiate the object

		this.gElementIdentifier = gElementIdentifier;	//store the current element identifier in the local variable
		
		//NOW INITIALIZE THE LOCAL OBJECTS	
		//mc object
		genericInfo = new GenericInfo();
		//two main portion
		headPortion = new ArrayList<>();
		filterBody = new ArrayList<>();
		body = new ArrayList<>();
		statementList = new ArrayList<>();

		
		//nested elements
		hmForLoopInfo = new HashMap<>();
		hmWhileLoopInfo = new HashMap<>();
		hmIfConditionInfo = new HashMap<>();
		hmElseIfConditionInfo = new HashMap<>();
		hmElseConditionInfo =  new HashMap<>();
		hmObjectInfo = new HashMap<>();
	}
	
	public int digGenericInfo() {
		int index = alToken.indexOf(gElementIdentifier);	//get the index of the current element first identifier, we start digging from here
		
		/*
		 * Here we first dig the header portion, get the first index of the head portion
		 * Send the first index of the body portion to the get body method
		 * It will return the first index of the next elements portion
		*/
		index = getBody(getHeadPortion(index));	//after completing the digging, we store the last+1 index (first index of the next element portion)
		
		/*
		 * Got all the generic information
		 * Feeding the information to the genericInfo object
		 * some property that will be needed later, will be initialized and set here
		*/
        MyObject myObject = new MyObject();
        myObject.setAlObjectList(new ArrayList<Integer>());
        myObject.setHmObjectInfo(new HashMap<Integer, ObjectInfo>());

		genericInfo.setIdentity(gElementIdentifier);
		genericInfo.setKeyword("");/////////////////////////////////////////////////////////////
		genericInfo.setHeadPortion(headPortion);
		genericInfo.setFilterBody(filterBody);
		genericInfo.setBody(body);
		genericInfo.setInformation("");////////////////////////////////////////////////////////
        genericInfo.setMyObject(myObject);
		
		genericInfo.setHmForLoopInfo(hmForLoopInfo);
		genericInfo.setHmWhileLoopInfo(hmWhileLoopInfo);
		genericInfo.setHmIfConditionInfo(hmIfConditionInfo);
		genericInfo.setHmElseIfConditionInfo(hmElseIfConditionInfo);
		genericInfo.setHmElseConditionInfo(hmElseConditionInfo);
		genericInfo.setHmvariableInfo(hmObjectInfo);
		genericInfo.setStatementList(statementList);
		
		return index;	//return to the first index of the next portion/next element
	}
	
	/*
	 * It must start with the opening first parenthesis OTHERWISE NO HEADER SECTION EXIST(else condition)
	 * So in stack we push the opening first parenthesis
	 * If there is another opening first parenthesis then we push it to the stack
	 * Else if there is a closing first parenthesis, then we popped the top opening first parenthesis from the stack
	 * Each token identifier is pushed to the header portion
	 * We go through till the stack is empty 
	*/
	private int getHeadPortion(int index) {
		index++;	//get to the first identifier, i.e opening first parenthesis
		
		//checking the header section existence
		if(hmTokenInfo.get(alToken.get(index)).getCatagory() != 22) {	//since this is not a opening first parenthesis, so no header section exist
			return index;
		}
		
		Stack<Integer> stkHeadPortion = new Stack<Integer>();	//here we track the parenthesi
		
		do {
			TokenInfo tokenInfo = new TokenInfo();	//for each identifier: identifier's token info will be placed here
			tokenInfo = hmTokenInfo.get(alToken.get(index));	//store the current token information using the token identifier from hash map
			Integer tokenIdentity = tokenInfo.getIdentity();	//get token identity, it will be stored in the header portion
			Integer tokenCategory = tokenInfo.getCatagory();	//get token category, check the token is a opening or closing first parenthesis
			
			if(tokenCategory == 22) {	//this is a opening first parenthesis
				stkHeadPortion.push(tokenIdentity);	//hence the parenthesis is inserted to the stack
			} else if(tokenCategory == 21) {	//this is the closing first parenthesis 
				stkHeadPortion.pop();	//hence we removed the top opening first parenthesis
			}
			
			headPortion.add(tokenIdentity);  //element in the parameter territory is added here
			
			index++;	//now try to get the next identifier
		} while(!stkHeadPortion.isEmpty());		//till the stack is not empty, i.e. condition territory is ended
		
		return index; // return to the first index of the body portion
	}
	
	/*
	 * Here may be single statement or multiple statements
	 *
	 * EXAMPLE OF SINGLE AND MULTIPLE STATEMENT
	 * if(condition) { statemetn 1; statement 2; }     is a multiple statement
	 * if(condition) statement;                        is a single statement
	 *
	 * If there is a single statement then there may not a second parenthesis
	 * For multiple parenthesis, there is obviously a second parenthesis
	 * If the body portion is started with a opening second parenthesis, then we track the second parenthesis to get the whole body (to the end) in stack
	 * Otherwise track till the semicolon (may be a single statement)
	 * On both case, there may be nested condition or loop
	 *
	 * EXAMPLE OF THE NESTED CONDIITON
	 * if(condition1) if(condition2) display_something;
	 * if(condition1) { if(condition2) display_something; }
	 *
	 * Nested loop or condition will be handle in separate method and after digging the nested portion
	 * It will return the first index of the end of nested portion
	*/

	/*
	 * in the body if there is only one statement then, simply kept it in the statement array
	 * A statement is ended by a semicolon (except generic elements)
	 * also generic element(loop, condition) stands for a one statement
	 * EXAMPLE
	 * if(condition) for(condition) {statement 1; statement 2;}
	 * here for is considered as a single statement for parent if condition generic info
	 *
	 * FOR A STATEMENT
	 * declare new statement object
	 * while we are not to the last element, simply add the identity of the token to the statement arrayList
	 * when we found a semicolon or a generic element just set the startStement true
	 * -> add the statement to the statement list and start statement as false
	 * -> declare new statement object
	*/
	private int getBody(int index) {	//here index is the first elements position of the body
		Stack<Integer> stkBody = new Stack<Integer>();	//if there  is multiple element, then we track the second parenthesis here

		/*
		 * In this method single statement is found if no opening second parenthesis is found in the first position
		 * Generic category is not equal 24 means, only one statement exist
		 * That can be a condition or loop or a statement
		*/
		if(hmTokenInfo.get(alToken.get(index)).getCatagory() != 24) {	//this is not the opening second parenthesis, i.e. has only one single statement
			//so this could be a just statements or loop or condition

            //here we tore the statements of the generic elements body
            statement = new ArrayList<>();	//declare the object

            if(hmTokenInfo.get(alToken.get(index)).getGenericCatagory() == 101) {	//if this is a loop(for/while/do while)
				index = loopHandle(index);	//this is a loop, so this is handled by the loopHandle method
				
				//since one statement
				statement.add(index);	//add the identity of the generic element, it's a statement with only one element
                statementList.add(statement);	//add the statement to the statement list
			}
			else { //no loop or condition, hence only single statements
				while(true) {	//traverse till we get a semicolon
					int tokenIdentity = hmTokenInfo.get(alToken.get(index)).getIdentity();	//get the current token identity
					filterBody.add(tokenIdentity);	//get the token identity and add to the methodBody ArrayList
					statement.add(tokenIdentity);	//add the identity of the token to the statement
					if(hmTokenInfo.get(alToken.get(index)).getCatagory() == 71) {	//for category value check the CATEGORY INFORMATION FOLDER
                        statementList.add(statement);	//since this is a generic element, this is a statement of only one element
						break;		//since there is single statement, loop is breaked by a ;
					}
					index++;		//to handle the next token
				}
				index++; 	//cause method digging is completed and now we can focus on next portion
			}
		} else {	//has multiple statements, started by opening second parenthesis and ended by closing second parenthesis
			boolean startStatement = false;	//initially this is not a start statement ie. first time do not want to declare the startStatement object in the loop
			statement = new ArrayList<>();	//declare object before enter the loop
			do {	//traversing till the stack is empty
				if(startStatement == true) {	//end of statement is shown, need to start a new statement and store previous
					startStatement = false;		//now start a new statement

                    statementList.add(statement);	//add the previous statement to the statement list
					statement = new ArrayList<>();	//declare the new statement
				}
				//checking if there is a nested loop or condition
				int genericCategory = hmTokenInfo.get(alToken.get(index)).getGenericCatagory();
				int tokenIdentity = hmTokenInfo.get(alToken.get(index)).getIdentity();
				int tokenCategory = hmTokenInfo.get(alToken.get(index)).getCatagory();
				
				if(genericCategory == 101 || genericCategory == 102) {	//if this is a loop(for/while/do while -> 101) or condition(if/else/else if -> 102)
					startStatement = true;	//since this is a generic element, have to store this and create new statement
					statement.add(tokenIdentity);	//add the token identity to the statement
					if(genericCategory == 101) {
						index = loopHandle(index);	//for a nested loop, handled by loopHandle method
					} else {
						index = conditionHandle(index);	//for a nested condition, handled by conditionHandle method
					}
					continue;	//now next identifier
				} else if(tokenCategory == 11) {    //declaration statement, start with a _dec i.e declaration statement

                    /*objectDigging = new ObjectDigging(token);   //instantiate the objectDigging object
                    index =  objectDigging.cookObject(tokenIdentity, objectList, hmObjectInfo);*/

                    ObjectDiggingCompileTime objectDiggingCompileTime = new ObjectDiggingCompileTime(token);
                    index = objectDiggingCompileTime.objectHandle(index);
                    statementList.add(objectDiggingCompileTime.getStatement());
                    continue;
                }
				
				/*
				 * If token is opening first parenthesis
				 * 		Then the identity will be pushed in stack
				 * Else if the token is closing closing first parenthesis 
				 * 		Then the the value is pop out from stack
				 * When the stack is empty then the parameter potion is finished
				*/
				if(tokenCategory == 24) {	//got a opening second parenthesis	
					stkBody.push(hmTokenInfo.get(alToken.get(index)).getIdentity());	//if a opening second parenthesis is found then it is pushed to stack
				} else if(tokenCategory == 23) {	//got a closing second parenthesis
					stkBody.pop();		//closing second parenthesis is found and popped operation from the stack
				} else {
					statement.add(tokenIdentity);	//general element is added to the statement
				}
				
				filterBody.add(tokenIdentity);		//get the current token and then add to the methodBody Arraylist
				
				if(tokenCategory == 71) {
					startStatement = true;
				}
				index++;	//to handle the next token
				
			} while(!stkBody.isEmpty());	//if the stack is empty then we finished the current method method body teritorry i.e. finished
			
		}
		return index;	//first index of the end of the loop portion
	}
	
	private int loopHandle(int index) {
		if(hmTokenInfo.get(alToken.get(index)).getCatagory() == 31) {	//this is a for loop
			int forLoopIdentifier = hmTokenInfo.get(alToken.get(index)).getIdentity();	//get the current loop identifier
			
			filterBody.add(forLoopIdentifier);	//adding the identifier to the for loop body
			
			ForLoopDigging forLoopDigging = new ForLoopDigging(token, forLoopIdentifier);
			index = forLoopDigging.digForLoop();	//retrive the first index after the curent for loop teritorry
			
			
			ForLoopInfo forLoopInfo = new ForLoopInfo();	//received the current for loop info
			forLoopInfo = forLoopDigging.getForLoopInfo();	//get the for loop info from the forLoopDigging class
			
			hmForLoopInfo.put(forLoopIdentifier, forLoopInfo);	//now put the info according to the 
			
		} else if(hmTokenInfo.get(alToken.get(index)).getCatagory() == 33) {	//this is a while loop
			int whileLoopIdentifier = hmTokenInfo.get(alToken.get(index)).getIdentity();	//get the current loop identifier
			
			filterBody.add(whileLoopIdentifier);	//adding the identifier to the for loop body  
			
			WhileLoopDigging whileLoopDigging = new WhileLoopDigging(token, whileLoopIdentifier);
			index = whileLoopDigging.digWhileLoop();	// retrive the first index after the curent while loop teritorry
			
			WhileLoopInfo whileLoopInfo = new WhileLoopInfo();	//received the current while loop info
			whileLoopInfo = whileLoopDigging.getWhileLoopInfo();	//get the while loop info from the forLoopDigging class
			
			hmWhileLoopInfo.put(whileLoopIdentifier, whileLoopInfo);	//now put the info according to the 
		
		} 
		return index; //return the index of the first identifier of the parent loop
	}
	
	private int conditionHandle(int index) {
		
		if(hmTokenInfo.get(alToken.get(index)).getCatagory() == 32) {	//this is a if condition
			int ifConditionIdentifier = hmTokenInfo.get(alToken.get(index)).getIdentity();	//get the current if condition identifier
			
			filterBody.add(ifConditionIdentifier);	//adding the identifier to the body
			
			IfConditionDigging ifConditionDigging = new IfConditionDigging(token, ifConditionIdentifier);
			index = ifConditionDigging.digIfCondition();	//retrive the first index after the current if condition territory
			
			IfConditionInfo ifConditionInfo = new IfConditionInfo();	//received the current if condition info
			ifConditionInfo = ifConditionDigging.getIfConditionInfo();	//get the if Condition info from the ifConditionDigging class
			
			hmIfConditionInfo.put(ifConditionIdentifier, ifConditionInfo);	//now put the info according to the 
		} else if(hmTokenInfo.get(alToken.get(index)).getCatagory() == 36) {	//this is a else if condition
			int elseIfConditionIdentifier = hmTokenInfo.get(alToken.get(index)).getIdentity();	//get the current else if condition identifier
			
			filterBody.add(elseIfConditionIdentifier);	//adding the identifier to the body
			
			ElseIfConditionDigging elseIfConditionDigging = new ElseIfConditionDigging(token, elseIfConditionIdentifier);
			index = elseIfConditionDigging.digElseIfCondition();	//retrive the first index after the current else if condition territory
			
			ElseIfConditionInfo elseIfConditionInfo = new ElseIfConditionInfo();	//received the current else if condition info
			elseIfConditionInfo = elseIfConditionDigging.getElseIfConditionInfo();	//get the else if Condition info from the elseIfConditionDigging class
			
			hmElseIfConditionInfo.put(elseIfConditionIdentifier, elseIfConditionInfo);	//now put the info according to the 
		} else if(hmTokenInfo.get(alToken.get(index)).getCatagory() == 34) {	//this is a else condition
			int elseConditionIdentifier = hmTokenInfo.get(alToken.get(index)).getIdentity();	//get the current else if condition identifier
			
			filterBody.add(elseConditionIdentifier);	//adding the identifier to the body
			
			ElseConditionDigging elseConditionDigging = new ElseConditionDigging(token, elseConditionIdentifier);
			index = elseConditionDigging.digElseCondition();	//retrive the first index after the current else condition territory
			
			ElseConditionInfo elseConditionInfo = new ElseConditionInfo();	//received the current else condition info
			elseConditionInfo = elseConditionDigging.getElseConditionInfo();	//get the else Condition info from the elseConditionDigging class
			
			hmElseConditionInfo.put(elseConditionIdentifier, elseConditionInfo);	//now put the info according to the
		}
		return index;
	}
	
}
