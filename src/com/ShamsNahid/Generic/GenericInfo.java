package com.ShamsNahid.Generic;

import java.util.ArrayList;
import java.util.HashMap;

import com.ShamsNahid.Condition.Else.ElseConditionInfo;
import com.ShamsNahid.Condition.ElseIf.ElseIfConditionInfo;
import com.ShamsNahid.Condition.IF.IfConditionInfo;
import com.ShamsNahid.Loop.DoWhile.DoWhileLoopInfo;
import com.ShamsNahid.Loop.For.ForLoopInfo;
import com.ShamsNahid.Loop.While.WhileLoopInfo;
import com.ShamsNahid.Objects.MyObject;
import com.ShamsNahid.Objects.ObjectInfo;

/////////////////////////////////////NEED TO WORK WITH HEAD PORTION/////////////////////////////////////////////////////////////////////
/*
 * 	KEEPING GENERIC INFO, THAT IS COMMON TO ALL CONTAINER LIKE LOOP, METHOD AND CONDITIONAL STATEMENT
 *  FOR EXAMPLE EACH HAS A IDENTITY, NAME(LOL LOOP AND CONDITIONAL STATEMENT HAS FIXED NAME)
 *  EACH HAS A BODY
 *  EACH CAN CONTAIN LOOP AND CONDITIONAL STATEMENT
*/
public class GenericInfo {
	/*
	 * Heres the identitiy keeps the identity,
	 * like for a loops  the keyword has a identity, a method
	 * name aslo has a identity
	 * 
	 * KEYWORD: keep the name, for loop or conditional statement it keep their name
	 * FILTER BODY: Here only keep the containers body, like if  a method cotains a 
	 * 		loop the method filter body does not contain the whole loop body, just the loop name, loop information  goes to the hashmap
	 * BODY: It keeps the whole body, like if a loop exists it contains the whole loop body
	 * INFORMATION: If any specific information is needed then here it is
	 * 
	 *    HASHMAP
	 * 	If there is a variable, loop of conditional container in the parnet container,
	 *  	then the information of the variable will be stored in the hashmap according to their identity 
	*/
	protected Integer identity;		//identity, i.e. method name identity in token class
	protected String keyword;		//elements name
	protected ArrayList<Integer> headPortion;///////////////////////////////////////////////////////////////////////////////////
	protected ArrayList<Integer> filterBody;	//No full statement of a loop, condition exist, only the first keyword
	protected ArrayList<Integer> body; 	//method body all elements identifier will be stored here
	protected String information;		//If there any information left
	private ArrayList<String> objectList;	//all the variable in this scope will be placed here
	private MyObject myObject;

    public void setHmObjectInfo(HashMap<String, ObjectInfo> hmObjectInfo) {
        this.hmObjectInfo = hmObjectInfo;
    }

    protected ArrayList<ArrayList<Integer>> statementList;	//here list of statement will be placed
	
	private HashMap<Integer, GenericInfo> hmGenericInfo;
	
	protected HashMap<Integer, ForLoopInfo> HmForLoopInfo;	//if there is a for loop in the loop body, then the loop info is found here
	protected HashMap<Integer, WhileLoopInfo> HmWhileLoopInfo;	//if there is a while loop in the loop body, then the loop info is found here
	protected HashMap<Integer, DoWhileLoopInfo> HmDoWhileLoopInfo;	//if there is a do while loop in the loop body, then the loop info is found here
	protected HashMap<Integer, IfConditionInfo> HmIfConditionInfo;	//if there is a nested if condition info
	protected HashMap<Integer, ElseConditionInfo> HmElseConditionInfo;	//if there is a nested else statement
	protected HashMap<Integer, ElseIfConditionInfo> HmElseIfConditionInfo;	//if there is a nested else if statement
	protected HashMap<String, ObjectInfo> hmObjectInfo;	//variable information will be placed according to it's name
	
	//GETTER AND SETTER
	public Integer getIdentity() {
		return identity;
	}
	public void setIdentity(Integer identity) {
		this.identity = identity;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public ArrayList<Integer> getHeadPortion() {
		return headPortion;
	}
	public void setHeadPortion(ArrayList<Integer> headPortion) {
		this.headPortion = headPortion;
	}
	public ArrayList<Integer> getFilterBody() {
		return filterBody;
	}
	public void setFilterBody(ArrayList<Integer> filterBody) {
		this.filterBody = filterBody;
	}
	public ArrayList<Integer> getBody() {
		return body;
	}
	public void setBody(ArrayList<Integer> body) {
		this.body = body;
	}
	public String getInformation() {
		return information;
	}
	public void setInformation(String information) {
		this.information = information;
	}
    public MyObject getMyObject() {
        return myObject;
    }
    public void setMyObject(MyObject myObject) {
        this.myObject = myObject;
    }
	public HashMap<Integer, GenericInfo> getHmGenericInfo() {
		return hmGenericInfo;
	}
	public void setHmGenericInfo(HashMap<Integer, GenericInfo> hmGenericInfo) {
		this.hmGenericInfo = hmGenericInfo;
	}
	public HashMap<Integer, ForLoopInfo> getHmForLoopInfo() {
		return HmForLoopInfo;
	}
	public void setHmForLoopInfo(HashMap<Integer, ForLoopInfo> hmForLoopInfo) {
		HmForLoopInfo = hmForLoopInfo;
	}
	public HashMap<Integer, WhileLoopInfo> getHmWhileLoopInfo() {
		return HmWhileLoopInfo;
	}
	public void setHmWhileLoopInfo(HashMap<Integer, WhileLoopInfo> hmWhileLoopInfo) {
		HmWhileLoopInfo = hmWhileLoopInfo;
	}
	public HashMap<Integer, DoWhileLoopInfo> getHmDoWhileLoopInfo() {
		return HmDoWhileLoopInfo;
	}
	public void setHmDoWhileLoopInfo(HashMap<Integer, DoWhileLoopInfo> hmDoWhileLoopInfo) {
		HmDoWhileLoopInfo = hmDoWhileLoopInfo;
	}
	public HashMap<Integer, IfConditionInfo> getHmIfConditionInfo() {
		return HmIfConditionInfo;
	}
	public void setHmIfConditionInfo(HashMap<Integer, IfConditionInfo> hmIfConditionInfo) {
		HmIfConditionInfo = hmIfConditionInfo;
	}
	public HashMap<Integer, ElseConditionInfo> getHmElseConditionInfo() {
		return HmElseConditionInfo;
	}
	public void setHmElseConditionInfo(HashMap<Integer, ElseConditionInfo> hmElseConditionInfo) {
		HmElseConditionInfo = hmElseConditionInfo;
	}
	public HashMap<Integer, ElseIfConditionInfo> getHmElseIfConditionInfo() {
		return HmElseIfConditionInfo;
	}
	public void setHmElseIfConditionInfo(HashMap<Integer, ElseIfConditionInfo> hmElseIfConditionInfo) {
		HmElseIfConditionInfo = hmElseIfConditionInfo;
	}
	public ArrayList<ArrayList<Integer>> getStatementList() {
		return statementList;
	}
	public void setStatementList(ArrayList<ArrayList<Integer>> statementList) {
		this.statementList = statementList;
	}
	public void setHmvariableInfo(HashMap<String, ObjectInfo> hmVariableInfo) {
		this.hmObjectInfo = hmVariableInfo;
	}
	public HashMap<String, ObjectInfo> getHmObjectInfo() {
		return hmObjectInfo;
	}
	public ArrayList<String> getObjectList() {
		return objectList;
	}
	public void setObjectList(ArrayList<String> objectList) {
		this.objectList = objectList;
	}
}
