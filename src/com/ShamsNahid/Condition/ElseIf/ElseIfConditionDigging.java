package com.ShamsNahid.Condition.ElseIf;

import java.util.ArrayList;
import java.util.HashMap;

import com.ShamsNahid.Condition.IF.IfConditionInfo;
import com.ShamsNahid.Generic.GenericDigging;
import com.ShamsNahid.Generic.GenericInfo;
import com.ShamsNahid.Helper.Utility;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;

public class ElseIfConditionDigging {
	
	private Token token;	//we will receive this through constructor
	private ArrayList<Integer> alToken; 	//Then all token will be kept here
	private HashMap<Integer, TokenInfo> hmTokenInfo;	//Each token info is stored here for regarding its id

	private ElseIfConditionInfo elseIfConditionInfo;	//bitch here 
		
	private Integer elseIfConditionIdentifier;	//receive from methodDigging class
	
	GenericDigging genericDigging;
	
	//GETTER
	public ElseIfConditionInfo getElseIfConditionInfo()  {
		return elseIfConditionInfo;
	}
	
	public ElseIfConditionDigging(Token token, Integer elseIfConditionIdentifier) {
		this.token = token;
		this.alToken = token.getToken();
		this.hmTokenInfo = token.getHm();
		this.elseIfConditionIdentifier = elseIfConditionIdentifier;
		
		elseIfConditionInfo = new ElseIfConditionInfo();
	}
	
	//for loop digging placed here
	public int digElseIfCondition() {
		
		genericDigging = new GenericDigging(token, elseIfConditionIdentifier);
		int index = genericDigging.digGenericInfo();	//the last identifier (index + 1) of the else if condition is store in the index
		GenericInfo genericInfo = genericDigging.getgenericInfo();
		elseIfConditionInfo.setGenericInfo(genericInfo);
		
		System.out.println("//+_+_+_Head Portion of  a If: ");
		new Utility().printGenericElements(genericInfo.getHeadPortion(), hmTokenInfo, alToken);
		System.out.println("//+_+_+_Filter body of  a loop: ");
		new Utility().printGenericElements(genericInfo.getFilterBody(), hmTokenInfo, alToken);
		
		return index;	//the next index of the for loop territory is placed here
	}

}
