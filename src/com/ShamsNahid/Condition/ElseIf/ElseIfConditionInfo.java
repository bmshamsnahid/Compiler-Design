package com.ShamsNahid.Condition.ElseIf;

import com.ShamsNahid.Generic.GenericInfo;

public class ElseIfConditionInfo {
	private GenericInfo genericInfo;

	public GenericInfo getGenericInfo() {
		return genericInfo;
	}

	public void setGenericInfo(GenericInfo genericInfo) {
		this.genericInfo = genericInfo;
	}
}
