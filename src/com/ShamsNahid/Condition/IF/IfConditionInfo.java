package com.ShamsNahid.Condition.IF;

import com.ShamsNahid.Generic.GenericInfo;

public class IfConditionInfo {
	
	private GenericInfo genericInfo;

	public GenericInfo getGenericInfo() {
		return genericInfo;
	}

	public void setGenericInfo(GenericInfo genericInfo) {
		this.genericInfo = genericInfo;
	}
}
