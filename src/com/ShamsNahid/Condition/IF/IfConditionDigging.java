package com.ShamsNahid.Condition.IF;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import com.ShamsNahid.Generic.GenericDigging;
import com.ShamsNahid.Generic.GenericInfo;
import com.ShamsNahid.Helper.Utility;
import com.ShamsNahid.Loop.DoWhile.DoWhileLoopDigging;
import com.ShamsNahid.Loop.DoWhile.DoWhileLoopInfo;
import com.ShamsNahid.Loop.For.ForLoopDigging;
import com.ShamsNahid.Loop.For.ForLoopInfo;
import com.ShamsNahid.Loop.While.WhileLoopDigging;
import com.ShamsNahid.Loop.While.WhileLoopInfo;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;

public class IfConditionDigging {
	private Token token;	//we will receive this through constructor
	private ArrayList<Integer> alToken; 	//Then all token will be kept here
	private HashMap<Integer, TokenInfo> hmTokenInfo;	//Each token info is stored here for regarding its id

	private IfConditionInfo ifConditionInfo;	//bitch here 
		
	private Integer ifConditionIdentifier;	//receive from methodDigging class
	
	GenericDigging genericDigging;
	
	//GETTER
	public IfConditionInfo getIfConditionInfo()  {
		return ifConditionInfo;
	}
	
	public IfConditionDigging(Token token, Integer ifConditionIdentifier) {
		this.token = token;
		this.alToken = token.getToken();
		this.hmTokenInfo = token.getHm();
		this.ifConditionIdentifier = ifConditionIdentifier;
		
		ifConditionInfo = new IfConditionInfo();
	}
	
	//for loop digging placed here
	public int digIfCondition() {
		
		genericDigging = new GenericDigging(token, ifConditionIdentifier);
		int index = genericDigging.digGenericInfo();	//the last identifier (index + 1) of the if condition is store in the index
		GenericInfo genericInfo = genericDigging.getgenericInfo();
		ifConditionInfo.setGenericInfo(genericInfo);
		
		System.out.println("//++++++++++Head Portion of  a If: ");
		new Utility().printGenericElements(genericInfo.getHeadPortion(), hmTokenInfo, alToken);
		System.out.println("//++++++++++Filter body of  a loop: ");
		new Utility().printGenericElements(genericInfo.getFilterBody(), hmTokenInfo, alToken);
		
		return index;	//the next index of the for loop territory is placed here
	}
	
}
