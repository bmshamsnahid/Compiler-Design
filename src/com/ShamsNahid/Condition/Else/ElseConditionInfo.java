package com.ShamsNahid.Condition.Else;

import com.ShamsNahid.Generic.GenericInfo;

public class ElseConditionInfo {
	
	private GenericInfo genericInfo;

	public GenericInfo getGenericInfo() {
		return genericInfo;
	}

	public void setGenericInfo(GenericInfo genericInfo) {
		this.genericInfo = genericInfo;
	}

}
