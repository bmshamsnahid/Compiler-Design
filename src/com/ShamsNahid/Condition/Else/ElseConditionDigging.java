package com.ShamsNahid.Condition.Else;

import java.util.ArrayList;
import java.util.HashMap;

import com.ShamsNahid.Condition.ElseIf.ElseIfConditionInfo;
import com.ShamsNahid.Generic.GenericDigging;
import com.ShamsNahid.Generic.GenericInfo;
import com.ShamsNahid.Helper.Utility;
import com.ShamsNahid.Tokenizing.Token;
import com.ShamsNahid.Tokenizing.TokenInfo;

public class ElseConditionDigging {
	
	private Token token;	//we will receive this through constructor
	private ArrayList<Integer> alToken; 	//Then all token will be kept here
	private HashMap<Integer, TokenInfo> hmTokenInfo;	//Each token info is stored here for regarding its id

	private ElseConditionInfo elseConditionInfo;	//bitch here 
		
	private Integer elseConditionIdentifier;	//receive from parent class
	
	GenericDigging genericDigging;
	
	//GETTER
	public ElseConditionInfo getElseConditionInfo()  {
		return elseConditionInfo;
	}
	
	public ElseConditionDigging(Token token, Integer elseConditionIdentifier) {
		this.token = token;
		this.alToken = token.getToken();
		this.hmTokenInfo = token.getHm();
		this.elseConditionIdentifier = elseConditionIdentifier;
		
		elseConditionInfo = new ElseConditionInfo();
	}
	
	//for loop digging placed here
	public int digElseCondition() {
		
		genericDigging = new GenericDigging(token, elseConditionIdentifier);
		int index = genericDigging.digGenericInfo();	//the last identifier (index + 1) of the else if condition is store in the index
		GenericInfo genericInfo = genericDigging.getgenericInfo();
		elseConditionInfo.setGenericInfo(genericInfo);
		
		System.out.println("//+_+_+_Head Portion of  a else(must be null): ");
		new Utility().printGenericElements(genericInfo.getHeadPortion(), hmTokenInfo, alToken);
		System.out.println("//+_+_+_Filter body of  a else: ");
		new Utility().printGenericElements(genericInfo.getFilterBody(), hmTokenInfo, alToken);
		
		return index;	//the next index of the for loop territory is placed here
	}

}
