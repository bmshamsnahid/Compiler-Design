void main() {

    _dec tempObject = "temp object value", tailObject = "tail Object Value";
    _dec testNO = {
        _dec underTestNo = "value";
    };

    _dec obj1 = 1 + 7, obj2 = {
        _dec obj3 = "nestedOb3", underOb2 = {
            _dec obj3 = 1234;
        }, trailNest;
    };

    obj1 = obj1 + obj1;

    _dec nestedObjForArrayTest = { _dec arrayTestName = "value"; };

    _dec obArray = ["1", 2 + 3];

    /*_dec obArray = [obj2.underOb2.obj3, "1", 2 + 3, obj1 + obj2.underOb2.obj3, nestedObjForArrayTest];
    _dec obj3 = 15;

    obj3 = 55;

    obj2.underOb2.obj3 = obj3 + 45 + obj2.underOb2.obj3;
    obj1 = obj2.underOb2.obj3 + 10;
    _dec objComplex = obj1 + obj2.underOb2.obj3 + 3;

    _dec genuineObj3 = obj3;
    _dec nestedObj3 = obj2.obj3;
    _dec nestedNestedObj3 = obj2.underOb2.obj3;
    _dec assignObj2 = obj2;
    _dec afterAssignment = assignObj2.underOb2.obj3;
    _dec useAfterAssignment = afterAssignment + 1;

    _dec testNestedObj = {
        _dec name = "myName", myAge = 23, information = {
            _dec info = "myInfo";
            _dec occ = (21 + 12) + 3, nnOb = {
               _dec test = "nn ob val", testUninitialized;
            };
        };
    };

    _dec testObj = {
        _dec anotherName = "This is another name", testName = "Test";
    }, anotherNestedObject = {
        _dec nestedProperty = "nested Property Value";
    };

    //obj2 = testObj;

    _dec usePrevValue = 4 + testNestedObj.myAge + (3+5) + testNestedObj.information.occ;

    for(head section) {
        this is for statement;
    }*/
}

int func(param1, param2) {

}
